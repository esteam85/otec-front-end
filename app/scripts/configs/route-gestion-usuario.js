angular.module('otecApp')
  .config(function ($stateProvider,USER_ROLES) {


    $stateProvider
      .state('gestion-usuario', {
        url:"/gestion-usuario",
        templateUrl: 'views/administracion.html',
        controller: 'AdministracionCtrl',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        resolve:{
          titulo:function(){
            return "Gestión de Usuario"
          },
          tabData:function(){
            return [
              {
                heading: 'Cuenta',
                route:   'gestion-usuario.cuenta'
              }
            ];
          }
        }
      })
      .state('gestion-usuario.cuenta', {
        url: "/cuenta",
        templateUrl: 'views/templates/gestion-usuario-cuenta.html',
        controller:'GestionUsuarioCuentaCtrl'
      })
  })
