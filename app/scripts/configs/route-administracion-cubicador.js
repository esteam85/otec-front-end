angular.module('otecApp')
  .config(function ($stateProvider,USER_ROLES) {

    $stateProvider
      .state('administracion-cubicador', {
        url:"/administracion-cubicador",
        templateUrl: 'views/administracion.html',
        controller: 'AdministracionCtrl',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        resolve:{
          titulo:function(){
            return "Administración de Cubicador";
          },
          tabData:function(){
            return [
              {
                heading: 'Regiones',
                route:   'administracion-cubicador.regiones'
              },
              {
                heading: 'Agencias',
                route:   'administracion-cubicador.agencias'
              },
              {
                heading: 'Centrales',
                route:   'administracion-cubicador.centrales'
              },
              {
                heading: 'Servicios',
                route:   'administracion-cubicador.servicios'
              },
              {
                heading: 'Materiales',
                route:   'administracion-cubicador.materiales'
              }
            ];
          }
        }
      })
      .state('administracion-cubicador.regiones', {
        url:'/regiones',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve: {
          leyenda:function(){
            return "Regiones";
          },
          tituloBoton: function(){
            return "Crear Regiones";
          },
          metodos: function(){
            return {
              listar:"Region"};
          },
          tableHeader: function(regionHeaderTable){
            return regionHeaderTable;
          },

          opcionesCrearEditar: function(){
            return {
              formulario:[
                {
                  key: 'codigo',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Codigo',
                    placeholder: 'Codigo',
                    required: true
                  }
                },
                {
                  key: 'nombre',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Descripción',
                    placeholder: 'Descripción...',
                    required: true
                  }
                },
                {
                  "key": "factor",
                  "type": "horizontalInput",
                  "templateOptions": {
                    label: 'Factor',
                    placeholder: 'Factor',
                    type: 'number',
                    required: true

                  }
                }
              ],
              tituloEditar:"Editar Region",
              tituloCrear: "Nueva Region",
              metodos:{crear:"Region",editar:"Region"}
            };
          }
        }
      })
      .state('administracion-cubicador.proyectos', {
        url:'/proyectos',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve: {
          leyenda:function(){
            return "Proyectos";
          },
          tituloBoton: function(){
            return "Crear Proyectos";
          },
          metodos: function(){
            return {
              listar:"Proyecto"};
          },
          tableHeader: function(proyectoHeaderTable){
            return proyectoHeaderTable;
          },

          opcionesCrearEditar: function(){
            return {
              formulario:[
                {
                  key: 'nombre',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Nombre',
                    placeholder: 'Nombre',
                    required: true
                  }
                },
                {
                  key: 'descripcion',
                  type: 'horizontalTextarea',
                  templateOptions: {
                    label: 'Descripción',
                    placeholder: 'Descripción...',
                    required: true
                  }
                },
                {
                  "key": "contrato",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Contrato",
                    "label": "Contrato",
                    "valueProp": "id",
                    "labelProp": "nombre",
                    "metodo":"Contrato",
                    required: true

                  }
                },
                {
                  "key": "estado",
                  "type": "horizontalRadios",
                  "templateOptions": {
                    "label": "Estado",
                    "options": [
                      {
                        "name": "Activo",
                        "value": "A"
                      },
                      {
                        "name": "Inactivo",
                        "value": "I"
                      }
                    ]}
                }
              ],
              tituloEditar:"Editar Proyecto",
              tituloCrear: "Nuevo Proyecto",
              metodos:{crear:"Proyecto",editar:"Proyecto"}
            };
          }
        }
      })
      .state('administracion-cubicador.agencias', {
        url:'/agencias',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve: {
          leyenda:function(){
            return "Agencias";
          },
          tituloBoton: function(){
            return "Crear Agencia";
          },
          metodos: function(){
            return {
              listar:"Agencia"};
          },
          tableHeader: function(agenciaHeaderTable){
            return agenciaHeaderTable;
          },

          opcionesCrearEditar: function(){
            return {
              formulario:[
                {
                  key: 'nombre',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Nombre',
                    placeholder: 'Nombre',
                    required: true
                  }
                },
                {
                  key: 'descripcion',
                  type: 'horizontalTextarea',
                  templateOptions: {
                    label: 'Descripción',
                    placeholder: 'Descripción...',
                    required: true
                  }
                },
                {
                  "key": "regiones",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Region",
                    "label": "Region",
                    "labelProp": "nombre",
                    "metodo":"Region",
                    required: true
                  }
                },
                //{
                //  "key": "agenciasEspecialidadeses",
                //  "type": "horizontalUiMultiSelectInput",
                //  "templateOptions": {
                //    "placeholder": "Especialidades",
                //    "label": "Especialidades",
                //    "labelProp": "nombre",
                //    "metodo":"Especialidad",
                //  required: true,
                //    "input":{label:"Precio", "placeholder": "Precio",inputProp:"precio",required:true,type:"number"}
                //  }
                //},
                {
                  "key": "estado",
                  "type": "horizontalRadios",
                  "templateOptions": {
                    "label": "Estado",
                    "options": [
                      {
                        "name": "Activo",
                        "value": "A"
                      },
                      {
                        "name": "Inactivo",
                        "value": "I"
                      }
                    ]}
                }
              ],
              tituloEditar:"Editar Agencia",
              tituloCrear: "Nuevo Agencia",
              metodos:{crear:"Agencia",editar:"Agencia"}
            };
          }
        }
      })
      .state('administracion-cubicador.centrales', {
        url:'/centrales',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve: {
          leyenda:function(){
            return "Centrales";
          },
          tituloBoton: function(){
            return "Crear Central";
          },
          metodos: function(){
            return {
              listar:"Central"};
          },
          tableHeader: function(centralHeaderTable){
            return centralHeaderTable;
          },

          opcionesCrearEditar: function(){
            return {
              formulario:[
                {
                  key: 'nombre',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Nombre',
                    placeholder: 'Nombre',
                    required: true
                  }
                },
                {
                  key: 'descripcion',
                  type: 'horizontalTextarea',
                  templateOptions: {
                    label: 'Descripción',
                    placeholder: 'Descripción...',
                    required: true
                  }
                },
                {
                  "key": "agencias",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Agencia",
                    "label": "Agencia",
                    "valueProp": "id",
                    "labelProp": "nombre",
                    "metodo":"Agencia",
                    required: true
                  }
                },
                {
                  "key": "estado",
                  "type": "horizontalRadios",
                  "templateOptions": {
                    "label": "Estado",
                    "options": [
                      {
                        "name": "Activo",
                        "value": "A"
                      },
                      {
                        "name": "Inactivo",
                        "value": "I"
                      }
                    ]}
                }
              ],
              tituloEditar:"Editar Central",
              tituloCrear: "Nuevo Central",
              metodos:{crear:"Central",editar:"Central"}
            };
          }
        }
      })
      .state('administracion-cubicador.servicios', {
        url:'/servicios',
        templateUrl: 'views/administracion-item.html',
        controller: 'AdministracionItemCtrl',
        resolve:{
          tabData:function(){
            return [
              {
                heading: 'Servicios',
                route:   'administracion-cubicador.servicios.lista-servicios',
                default:true
              },
              {
                heading: 'Tipo Servicio',
                route:   'administracion-cubicador.servicios.lista-tipo-servicios',
                default:true
              }
            ];
          }
        }
      })
      .state('administracion-cubicador.servicios.lista-servicios', {
        url: "/lista-servicios",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Servicios";
          },
          tituloBoton: function(){
            return "Crear Servicio";
          },
          metodos: function(){
            return {
              listar:"Servicio"};
          },
          tableHeader: function(servicioHeaderTable){
            return servicioHeaderTable;
          },

          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:[
                {
                  key: 'nombre',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Nombre',
                    placeholder: 'Nombre',
                    required: true
                  }
                },
                {
                  key: 'descripcion',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Descripción',
                    placeholder: 'Descripción...',
                    required: true
                  }
                },
                {
                  key: 'codigo',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Codigo',
                    placeholder: 'Codigo',
                    required: true
                  }
                },
                {
                  key: 'precio',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'precio',
                    placeholder: 'precio',
                    type:'number',
                    required: true
                  }
                },
                {
                  "key": "tipoServicio",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Tipo Servicio",
                    "label": "Tipo Servicio",
                    "labelProp": "nombre",
                    "metodo":"TipoServicio",
                    required: true
                  }
                },
                {
                  "key": "tipoUnidadMedida",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Tipo Unidad Medida",
                    "label": "Tipo Unidad Medida",
                    "labelProp": "nombre",
                    "metodo":"TipoUnidadMedida",
                    required: true
                  }
                },
                {
                  "key": "serviciosMaterialeses",
                  "type": "horizontalUiMultiSelectInput",
                  "templateOptions": {
                    "subkey":"materiales",
                    "placeholder": "Materiales",
                    "label": "Materiales",
                    "labelProp": "nombre",
                    "metodo":"Material",
                    required: true,
                    "input":{label:"Cantidad", "placeholder": "Cantidad",inputProp:"cantidad",required:true,type:"number"}
                  }
                },
                {
                  "key": "estado",
                  "type": "horizontalRadios",
                  "templateOptions": {
                    "label": "Estado",
                    "options": [
                      {
                        "name": "Activo",
                        "value": "A"
                      },
                      {
                        "name": "Inactivo",
                        "value": "I"
                      }
                    ]}
                }
              ],
              tituloEditar:"Editar Servicio",
              tituloCrear: "Nuevo Servicio",
              metodos:{crear:"Servicio",editar:"Servicio"}
            };
          }
        }
      })
      .state('administracion-cubicador.servicios.lista-tipo-servicios', {
        url: "/lista-tipo-servicios",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Tipo Servicio";
          },
          tituloBoton: function(){
            return "Crear Tipo Servicio";
          },
          metodos: function(){
            return {
              listar:"TipoServicio"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            return tableTipoHeaderDefault;
          },

          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:tipoDefaultForm,
              tituloEditar:"Editar Tipo Servicio",
              proximoModal:"CargarImagen",
              tituloCrear: "Nuevo Tipo Servicio",
              metodos:{crear:"TipoServicio",editar:"TipoServicio"}
            };
          }
        }
      })
      .state('administracion-cubicador.materiales', {
        url:'/materiales',
        templateUrl: 'views/administracion-item.html',
        controller: 'AdministracionItemCtrl',
        resolve:{
          tabData:function(){
            return [
              {
                heading: 'Materiales',
                route:   'administracion-cubicador.materiales.lista-materiales',
                default:true
              },
              {
                heading: 'Tipo Material',
                route:   'administracion-cubicador.materiales.lista-tipo-material'
              },
              {
                heading: 'Tipo Moneda',
                route:   'administracion-cubicador.materiales.lista-tipo-moneda'
              },
              {
                heading: 'Tipo Unidad Medida',
                route:   'administracion-cubicador.materiales.lista-tipo-unidad-medida'
              }
            ];
          }
        }
      })
      .state('administracion-cubicador.materiales.lista-materiales', {
        url: "/lista-materiales",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Materiales";
          },
          tituloBoton: function(){
            return "Crear Material";
          },
          metodos: function(){
            return {
              listar:"Material"};
          },
          tableHeader: function(materialHeaderTable){
            return materialHeaderTable;
          },

          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:[
                {
                  key: 'nombre',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Nombre',
                    placeholder: 'Nombre',
                    required: true
                  }
                },
                {
                  key: 'codigo',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Codigo',
                    placeholder: 'Codigo',
                    required: true
                  }
                },
                {
                  key: 'codigoSap',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Codigo Sap',
                    placeholder: 'Codigo Sap',
                    required: true
                  }
                },
                {
                  key: 'descripcion',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Descripción',
                    placeholder: 'Descripción',
                    required: true
                  }
                },
                {
                  "key": "precio",
                  "type": "horizontalInput",
                  "templateOptions": {
                    label: 'Precio',
                    placeholder: 'Precio',
                    type: 'number',
                    required: true

                  }
                },
                {
                  "key": "stockActual",
                  "type": "horizontalInput",
                  "templateOptions": {
                    label: 'Stock Actual',
                    placeholder: 'Stock Actual',
                    type: 'number',
                    required: true

                  }
                },
                {
                  "key": "familia",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Tipo Material",
                    "label": "Tipo Material",
                    "valueProp": "id",
                    "labelProp": "nombre",
                    "metodo":"Familia",
                    required: true

                  }
                },
                {
                  "key": "tipoMoneda",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Tipo Moneda",
                    "label": "Tipo Moneda",
                    "valueProp": "id",
                    "labelProp": "nombre",
                    "metodo":"TipoMoneda",
                    required: true

                  }
                },
                {
                  "key": "tipoUnidadMedida",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Tipo Unidad Medida",
                    "label": "Tipo Unidad Medida",
                    "valueProp": "id",
                    "labelProp": "nombre",
                    "metodo":"TipoUnidadMedida",
                    required: true

                  }
                },
                {
                  "key": "estado",
                  "type": "horizontalRadios",
                  "templateOptions": {
                    "label": "Estado",
                    "options": [
                      {
                        "name": "Activo",
                        "value": "A"
                      },
                      {
                        "name": "Inactivo",
                        "value": "I"
                      }
                    ]}
                }
              ],
              tituloEditar:"Editar Material",
              tituloCrear: "Nuevo Material",
              metodos:{crear:"Material",editar:"Material"}
            };
          }
        }
      })
      .state('administracion-cubicador.materiales.lista-tipo-material', {
        url: "/lista-tipo-material",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Tipo Material";
          },
          tituloBoton: function(){
            return "Crear Tipo Material";
          },
          metodos: function(){
            return {
              listar:"Familia"};
          },
          tableHeader: function(){
            return [{ title: 'Nombre', field: 'nombre', visible: true},
              { title: 'Descripcion', field: 'descripcion',visible: true}
            ];
          },

          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:[
                {
                  key: 'nombre',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Nombre',
                    placeholder: 'Nombre',
                    required: true
                  }
                },
                {
                  key: 'descripcion',
                  type: 'horizontalTextarea',
                  templateOptions: {
                    label: 'Descripción',
                    placeholder: 'Descripción...',
                    required: true
                  }
                }

              ],
              tituloEditar:"Editar Tipo Material",
              tituloCrear: "Nuevo Tipo Material",
              metodos:{crear:"Familia",editar:"Familia"}
            };
          }
        }
      })
      .state('administracion-cubicador.materiales.lista-tipo-moneda', {
        url: "/lista-tipo-moneda",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Tipo Moneda";
          },
          tituloBoton: function(){
            return "Crear Tipo Moneda";
          },
          metodos: function(){
            return {
              listar:"TipoMoneda"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            return tableTipoHeaderDefault;
          },

          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:tipoDefaultForm,
              tituloEditar:"Editar Tipo Moneda",
              tituloCrear: "Nuevo Tipo Moneda",
              metodos:{crear:"TipoMoneda",editar:"TipoMoneda"}
            };
          }
        }
      })
      .state('administracion-cubicador.materiales.lista-tipo-unidad-medida', {
        url: "/lista-tipo-unidad-medida",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Tipo Unidad Medida";
          },
          tituloBoton: function(){
            return "Crear Tipo Unidad Medida";
          },
          metodos: function(){
            return {
              listar:"TipoUnidadMedida"};
          },
          tableHeader: function(){
            return [{ title: 'Codigo', field: 'codigo', visible: true},
              { title: 'Nombre', field: 'nombre',visible: true},
              { title: 'Estado', field: 'estado',formatter :"estado", class:"text-center", visible: true, filter: { 'name': 'text' } }
            ];
          },

          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:[
                {
                  key: 'codigo',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Codigo',
                    placeholder: 'Codigo',
                    required: true
                  }
                },
                {
                  key: 'nombre',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Nombre',
                    placeholder: 'Nombre',
                    required: true
                  }
                },
                {
                  "key": "estado",
                  "type": "horizontalRadios",
                  "templateOptions": {
                    "label": "Estado",
                    "options": [
                      {
                        "name": "Activo",
                        "value": "A"
                      },
                      {
                        "name": "Inactivo",
                        "value": "I"
                      }
                    ]}
                }
              ],
              tituloEditar:"Editar Tipo Unidad Medida",
              tituloCrear: "Nuevo Tipo Unidad Medida",
              metodos:{crear:"TipoUnidadMedida",editar:"TipoUnidadMedida"}
            };
          }
        }
      })



  });
