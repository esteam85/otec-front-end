angular.module('otecApp')
  .config(function ($stateProvider,USER_ROLES) {


    $stateProvider
      .state('gestion-ot-general', {
        url:"/gestion-ot-general",
        templateUrl: 'views/administracion.html',
        controller: 'AdministracionCtrl',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        resolve:{
          titulo:function(){
            return "Gestión de Órdenes de Trabajo"
          },
          tabData:function(){
            return [
              {
                heading: 'Ejecutar',
                route:   'gestion-ot-general.ot'
              },
              {
                heading: 'Abiertas',
                route:   'gestion-ot-general.ot-abierta'
              },
              {
                heading: 'Cerradas',
                route:   'gestion-ot-general.ot-cerrada'
              }
            ];
          }
        }
      })
      .state('gestion-ot-general.ot', {
        url: "/ot/:desdeBoton",
        templateUrl: 'views/templates/crear-ot-template.html',
        controller: 'CrearOtCtrl',
        title:"Ot Ejecutar",
        resolve:{
          leyenda:function(){
            return "Listado de Órdenes de Trabajo";
          },
          tituloBoton: function(){
            return "Crear OT/AP";
          },
          metodos: function(){
            return {
              action:"listarOt",
              parametros:{estado:"play",pos:0,cantidad:10}};
          },
          tableHeader: function(otGeneralHeaderTable){
            return otGeneralHeaderTable;
          },
          opcionesCrearEditar: function(otGeneralHeaderCrearEditarTable) {
            return otGeneralHeaderCrearEditarTable;
          }
        }
      })
      .state('gestion-ot-general.ot-abierta', {
        url: "/ot-abierta/:desdeBoton",
        templateUrl: 'views/templates/crear-ot-template.html',
        controller: 'CrearOtCtrl',
        title:"Ot Abiertas",
        resolve:{
          leyenda:function(){
            return "Listado de Órdenes de Trabajo";
          },
          tituloBoton: function(){
            return "Crear OT/AP";
          },
          metodos: function(){
            return {
              action:"listarOt",
              parametros:{estado:"abierta",pos:0,cantidad:10}};
          },
          tableHeader: function(otGeneralHeaderTable){
            return otGeneralHeaderTable;
          },
          opcionesCrearEditar: function(otGeneralHeaderCrearEditarTable) {
            return otGeneralHeaderCrearEditarTable;
          }
        }
      })
      .state('gestion-ot-general.ot-cerrada', {
        url: "/ot-cerrada/:desdeBoton",
        templateUrl: 'views/templates/crear-ot-template.html',
        controller: 'CrearOtCtrl',
        title:"Ot Cerradas",
        resolve:{
          leyenda:function(){
            return "Listado de Órdenes de Trabajo";
          },
          tituloBoton: function(){
            return "Crear OT/AP";
          },
          metodos: function(){
            return {
              action:"listarOt",
              parametros:{estado:"cerrada",pos:0,cantidad:10,orderBy:"-fechaCreacion"}};
          },
          tableHeader: function(otGeneralHeaderTable){
            return otGeneralHeaderTable;
          },
          opcionesCrearEditar: function(otGeneralHeaderCrearEditarTable) {
            return otGeneralHeaderCrearEditarTable;
          }
        }
      }).state('detalleOt', {
        url:"/detalleOt/:id?nombre?tipo?ejecutar?editar",
        templateUrl: 'views/templates/ver-detalle-ot.html',
        controller: 'VerDetalleOtCtrl',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        title:"Detalle Ot"
      });

  });
