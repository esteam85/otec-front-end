angular.module('otecApp')
  .config(function ($stateProvider,USER_ROLES) {

    $stateProvider
      .state('administracion-usuarios', {
        url:"/administracion-usuarios",
        templateUrl: 'views/administracion.html',
        controller: 'AdministracionCtrl',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        resolve:{
          titulo:function(){
            return "Administración de Usuarios";
          },
          tabData:function(){
            return [
              {
                heading: 'Usuarios',
                route:   'administracion-usuarios.usuarios'
              },
              {
                heading: 'Areas',
                route:   'administracion-usuarios.lista-areas'
              },
              {
                heading: 'Perfiles',
                route:   'administracion-usuarios.perfiles'
              },
              {
                heading: 'Roles',
                route:   'administracion-usuarios.roles'
              },
              {
                heading: 'Privilegios',
                route:   'administracion-usuarios.privilegios'
              }
            ];
          }
        }
      })

      .state('administracion-usuarios.usuarios', {
        url: "/usuarios",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Usuarios";
          },
          tituloBoton: function(){
            return "Crear Usuario";
          },
          metodos: function(){
            return {
              listar:"Usuario"};
          },
          tableHeader: function(usuarioHeaderTable){
            return  usuarioHeaderTable;
          },
          opcionesCrearEditar: function(){
            return {
              formulario:[{
                key: 'nombreUsuario',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombre Usuario',
                  placeholder: 'Nombre Usuario',
                  required: true
                }
              },{
                key: 'nombres',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombres',
                  placeholder: 'Nombres',
                  required: true
                }
              },{
                "key": "apellidos",
                type: 'horizontalInput',
                "templateOptions": {
                  "placeholder": "Apellidos",
                  "label": "Apellidos",
                  required: true

                }
              },{
                "key": "rut",
                type: 'horizontalInputRut',
                "templateOptions": {
                  "placeholder": "Rut",
                  "label": "Rut",
                  required: true

                }
              },{
                "key": "email",
                type: 'horizontalInput',
                "templateOptions": {
                  "placeholder": "Email",
                  "label": "Email",
                  "type": "email",
                  required: true

                }
              },{
                "key": "celular",
                type: 'horizontalInputMask',
                templateOptions: {
                  "label": 'Celular',
                  "placeholder": "Celular",
                  mask: '(999) 99999999',
                  required: true
                }
              },{
                "key": "proveedor",
                "type": "horizontalUiSelect",
                "templateOptions": {
                  "placeholder": "Proveedor",
                  "label": "Proveedor",
                  "valueProp": "id",
                  "labelProp": "nombre",
                  "metodo":"Proveedor",
                  required: true
                }
              },{
                "key": "area",
                "type": "horizontalUiSelect",
                "templateOptions": {
                  "placeholder": "Area",
                  "label": "Area",
                  "valueProp": "id",
                  "labelProp": "nombre",
                  "metodo":"Area",
                  required: true
                }
              },{
                "key": "repositorio",
                "type": "horizontalUiSelect",
                "templateOptions": {
                  "placeholder": "Repositorio",
                  "label": "Repositorio",
                  "valueProp": "id",
                  "labelProp": "nombre",
                  "metodo":"Repositorio",
                  required: true
                }
              },
                {
                "key": "usuariosContratos",
                "type": "horizontalUiMultiSelect",
                "templateOptions": {
                  "placeholder": "Contratos",
                  "label": "Contratos",
                  "valueProp": "id",
                  "labelProp": "nombre",
                  "metodo":"Contrato",
                  "soloId":true,
                  required: true
                }
              },
                {
                "key": "usuariosRoles",
                "type": "horizontalUiMultiSelect",
                "templateOptions": {
                  "placeholder": "Roles",
                  "label": "Roles",
                  "valueProp": "id",
                  "labelProp": "nombre",
                  "soloId":true,
                  "metodo":"Rol",
                  required: true
                }
              },
              //  {
              //    "key": "avatar",
              //    "type": "adjuntoImagen",
              //    "templateOptions": {
              //      "placeholder": "Imagen de Avatar",
              //      "label": "Imagen",
              //      required: false
              //    }
              //},
                {
                "key": "estado",
                "type": "horizontalRadios",
                "templateOptions": {
                  "label": "Estado",
                  "options": [
                    {
                      "name": "Activo",
                      "value": "A"
                    },
                    {
                      "name": "Inactivo",
                      "value": "I"
                    }
                  ]}
              }
              ],
              tituloEditar:"Editar Usuario",
              proximoModal:"CargarImagen",
              tituloCrear: "Nuevo Usuario",
              metodos:{crear:"Usuario",editar:"Usuario"}
            };
          }
        }
      })
      .state('administracion-usuarios.lista-areas', {
        url:'/lista-areas',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Areas";
          },
          tituloBoton: function(){
            return "Crear Area";
          },
          metodos: function(){
            return {
              listar:"Area"};
          },
          tableHeader: function(){
            return [{ title: 'nombre', field: 'nombre', visible: true, filter: { 'name': 'text' }},
              { title: 'descripcion', field: 'descripcion', visible: true, filter: { 'name': 'text' }}
            ];
          },
          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:[{
                key: 'nombre',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombres',
                  placeholder: 'Nombres',
                  required: true
                }
              },{
                "key": "descripcion",
                type: 'horizontalInput',
                "templateOptions": {
                  "placeholder": "Descripcion",
                  "label": "Descripcion",
                  required: true

                }
              }
              ],
              tituloEditar:"Editar Area",
              tituloCrear: "Nueva Area",
              metodos:{crear:"Area",editar:"Area"}
            };
          }
        }
      })
      .state('administracion-usuarios.perfiles', {
        url: "/lista-perfiles",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Perfiles";
          },
          tituloBoton: function(){
            return "Crear Perfil";
          },
          metodos: function(){
            return {
              listar:"Perfil"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            return tableTipoHeaderDefault;
          },

          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:tipoDefaultForm,
              tituloEditar:"Editar Perfil",
              tituloCrear: "Nuevo Perfil",
              metodos:{crear:"Perfil",editar:"Perfil"}
            };
          }
        }
      })
      .state('administracion-usuarios.roles', {
        url:'/parametros',
        templateUrl: 'views/administracion-item.html',
        controller: 'AdministracionItemCtrl',
        resolve:{
          tabData:function(){
            return [
              {
                heading: 'Roles',
                route:   'administracion-usuarios.roles.lista-roles',
                default:true
              },
              {
                heading: 'Opciones de Menú',
                route:   'administracion-usuarios.roles.lista-opciones-menu'
              }
            ];
          }
        }
      })
      .state('administracion-usuarios.roles.lista-roles', {
        url: "/lista-roles",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Roles";
          },
          tituloBoton: function(){
            return "Crear Rol";
          },
          metodos: function(){
            return {
              listar:"Rol"};
          },
          tableHeader: function(rolHeaderTable){
            return rolHeaderTable;
          },
          opcionesCrearEditar: function(){
            return {
              formulario:[{
                key: 'nombre',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombre',
                  placeholder: 'Nombre',
                  required: true
                }
              },{
                "key": "perfil",
                "type": "horizontalUiSelect",
                "templateOptions": {
                  "placeholder": "Perfil",
                  "label": "Perfil",
                  "valueProp": "id",
                  "labelProp": "nombre",
                  "metodo":"Perfil",
                  required: true

                }
              },
              //  {
              //  "key": "rolesPrivilegios",
              //  "type": "horizontalUiMultiSelect",
              //  "templateOptions": {
              //    "placeholder": "Privilegios",
              //    "label": "Privilegios",
              //    "valueProp": "id",
              //    "labelProp": "nombre",
              //    "metodo":"Privilegio",
              //    required: true
              //  }
              //},
                {
                "key": "rolesOpciones",
                "type": "horizontalUiMultiSelect",
                "templateOptions": {
                  "placeholder": "Opciones de Menú",
                  "label": "Opciones Menú",
                  "valueProp": "id",
                  "labelProp": "nombre",
                  "metodo":"Opcion",
                  required: true
                }
              },{
                "key": "estado",
                "type": "horizontalRadios",
                "templateOptions": {
                  "label": "Estado",
                  "options": [
                    {
                      "name": "Activo",
                      "value": "A"
                    },
                    {
                      "name": "Inactivo",
                      "value": "I"
                    }
                  ]}
              }
              ],
              tituloEditar:"Editar Rol",
              tituloCrear: "Nuevo Rol",
              metodos:{crear:"Rol",editar:"Rol"}
            };
          }

        }
      })
      .state('administracion-usuarios.roles.lista-opciones-menu', {
        url: "/lista-opciones-menu",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Opciones de Menú";
          },
          tituloBoton: function(){
            return "Crear Opción de Menú";
          },
          metodos: function(){
            return {
              listar:"Opcion"};
          },
          tableHeader: function(menuHeaderTable){
            return menuHeaderTable;
          },
          opcionesCrearEditar: function(){
            return {
              formulario:[{
                key: 'nombre',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombre',
                  placeholder: 'Nombre',
                  required: true
                }
              },{
                key: 'codigoMenu',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Código',
                  type: 'number',
                  placeholder: 'Código de Opción de Menú',
                  required: true
                }
              },{
                  "key": "descripcion",
                  type: 'horizontalTextarea',
                  "templateOptions": {
                    "placeholder": "Descripción",
                    "label": "Descripción",
                    required: true
                  }
                },
                {
                "key": "estado",
                "type": "horizontalRadios",
                "templateOptions": {
                  "label": "Estado",
                  "options": [
                    {
                      "name": "Activo",
                      "value": "A"
                    },
                    {
                      "name": "Inactivo",
                      "value": "I"
                    }
                  ]}
              }
              ],
              tituloEditar:"Editar Opción de Menú",
              tituloCrear: "Nueva Opción de Menú",
              metodos:{crear:"Opcion",editar:"Opcion"}
            };
          }

        }
      })
      .state('administracion-usuarios.privilegios', {
        url: "/lista-privilegios",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Privilegios";
          },
          tituloBoton: function(){
            return "Crear Privilegio";
          },
          metodos: function(){
            return {
              listar:"Privilegio"};
          },
          tableHeader: function(privilegioHeaderTable){
            return privilegioHeaderTable;
          },
          opcionesCrearEditar: function(){
            return {
              formulario:[{
                key: 'nombre',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombre',
                  placeholder: 'Nombre',
                  required: true
                }
              },{
                "key": "descripcion",
                type: 'horizontalInput',
                "templateOptions": {
                  "placeholder": "Descripcion",
                  "label": "Descripcion",
                  required: true

                }
              },{
                "key": "evento",
                "type": "horizontalUiSelect",
                "templateOptions": {
                  "placeholder": "Evento",
                  "label": "Evento",
                  "valueProp": "id",
                  "labelProp": "nombre",
                  "metodo":"Evento",
                  required: true

                }
              }
              ],
              tituloEditar:"Editar Privilegio",
              tituloCrear: "Nuevo Privilegio",
              metodos:{crear:"Privilegio",editar:"Privilegio"}
            };
          }
        }
      });

  });
