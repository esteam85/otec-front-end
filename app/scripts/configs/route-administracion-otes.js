angular.module('otecApp')
  .config(function ($stateProvider,USER_ROLES) {

    $stateProvider
      //Administracion de OT
      .state('administracion-ot', {
        url:"/administracion-ot",
        templateUrl: 'views/administracion.html',
        controller: 'AdministracionCtrl',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        resolve:{
          titulo:function(){
            return "Administración de Ordenes de Trabajo"
          },
          tabData:function(){
            return [
              {
                heading: 'Ordenes de Trabajo',
                route:   'administracion-ot.otes'
              },
              {
                heading: 'Repositorios',
                route:   'administracion-ot.lista-repositorios'
              }
            ];
          }
        }
      })

      .state('administracion-ot.otes', {
        url:'/otes',
        templateUrl: 'views/administracion-item.html',
        controller: 'AdministracionItemCtrl',
        resolve:{
          tabData:function(){
            return [
              {
                heading: 'Tipo de OT',
                route:   'administracion-ot.otes.lista-tipo-otes',
                default:true
              },
              {
                heading: 'Tipo de Estados de OT',
                route:   'administracion-ot.otes.lista-tipo-estado-otes'
              },
              {
                heading: 'Tipo de Solicitud',
                route:   'administracion-ot.otes.lista-tipo-solicitud'
              }
            ];
          }
        }
      })

      .state('administracion-ot.otes.lista-tipo-otes', {
        url: "/lista-tipo-otes",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Tipo de Ordenes de Trabajo";
          },
          tituloBoton: function(){
            return "Crear Tipo de OT";
          },
          metodos: function(){
            return {
              listar:"TipoOt"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            return tableTipoHeaderDefault;
          },

          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:tipoDefaultForm,
              tituloEditar:"Editar Tipo Orden de Trabajo",
              tituloCrear: "Nuevo Tipo Orden de Trabajo",
              metodos:{crear:"TipoOt",editar:"TipoOt"}
            };
          }
        }
      })

      .state('administracion-ot.otes.lista-tipo-estado-otes', {
        url:'/lista-tipo-estado-otes',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Estados de Ordenes de Trabajo";
          },
          tituloBoton: function(){
            return "Crear Estado de OT";
          },
          metodos: function(){
            return {
              listar:"TipoEstadoOt"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            return tableTipoHeaderDefault;
          },
          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:tipoDefaultForm,
              tituloEditar:"Editar Estado de Orden de Trabajo",
              tituloCrear: "Nuevo Estado de Orden de Trabajo",
              metodos:{crear:"TipoEstadoOt",editar:"TipoEstadoOt"}
            };
          }
        }
      })

      .state('administracion-ot.otes.lista-tipo-solicitud', {
        url:'/lista-tipo-solicitud',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Estados de Ordenes de Trabajo";
          },
          tituloBoton: function(){
            return "Crear Estado de OT";
          },
          metodos: function(){
            return {
              listar:"TipoSolicitud"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            return tableTipoHeaderDefault;
          },
          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:tipoDefaultForm,
              tituloEditar:"Editar Estado de Orden de Trabajo",
              tituloCrear: "Nuevo Estado de Orden de Trabajo",
              metodos:{crear:"TipoSolicitud",editar:"TipoSolicitud"}
            };
          }
        }
      })
      .state('administracion-ot.lista-repositorios', {
        url:'/lista-repositorios',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Repositorios";
          },
          tituloBoton: function(){
            return "Crear Repositorio";
          },
          metodos: function(){
            return {
              listar:"Repositorio"};
          },
          tableHeader: function(repositorioHeaderTable){
            return repositorioHeaderTable;
          },
          opcionesCrearEditar: function(){


            return {
              formulario:[{
                key: 'servidor',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Servidor',
                  placeholder: 'Servidor',
                  required: true
                }
              },{
                key: 'usuario',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Usuario',
                  placeholder: 'Usuario',
                  required: true
                }
              },{
                key: 'nombre',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombre',
                  placeholder: 'Nombre',
                  required: true
                }
              },{
                key: 'password',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Password',
                  placeholder: 'password',
                  required: true
                }
              },{
                key: 'puerto',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Puerto',
                  placeholder: 'Puerto',
                  type: 'number',
                  required: true
                }
              },{
                key: 'url',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Url',
                  placeholder: 'Url',
                  required: true
                }
              },{
                key: 'descripcion',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Descripcion',
                  placeholder: 'Descripcion',
                  required: true
                }
              }

              ],
              tituloEditar:"Editar Repositorio",
              tituloCrear: "Nuevo Repositorio",
              metodos:{crear:"Repositorio",editar:"Repositorio"}
            };
          }
        }
      });

  });


