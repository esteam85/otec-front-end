angular.module('otecApp')
  .config(function ($stateProvider,$urlRouterProvider,$httpProvider,$locationProvider,RestangularProvider,USER_ROLES) {

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    //console.log($cookieStore);
    //RestangularProvider.setDefaultHeaders({ Authorization: function() { return "Token " + $cookieStore.get('id'); } })

    $urlRouterProvider.when('', '/login');
    $urlRouterProvider.otherwise("/login");
    //$locationProvider.html5Mode(true).hashPrefix('!');

    //RestangularProvider.setBaseUrl('https://api-otec.herokuapp.com/');

    //RestangularProvider.setBaseUrl('http://localhost:8080');
    //Erbi
    //RestangularProvider.setBaseUrl('http://192.168.1.228:9090');
    //RestangularProvider.setBaseUrl('http://192.168.1.252:8080');
    //Jony
    //RestangularProvider.setBaseUrl('http://192.168.1.241:8080');



    RestangularProvider.setFullResponse(true);
    $stateProvider
      .state('login', {
        url:'/login',
        templateUrl: 'views/login.html',
        data:{
          authorizedRoles: [USER_ROLES.guest]
        },
        controller:'LoginCtrl',
        title:"Login"
      }).state('dummy', {
        url:'/dummy',
        templateUrl: 'views/templates/crear-workflow-eventos.html',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        controller:'CrearWorkflowEventosCtrl'
      })
      .state('dashboard', {
        url:'/dashboard',
        templateUrl: 'views/dashboard.html',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        title:"Dashboard",
        controller:'DashboardCtrl'
      }).state('cambioPass', {
        url:'/cambioPass?token',
        data:{
          authorizedRoles: [USER_ROLES.guest]
        },
        templateUrl: 'views/templates/cambio-clave.html',
        controller:'CambioClaveCtrl',
        title:"Cambio de Password"

      });

  });
