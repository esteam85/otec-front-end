angular.module('otecApp')
  .config(function ($stateProvider,USER_ROLES) {

    var headerTableItems = [
      {
        title: 'Item',
        type:"formlyAdd",
        valorFormField:{
          "key": "item",
          "type": "input",
          "templateOptions": {
            "placeholder": "Item",
            "required": true
          },
          expressionProperties: {
            'templateOptions.disabled': 'model.disabled'}
        },
        field: 'item',
        visible: true
      },
      {
        title: 'Unidad',
        type:"formlyAdd",
        valorFormField:{
          "key": "tipoUnidadMedida",
          "type": "horizontalUiSelectGETNoLabel",
          "templateOptions": {
            "valueProp": "id",
            "labelProp": "nombre",
            "placeholder": "Unidad",
            "metodo":"TipoUnidadMedida",
            "required": true
          },
          expressionProperties: {
            'templateOptions.disabled': 'model.disabled'}
        },
        field: 'precio',
        visible: true
      },
      {
        title: 'Cantidad',
        type:"formlyAdd",
        valorFormField:{
          "key": "cantidad",
          "type": "input",
          "templateOptions": {
            "min":0,
            "type":"number",
            "placeholder": "Cantidad",
            "required": true
          },
          expressionProperties: {
            'templateOptions.disabled': 'model.disabled'}
        },
        field: 'cantidad',
        visible: true
      },
      {
        title: 'Precio',
        type:"formlyAdd",
        valorFormField:{
          "key": "precio",
          "type": "input",
          "templateOptions": {
            "type":"number",
            "placeholder": "Precio",
            "required": true
          },
          expressionProperties: {
            'templateOptions.disabled': 'model.disabled'}
        },
        field: 'precio',
        visible: true
      },
      {
        title: 'Moneda',
        type:"formlyAdd",
        valorFormField:{
          "key": "tipoMoneda",
          "type": "horizontalUiSelectGETNoLabel",
          "templateOptions": {
            "valueProp": "id",
            "labelProp": "nombre",
            "placeholder": "Moneda",
            "metodo":"TipoMoneda",
            "required": true
          },
          expressionProperties: {
            'templateOptions.disabled': 'model.disabled'}
        },
        field: 'precio',
        visible: true
      },
      {
        title: 'Quitar',
        type:"remover",
        class:"text-center icon-options",
        visible: true
      }
    ];

    $stateProvider
      .state('gestion-cubicacion-general', {
        url:"/gestion-cubicacion-general",
        templateUrl: 'views/administracion.html',
        controller: 'AdministracionCtrl',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        resolve:{
          titulo:function(){
            return "Gestión de Costeos";
          },
          tabData:function(){
            return [
              {
                heading: 'Listar Costeos',
                route:   'gestion-cubicacion-general.cubicacion'
              }
            ];
          }
        }
      })
      .state('gestion-cubicacion-general.cubicacion', {
        url:'/cubicacion/:desdeBoton',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'CubicacionCtrl',
        title:"Cubicación/Costeo",
        resolve:{
          leyenda:function(){
            return "Listar Costeos";
          },
          tituloBoton: function(){
            return "Crear Costeo";
          },
          metodos: function(){
            return {
              action: "listarCubicacionesPorUsuarioId",
              parametros:{pos:0,cantidad:10,orderBy:"-fechaCreacion"}
            };
          },
          tableHeader: function(cubicadorHeaderTable){
            return cubicadorHeaderTable;
          },
          opcionesCrearEditar: function(){
            return {
              formulario:[
                {
                  className: 'row',
                  fieldGroup:[
                    {
                      className: 'col-lg-6',
                      fieldGroup:[
                        {
                          className: 'row',
                          fieldGroup: [
                            {
                              "key": "contrato",
                              "type": "horizontalUiSelectGET",
                              "templateOptions": {
                                "placeholder": "Contrato",
                                "label": "Contrato",
                                "valueProp": "id",
                                "labelProp": "nombre",
                                "action":"listarContratosPorUsuario",
                                required: true
                              }
                            },
                            {
                              "key": "region",
                              "type": "horizontalUiSelectGetCascade",
                              "templateOptions": {
                                "action":"listarRegionesPorContrato",
                                "parametros":function(model){
                                  return{contratoId:model.contrato.id};
                                },
                                "toWatch":'contrato',
                                "get":true,
                                "placeholder": "Región",
                                "label": "Región",
                                "valueProp": "id",
                                "labelProp": "nombre",
                                "metodo":"Region",
                                required: true
                              },
                              hideExpression:"model.contrato.id !== 1 && model.contrato.id !== 3 && model.contrato.id !== 4"
                            },
                            {
                              "key": "proveedor",
                              "type": "horizontalUiSelectGetCascade",
                              "templateOptions": {
                                "action":"listarProveedoresPorRegion",
                                "toWatch":'region',
                                "get":true,
                                "parametros":function(model){return{contratoId:model.contrato.id,regionId:model.region.id}},
                                "placeholder": "Proveedor",
                                "label": "Proveedor",
                                "valueProp": "id",
                                "labelProp": "nombre",
                                "rejectId": 9999,
                                required: true
                              },
                              hideExpression:"(model.contrato.id !== 1 && model.contrato.id !== 3 && model.contrato.id !== 4) || model.region === undefined"
                            },
                            {
                              "key": "proveedor",
                              "type": "horizontalUiSelectGET",
                              "templateOptions": {
                                "add":{
                                  "toolTip":"Nuevo Proveedor",
                                  formulario:[
                                    {
                                      key: 'nombre',
                                      type: 'horizontalInput',
                                      templateOptions: {
                                        label: 'Nombre',
                                        placeholder: 'Nombre',
                                        required: true
                                      }
                                    },
                                    {
                                      "key": "rut",
                                      type: 'horizontalInputRut',
                                      "templateOptions": {
                                        "placeholder": "Rut",
                                        "label": "Rut",
                                        required: true
                                      }
                                    },{
                                      "key": "email",
                                      type: 'horizontalInput',
                                      "templateOptions": {
                                        "placeholder": "Email",
                                        "label": "Email",
                                        "type": "email",
                                        required: true

                                      }
                                    },
                                    {
                                      "key": "telefono",
                                      type: 'horizontalInputMask',
                                      templateOptions: {
                                        "label": 'Teléfono',
                                        "placeholder": "Teléfono",
                                        mask: '(9999) 99999999',
                                        required: true
                                      }
                                    },
                                    {
                                      "key": "direccion",
                                      type: 'horizontalInput',
                                      "templateOptions": {
                                        "placeholder": "Dirección",
                                        "label": "Dirección",
                                        required: true

                                      }
                                    }
                                  ],
                                  tituloEditar:"Editar Proveedor",
                                  tituloCrear: "Nuevo Proveedor",
                                  "action":"crearProveedorCubicador"

                                },
                                "placeholder": "Proveedor",
                                "label": "Proveedor",
                                "valueProp": "id",
                                "labelProp": "nombre",
                                "rejectId": 9999,
                                "metodo":"Proveedor",
                                required: true
                              },
                              hideExpression:"model.contrato.id !== 2"
                            },
                            {
                              "key": "tipoServicio",
                              "type": "horizontalUiSelectGetCascade",
                              "templateOptions": {
                                "action":"listarTipoServicioPorContrato",
                                "placeholder": "Tipo Servicio",
                                "parametros":function(model){return{contratoId:model.contrato.id}},
                                "label": "Tipo Servicio",
                                "toWatch":'contrato',
                                "get":true,
                                "labelProp": "nombre",
                                required: true
                              },
                              hideExpression:"(model.contrato.id !== 1 && model.contrato.id !== 3 && model.contrato.id !== 4) || model.disabled === true || model.proveedor.id === undefined"
                            },
                            {
                              "key": "cubicadorServiciosPrev",
                              "type": "horizontalUiSelectGetCascade",
                              "templateOptions": {
                                "placeholder": "Servicio",
                                "parametros":function(model){
                                  return {
                                    contratoId:model.contrato.id,regionId:model.region.id,proveedorId:model.proveedor.id,tipoServicioId:model.tipoServicio.id
                                  };
                                },
                                "action":"listarServiciosPorContratoRegionProveedor",
                                "label": "Servicio",
                                "toWatch":"tipoServicio",
                                "subkey":"servicio",
                                "get":true,
                                "labelProp": "nombre",
                                required: true
                              },
                              hideExpression:"(model.contrato.id !== 3) || model.disabled === true || model.tipoServicio.id === undefined"
                            }
                          ]
                        }
                      ]
                    },
                    {
                      className: 'col-lg-6',
                      fieldGroup:[
                        {
                          className: 'row',
                          fieldGroup: [
                            {
                              key: 'nombre',
                              type: 'horizontalInput',
                              templateOptions: {
                                label: 'Nombre',
                                placeholder: 'Nombre',
                                required: true
                              }
                            },
                            {
                              "key": "descripcion",
                              type: 'horizontalTextarea',
                              "templateOptions": {
                                "placeholder": "Descripción",
                                "label": "Descripción",
                                required: true
                              }
                            }
                          ]
                        }
                      ]
                    },
                  ]
                },
                {
                  className:'row',
                  fieldGroup:[
                    {
                      key:"cubicadorServiciosTitulo",
                      "type":"titleFancy",
                      "templateOptions":{
                        "buttons":[{
                          "icon":"fa fa-trash-o",
                          "click":function(model,to,scope){
                            model.cubicadorServicios = [];
                          },
                          tooltip:"Borrar Todo"
                        }],
                        "title":"Servicios"
                      },
                      hideExpression:"(model.contrato.id !== 1 && model.contrato.id !== 4) || model.proveedor === undefined || model.region === undefined || model.contrato === undefined || model.tipoServicio === undefined || model.disabled === true"
                    }
                  ]
                },
                {
                  className: 'row',
                  fieldGroup:[
                    {
                      className: 'col-lg-12',
                      fieldGroup:[
                        {
                          "key": "cubicadorServicios",
                          "type": "horizontalUiMultiSelectInputGETCascadeNoLabel",
                          "templateOptions": {
                            "toWatch":"tipoServicio",
                            "subkey":"servicio",
                            "placeholder": "Servicios",
                            "labelProp": "descripcion",
                            "valoresDefecto":function(data,model){
                              if(model.cubicadorServicios == undefined) model.cubicadorServicios = [];

                              var retorno = [];
                              _.each(data,function(item){

                                if(item.servicio.isPackBasico){
                                  if(!_.find(model.cubicadorServicios, function(oldItem){ return oldItem.servicio.id == item.servicio.id})){
                                    item.cantidad = 1;
                                    retorno.push(item)
                                  }
                                };
                              });
                              retorno = retorno.concat(model.cubicadorServicios);
                              return retorno;
                            },
                            "parametros":function(model){
                              return {
                                contratoId:model.contrato.id,regionId:model.region.id,proveedorId:model.proveedor.id,tipoServicioId:model.tipoServicio.id
                              };
                            },
                            "action":"listarServiciosPorContratoRegionProveedor",
                            required: true,
                            "input":{label:"Cantidad", min:0, "placeholder": "Cantidad",inputProp:"cantidad",onSelect:function(item,model,scope){
                              item.cantidad=1;
                            }, required:true,type:"number"}
                          },
                          hideExpression:"(model.contrato.id !== 1 && model.contrato.id !== 4) || model.proveedor === undefined || model.region === undefined || model.contrato === undefined || model.tipoServicio === undefined || model.disabled === true",
                        }
                      ]
                    }
                  ]
                },
                {
                  className: 'row',
                  fieldGroup:[
                    {
                      key:"tableServicios",
                      type:"simpleTable",
                      templateOptions:{
                        title:"Resumen Servicios",
                        targetkey:"cubicadorServicios",
                        compareKey:"cantidad",
                        totalKey:"totalServicios",
                        columns:[{
                          nestedField:"descripcion",
                          field: "servicio",
                          title: 'Descripción',
                          sortable: "descripcion",
                          visible: true
                        },
                          {
                            field: "cantidad",
                            title: 'Cantidad',
                            type:'number',
                            sortable: "name",
                            visible: true,
                            class:"text-center",
                            groupable: "name"
                          },
                          {
                            nestedField:"precio",
                            field: "servicio",
                            title: 'Precio',
                            currency:true,
                            class:"text-center",
                            sortable: "precio",
                            visible: true
                          },
                          {
                            expression:function(objeto){
                              var total = 0;
                              var cantidad = 0;
                              if(objeto.cantidad){ cantidad = objeto.cantidad}
                              total = cantidad * objeto.servicio.precio;
                              objeto.total = total;
                              return total;
                            },
                            field: "total",
                            class:"text-center",
                            currency:true,
                            title: 'Total',
                            sortable: "total",
                            visible: true
                          }]
                      },
                      hideExpression:"(model.contrato.id !== 1 && model.contrato.id !== 4) || !model.cubicadorServicios"
                    },
                    {
                      key:"cubicadorServicios",
                      type:"simpleTable",
                      templateOptions:{
                        title:"Resumen Servicios",
                        targetkey:"cubicadorServiciosPrev",
                        consolidarDatos: function(data,columns,scope){
                          var obj = {};
                          obj.servicio = data;
                          obj.cantidad = 1;
                          obj.descripcion=obj.servicio.descripcion;
                          obj.precio=obj.servicio.precio;
                          obj.total = obj.cantidad * obj.servicio.precio;

                          if(obj.servicio.tipoMoneda.id == 1){
                            scope.to.columnsB[2].currencySimbol = "US$";
                            scope.to.columnsB[3].currencySimbol = "US$";
                          }
                          if(obj.servicio.tipoMoneda.id == 2){
                            scope.to.columnsB[2].currencySimbol = "$";
                            scope.to.columnsB[3].currencySimbol = "$";
                          }
                          if(obj.servicio.tipoMoneda.id == 3){
                            scope.to.columnsB[2].currencySimbol = "UF";
                            scope.to.columnsB[3].currencySimbol = "UF";
                          }
                          if(obj.servicio.tipoMoneda.id == 4){
                            scope.to.columnsB[2].currencySimbol = "€";
                            scope.to.columnsB[3].currencySimbol = "€";
                          }

                          scope.model["cubicadorServicios"] = [obj];
                          scope.objetos = [obj];

                          scope.refresh();

                        },
                        compareKey:"cantidad",
                        totalKey:"totalServicios",
                        columns:[{
                          nestedField:"descripcion",
                          field: "servicio",
                          title: 'Descripción',
                          sortable: "descripcion",
                          visible: true
                        },
                          {
                            field: "cantidad",
                            title: 'Cantidad',
                            type:'number',
                            sortable: "name",
                            visible: true,
                            class:"text-center",
                            groupable: "name"
                          },
                          {
                            nestedField:"precio",
                            field: "servicio",
                            title: 'Precio',
                            currency:true,
                            class:"text-center",
                            sortable: "precio",
                            visible: true
                          },
                          {
                            field: "total",
                            class:"text-center",
                            currency:true,
                            title: 'Total',
                            sortable: "total",
                            visible: true
                          }]
                      },
                      hideExpression:"( model.contrato.id !== 3) || !model.cubicadorServiciosPrev"
                    }
                  ]
                },
                {
                  className: 'row',
                  fieldGroup:[
                    {
                      className: 'row',
                      fieldGroup: [
                        {
                          key: 'cubicadorOrdinario',
                          type: 'TableForm',
                          templateOptions: {
                            minData:{},
                            requerid:true,
                            type:'add',
                            titulo:"Items",
                            columns:headerTableItems
                          },
                          hideExpression:"model.contrato.id !== 2 || model.disabled === true",
                          validators:{
                            minItems:function($modelValue){

                              if($modelValue.length>0) return true;

                              return false;
                            }
                          }
                        }
                      ]
                    },
                    {
                      className: 'row',
                      fieldGroup: [
                        {
                          key:"tableItemsCLP",
                          type:"simpleTable",
                          templateOptions:{
                            totalKey:"totalCLP",
                            condition:function(objeto){
                              if(objeto.tipoMoneda)
                                if(objeto.tipoMoneda.id == 2)return true;

                              return false;
                            },
                            title:"Resumen Items $(CLP)",
                            targetkey:"cubicadorOrdinario",
                            columns:[
                              {
                                field: "item",
                                title: 'Items',
                                sortable: "item",
                                visible: true
                              },
                              {
                                field: "cantidad",
                                title: 'Cantidad',
                                sortable: "name",
                                visible: true,
                                class:"text-center",
                                groupable: "name"
                              },
                              {
                                field: "precio",
                                title: 'Precio',
                                currency:true,
                                class:"text-center",
                                sortable: "precio",
                                visible: true
                              },
                              {
                                expression:function(objeto){
                                  var total = 0;
                                  var cantidad = 0;
                                  if(objeto.cantidad){ cantidad = objeto.cantidad}
                                  total = cantidad * objeto.precio;
                                  objeto.total = total;
                                  return total;
                                },
                                field: "total",
                                class:"text-center",
                                currency:true,
                                title: 'Total',
                                sortable: "total",
                                visible: true
                              }]
                          },
                          hideExpression:"model.contrato.id !== 2"
                        }
                      ]
                    },
                    {
                      className: 'row',
                      fieldGroup: [
                        {
                          key:"tableItemsUS",
                          type:"simpleTable",
                          templateOptions:{
                            totalKey:"totalUS",
                            title:"Resumen Items US$",
                            condition:function(objeto){
                              if(objeto.tipoMoneda)
                                if(objeto.tipoMoneda.id == 1)return true;

                              return false;
                            },
                            targetkey:"cubicadorOrdinario",
                            columns:[
                              {
                                field: "item",
                                title: 'Items',
                                sortable: "item",
                                visible: true
                              },
                              {
                                field: "cantidad",
                                title: 'Cantidad',
                                sortable: "name",
                                visible: true,
                                class:"text-center",
                                groupable: "name"
                              },
                              {
                                field: "precio",
                                title: 'Precio',
                                sortable: "precio",
                                currency:true,
                                currencySimbol:"US$",
                                class:"text-center",
                                visible: true
                              },
                              {
                                expression:function(objeto){
                                  var total = 0;
                                  var cantidad = 0;
                                  if(objeto.cantidad){ cantidad = objeto.cantidad}
                                  total = cantidad * objeto.precio;
                                  objeto.total = total;
                                  return total;
                                },
                                field: "total",
                                title: 'Total',
                                sortable: "total",
                                currency:true,
                                currencySimbol:"US$",
                                class:"text-center",
                                visible: true
                              }
                            ]
                          },
                          hideExpression:"model.contrato.id !== 2"
                        }
                      ]
                    },
                    {
                      className: 'row',
                      fieldGroup: [
                        {
                          key:"tableItemsUF",
                          type:"simpleTable",
                          templateOptions:{
                            totalKey:"totalUF",
                            title:"Resumen Items UF",
                            targetkey:"cubicadorOrdinario",
                            condition:function(objeto){
                              if(objeto.tipoMoneda)
                                if(objeto.tipoMoneda.id == 3)return true;

                              return false;
                            },
                            columns:[
                              {
                                field: "item",
                                title: 'Items',
                                sortable: "item",
                                visible: true
                              },
                              {
                                field: "cantidad",
                                title: 'Cantidad',
                                sortable: "name",
                                visible: true,
                                class:"text-center",
                                groupable: "name"
                              },
                              {
                                field: "precio",
                                title: 'Precio',
                                sortable: "precio",
                                "sufijo":"UF",
                                class:"text-center",
                                visible: true
                              },{
                                expression:function(objeto){
                                  var total = 0;
                                  var cantidad = 0;
                                  if(objeto.cantidad){ cantidad = objeto.cantidad}
                                  total = cantidad * objeto.precio;
                                  objeto.total = total;
                                  return total;
                                },
                                field: "total",
                                title: 'Total',
                                sortable: "total",
                                sufijo:"UF",
                                class:"text-center",
                                visible: true
                              }]
                          },
                          hideExpression:"model.contrato.id !== 2"
                        }
                      ]
                    },
                    {
                      className: 'row',
                      fieldGroup: [
                        {
                          key:"tableItemsEURO",
                          type:"simpleTable",
                          templateOptions:{
                            totalKey:"totalEURO",
                            title:"Resumen Items EURO",
                            targetkey:"cubicadorOrdinario",
                            condition:function(objeto){
                              if(objeto.tipoMoneda)
                                if(objeto.tipoMoneda.id == 4)return true;

                              return false;
                            },
                            columns:[
                              {
                                field: "item",
                                title: 'Items',
                                sortable: "item",
                                visible: true
                              },
                              {
                                field: "cantidad",
                                title: 'Cantidad',
                                sortable: "name",
                                visible: true,
                                class:"text-center",
                                groupable: "name"
                              },
                              {
                                field: "precio",
                                title: 'Precio',
                                sortable: "precio",
                                currency:true,
                                currencySimbol:"€",
                                class:"text-center",
                                visible: true
                              },{
                                expression:function(objeto){
                                  var total = 0;
                                  var cantidad = 0;
                                  if(objeto.cantidad){ cantidad = objeto.cantidad}
                                  total = cantidad * objeto.precio;
                                  objeto.total = total;
                                  return total;
                                },
                                field: "total",
                                title: 'Total',
                                sortable: "total",
                                currency:true,
                                currencySimbol:"€",
                                class:"text-center",
                                visible: true
                              }]
                          },
                          hideExpression:"model.contrato.id !== 2"
                        }
                      ]
                    }
                  ]
                }

              ],
              tituloEditar:"Editar Costeo",
              tituloCrear: "Nuevo Costeo",
              metodos:{editar:"Cubicador"},
              action:'crearCubicacion',
              modalSize:"xl",
              backdrop:'static',
              hideEditar:true,
              hideRemove:true
            };
          }
        }
      })

    //.state('cubicacion-clonar', {
    //  url:"/cubicacion-clonar",
    //  templateUrl: 'views/administracion.html',
    //  controller: 'AdministracionCtrl',
    //  data:{
    //    authorizedRoles: [USER_ROLES.logged]
    //  },
    //  resolve:{
    //    titulo:function(){
    //      return "Gestion de Cubicación";
    //    },
    //    tabData:function(){
    //      return [
    //        {
    //          heading: 'Clonar',
    //          route:   'cubicacion-clonar.listar'
    //        }
    //      ];
    //    }
    //  }
    //})
    //.state('cubicacion-clonar.listar', {
    //  url:'/listar',
    //  templateUrl: 'views/templates/configuracion-component-template.html',
    //  controller: 'ConfiguracionComponentCtrl',
    //  resolve:{
    //    leyenda:function(){
    //      return "Cubicaciones";
    //    },
    //    tituloBoton: function(){
    //      return "Clonarrrr Cubicacion";
    //    },
    //    metodos: function(){
    //      return {
    //        listar:"TipoEstadoOt"};
    //    },
    //    tableHeader: function(tableTipoHeaderDefault){
    //      return tableTipoHeaderDefault;
    //    },
    //    opcionesCrearEditar: function(tipoDefaultForm){
    //      return {
    //        formulario:tipoDefaultForm,
    //        tituloEditar:"Editar Estado de Orden de Trabajo",
    //        tituloCrear: "Nuevo Estado de Orden de Trabajo",
    //        metodos:{crear:"TipoEstadoOt",editar:"TipoEstadoOt"}
    //      };
    //    }
    //  }
    //})




  });


