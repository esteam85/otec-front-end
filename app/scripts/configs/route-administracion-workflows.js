angular.module('otecApp')
  .config(function ($stateProvider,USER_ROLES) {

    $stateProvider
      .state('administracion-workflows', {
        url:"/administracion-workflows",
        templateUrl: 'views/administracion.html',
        controller: 'AdministracionCtrl',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        resolve:{
          titulo:function(){
            return "Administración de Workflow"
          },
          tabData:function(){
            return [
              {
                heading: 'Workflows',
                route:   'administracion-workflows.workflows'
              },
              {
                heading: 'Contratos',
                route:   'administracion-workflows.contratos'
              },
              //{
              //  heading: 'Agencias',
              //  route:   'administracion-workflows.agencias'
              //},
              //{
              //  heading: 'Especialidades',
              //  route:   'administracion-workflows.especialidades'
              //},
              {
                heading: 'Eventos',
                route:   'administracion-workflows.eventos'
              },
              {
                heading: 'Acciones',
                route:   'administracion-workflows.acciones'
              },
              {
                heading: 'Paramentros',
                route:   'administracion-workflows.parametros'
              }
            ];
          }
        }
      })
      .state('administracion-workflows.workflows', {
        url:'/workflows',
        templateUrl: 'views/administracion-item.html',
        controller: 'AdministracionItemCtrl',
        resolve:{
          tabData:function(){
            return [
              {
                heading: 'Workflows',
                route:   'administracion-workflows.workflows.lista-workflows',
                default:true
              },
              {
                heading: 'Estados Workflow',
                route:   'administracion-workflows.workflows.lista-tipo-estado-workflow'
              }
            ];
          }
        }
      })
      .state('administracion-workflows.workflows.lista-workflows', {
        url: "/lista-workflows",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Workflows";
          },
          tituloBoton: function(){
            return "Crear Workflow";
          },
          metodos: function(){
            return {
              listar:"Workflow"};
          },
          tableHeader: function(workflowHeaderTable){
            return workflowHeaderTable;
          },
          opcionesCrearEditar: function(){
            return {templateUrl: 'views/templates/crear-workflow-eventos.html', controller: 'CrearWorkflowEventosCtrl'};
          }

        }
      })
      .state('administracion-workflows.workflows.lista-tipo-estado-workflow', {
        url:'/lista-tipo-estado-workflow',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Estados de Workflow";
          },
          tituloBoton: function(){
            return "Crear Estado de Workflow";
          },
          metodos: function(){
            return {
              listar:"EstadoWorkflow"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            return tableTipoHeaderDefault;
          },
          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:tipoDefaultForm,
              tituloEditar:"Editar Estado de Workflow",
              tituloCrear: "Nuevo Estado de Workflow",
              metodos:{crear:"EstadoWorkflow",editar:"EstadoWorkflow"}
            };
          }
        }
      })

      .state('administracion-workflows.contratos', {
        url:'/contratos',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Contratos";
          },
          tituloBoton: function(){
            return "Crear Contrato";
          },
          metodos: function(){
            return {
              listar:"Contrato"};
          },
          tableHeader: function(contratoHeaderTable){
            return contratoHeaderTable;
          },
          opcionesCrearEditar: function(){
            return {
              formulario:[{
                key: 'nombre',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombre',
                  placeholder: 'Nombre',
                  required: true
                }
              },{
                "key": "workflow",
                "type": "horizontalUiSelect",
                "templateOptions": {
                  "placeholder": "Workflow",
                  "label": "Workflow",
                  "labelProp": "nombre",
                  "metodo":"Workflow",
                  required: true

                }
              },
                {
                  "key": "tipoPlanta",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Tipo de Planta",
                    "label": "Tipo de Planta",
                    valueProp: 'id',
                    "labelProp": "nombre",
                    options: [{id:1,nombre:'Interna'},{id:2,nombre:'Externa'}],
                    required: true

                  }
                }
                ,{
                "key": "fechaInicio",
                "type": "horizontalDatepicker",
                "templateOptions": {
                  "fecha":true,
                  "label": "Fecha Inicio",
                  "type": "text",
                  "datepickerPopup": "dd-MM-yyyy",
                  required: true
                }
              },{
                "key": "fechaTermino",
                "type": "horizontalDatepicker",
                "templateOptions": {
                  "fecha":true,
                  "label": "Fecha Término",
                  "type": "text",
                  "datepickerPopup": "dd-MM-yyyy",
                  required: true
                }
              },
                {
                  "key": "contratosProveedoreses",
                  "type": "horizontalUiMultiSelectInput",
                  "templateOptions": {
                    "subkey":"proveedores",
                    "placeholder": "Proveedores",
                    "label": "Proveedores",
                    "labelProp": "nombre",
                    "metodo":"Proveedor",
                    required: true,
                    "input":{label:"Cod Acuerdo", "placeholder": "Código de Acuerdo",inputProp:"codigoAcuerdo",required:true,type:"number"}
                  }
                },
                {
                  "key": "contratosAgencias",
                  "type": "horizontalUiMultiSelect",
                  "templateOptions": {
                    "placeholder": "Agencias",
                    "label": "Agencia",
                    "valueProp": "id",
                    "labelProp": "nombre",
                    "metodo":"Agencia"
                  }
                }
                ,{
                "key": "estado",
                "type": "horizontalRadios",
                "templateOptions": {
                  "label": "Estado",
                  "options": [
                    {
                      "name": "Activo",
                      "value": "A"
                    },
                    {
                      "name": "Inactivo",
                      "value": "I"
                    }
                  ]}
              }
              ],
              tituloEditar:"Editar Contrato",
              tituloCrear: "Nuevo Contrato",
              metodos:{crear:"Contrato",editar:"Contrato"}
            };
          }
        }
      })
      //.state('administracion-workflows.agencias', {
      //  url:'/agencias',
      //  templateUrl: 'views/templates/configuracion-component-template.html',
      //  controller: 'ConfiguracionComponentCtrl',
      //  resolve:{
      //    leyenda:function(){
      //      return "Agencias";
      //    },
      //    tituloBoton: function(){
      //      return "Crear Agencia";
      //    },
      //    metodos: function(){
      //      return {
      //        listar:"Agencia"};
      //    },
      //    tableHeader: function(tableTipoHeaderDefault){
      //      return tableTipoHeaderDefault;
      //    },
      //    opcionesCrearEditar: function(){
      //      return {
      //        formulario:[{
      //          key: 'nombre',
      //          type: 'horizontalInput',
      //          templateOptions: {
      //            label: 'Nombre',
      //            placeholder: 'Nombre',
      //            required: true
      //          }
      //        },{
      //          "key": "descripcion",
      //          "type": "horizontalInput",
      //          "templateOptions": {
      //            label: 'Descripcion',
      //            placeholder: 'Descripcion',
      //            required: true
      //          }
      //        },
      //          //{
      //          //"key": "agenciasEspecialidadeses",
      //          //"type": "horizontalUiMultiSelect",
      //          //"templateOptions": {
      //          //  "placeholder": "Especialidades",
      //          //  "label": "Especialidades",
      //          //  "valueProp": "id",
      //          //  "labelProp": "nombre",
      //          //  "metodo":"Especialidad"
      //          // }
      //          //},
      //           {
      //              "key": "estado",
      //              "type": "horizontalRadios",
      //              "templateOptions": {
      //                "label": "Estado",
      //                "options": [
      //                  {
      //                    "name": "Activo",
      //                    "value": "A"
      //                  },
      //                  {
      //                    "name": "Inactivo",
      //                    "value": "I"
      //                  }
      //                ]}
      //            }
      //        ],
      //        tituloEditar:"Editar Agencia",
      //        tituloCrear: "Nuevo Agencia",
      //        metodos:{crear:"Agencia",editar:"Agencia"}
      //      };
      //    }
      //  }
      //})
      .state('administracion-workflows.especialidades', {
        url:'/especialidades',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Especialidades";
          },
          tituloBoton: function(){
            return "Crear Especialidad";
          },
          metodos: function(){
            return {
              listar:"Especialidad"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            return tableTipoHeaderDefault;
          },
          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:tipoDefaultForm,
              tituloEditar:"Editar Especialidad",
              tituloCrear: "Nueva Especialidad",
              metodos:{crear:"Especialidad",editar:"Especialidad"}
            };
          }
        }
      })
      .state('crear-workflow', {
        url:"/crear-workflow",
        templateUrl: 'views/templates/crear-editar-workflow.html',
        controller: 'CrearWorkflowCtrl'
      })
      .state('administracion-workflows.eventos', {
        url:'/eventos',
        templateUrl: 'views/administracion-item.html',
        controller: 'AdministracionItemCtrl',
        resolve:{
          tabData:function(){
            return  [
              {
                heading: 'Eventos',
                route:   'administracion-workflows.eventos.lista-eventos',
                default:true
              },
              {
                heading: 'Tipo de Eventos',
                route:   'administracion-workflows.eventos.lista-tipo-eventos'
              },
              {
                heading: 'Mensajes',
                route:   'administracion-workflows.eventos.lista-mensajes-eventos'
              },
              {
                heading: 'Decisiones',
                route:   'administracion-workflows.eventos.lista-decisiones'
              },

            ];
          }
        }
      })
      .state('administracion-workflows.eventos.lista-eventos', {
        url:'/lista-eventos',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Eventos";
          },
          tituloBoton: function(){
            return "Crear Evento";
          },
          metodos: function(){
            return {
              listar:"Evento"};
          },
          tableHeader: function(eventoHeaderTable){
            return  eventoHeaderTable;
          },
          opcionesCrearEditar: function(){
            return {
              formulario:[{
                key: 'nombre',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombre',
                  placeholder: 'Nombre',
                  required: true
                }
              },
                {
                  "key": "tipoEvento",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Tipo de Evento",
                    "label": "Tipo",
                    "valueProp": "id",
                    "labelProp": "nombre",
                    "metodo":"TipoEvento",
                    required: true
                  }
                },
                {
                  "key": "mensajeEvento",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Mensaje de notificación",
                    "label": "Mensaje",
                    "valueProp": "id",
                    "labelProp": "mensajeNotificacion",
                    "metodo":"MensajeEvento",
                    required: true
                  }},
                {
                  "key": "eventosAccioneses",
                  "type": "horizontalUiMultiSelect",
                  "templateOptions": {
                    "placeholder": "Acciones",
                    "label": "Acciones",
                    "valueProp": "id",
                    "labelProp": "nombre",
                    "metodo":"Accion",
                    required: true
                  }
                },
                {
                  "key": "eventosAlarmas",
                  "type": "horizontalUiMultiSelectForm",
                  "templateOptions": {
                    "subkey":"alarmas",
                    "placeholder": "Alarmas",
                    "label": "Alarmas",
                    "valueProp": "id",
                    "labelProp": "nombre",
                    "metodo":"Alarma",
                    "subForm":[
                      {
                        key: 'descripcion',
                        type: 'input',
                        templateOptions: {
                          label: 'Descipción',
                          placeholder: 'Descipción',
                          required: true
                        }
                      },
                      {
                        "key": "estadoAlarma",
                        "type": "checkboxActivar",
                        "templateOptions": {
                          "label": "Activar"
                      }
                }]}},
                {
                  key: 'duracion',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Duración',
                    placeholder: 'Duración en días',
                    type: 'number',
                    required: true
                  }
                }],
              tituloEditar:"Editar Evento",
              tituloCrear: "Nuevo Evento",
              metodos:{crear:"Evento",editar:"Evento"}
            };
          }
        }
      })
      .state('administracion-workflows.eventos.lista-tipo-eventos', {
        url: "/lista-tipo-eventos",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Tipos de Evento";
          },
          tituloBoton: function(){
            return "Crear Tipo de Evento";
          },
          metodos: function(){
            return {
              listar:"TipoEvento"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            return tableTipoHeaderDefault;
          },
          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:tipoDefaultForm,
              tituloEditar:"Editar Tipo de Evento",
              tituloCrear: "Nuevo Tipo de Evento",
              metodos:{crear:"TipoEvento",editar:"TipoEvento"}
            };
          }
        }
      })
      .state('administracion-workflows.eventos.lista-mensajes-eventos', {
        url:'/lista-mensajes-eventos',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return  "Mensaje de Notificación de Eventos";
          },
          tituloBoton: function(){
            return "Crear Mensaje";
          },
          metodos: function(){
            return {
              listar:"MensajeEvento"};
          },
          tableHeader: function(){
            return [{ title: 'Mensaje', field: 'mensajeNotificacion', visible: true, filter: { 'name': 'text' }}];
          },
          opcionesCrearEditar: function(){
            return {
              formulario:[
                {
                  key: 'mensajeNotificacion',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Mensaje',
                    placeholder: 'Mensaje de Notificación',
                    required: true,
                    "rows": 1,
                    "cols": 2
                  }
                }
              ],
              tituloEditar:"Editar Mensaje",
              tituloCrear: "Nuevo Mensaje",
              metodos:{crear:"MensajeEvento",editar:"MensajeEvento"}
            };
          }
        }
      })
      .state('administracion-workflows.eventos.lista-decisiones', {
        url:'/lista-decisiones',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return  "Decisión";
          },
          tituloBoton: function(){
            return "Crear Decisión";
          },
          metodos: function(){
            return {
              listar:"Decision"};
          },
          tableHeader: function(){
            return [{ title: 'Opcion', field: 'respuesta', visible: true, filter: { 'name': 'text' }}];
          },
          opcionesCrearEditar: function(){
            return {
              formulario:[
                {
                  key: 'respuesta',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Opción',
                    placeholder: 'Opción',
                    required: true,
                    "rows": 1,
                    "cols": 2
                  }
                }
              ],
              tituloEditar:"Editar Decisión",
              tituloCrear: "Nuevo Decisión",
              metodos:{crear:"Decision",editar:"Decision"}
            };
          }
        }
      })

      .state('administracion-workflows.acciones', {
        url:'/acciones',
        templateUrl: 'views/administracion-item.html',
        controller: 'AdministracionItemCtrl',
        resolve:{
          tabData:function(){
            return  [
              {
                heading: 'Acciones',
                route:   'administracion-workflows.acciones.lista-acciones',
                default:true
              },
              {
                heading: 'Tipo de Acciones',
                route:   'administracion-workflows.acciones.lista-tipo-acciones'
              }
            ];
          }
        }
      })
      .state('administracion-workflows.acciones.lista-acciones', {
        url:'/lista-acciones',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Acciones";
          },
          tituloBoton: function(){
            return "Crear Acción";
          },
          metodos: function(){
            return {
              listar:"Accion"};
          },
          tableHeader: function(accionHeaderTable){
            return accionHeaderTable;
          },
          opcionesCrearEditar: function(){


            return {
              formulario:[{
                key: 'nombre',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombre',
                  placeholder: 'Nombre',
                  required: true
                }
              },{
                "key": "tipoAccion",
                "type": "horizontalUiSelect",
                "templateOptions": {
                  "placeholder": "Tipo de Acción",
                  "label": "Tipo",
                  "valueProp": "id",
                  "labelProp": "nombre",
                  "metodo":"TipoAccion",
                  required: true
                }
              },{
                "key": "accionesParametroses",
                "type": "horizontalUiMultiSelect",
                "templateOptions": {
                  "placeholder": "Parámetros",
                  "label": "Parámetros",
                  "valueProp": "id",
                  "labelProp": "nombre",
                  "metodo":"Parametro",
                  required: true
                }
              }
              ],
              tituloEditar:"Editar Acción",
              tituloCrear: "Nueva Acción",
              metodos:{crear:"Accion",editar:"Accion"}
            };
          }
        }
      })
      .state('administracion-workflows.acciones.lista-tipo-acciones', {
        url: "/lista-tipo-acciones",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Tipos de Acción";
          },
          tituloBoton: function(){
            return "Crear Tipo de Acción";
          },
          metodos: function(){
            return {
              listar:"TipoAccion"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            var tipoHeader = tableTipoHeaderDefault.slice();
            tipoHeader.push({ title: 'Configurable', field: 'configurable', visible: true,icon:true, filter: { 'name': 'text' },class:"text-center" });
            return tipoHeader;
          },
          opcionesCrearEditar: function(tipoDefaultForm){

            var tipoForm = tipoDefaultForm.slice();
            tipoForm.push({
              "type": "horizontalCheckbox",
              "key": "configurable",
              "templateOptions": {
                "label": "Configurable"
              }
            });
            return {
              formulario:tipoForm,
              metodos:{crear:"TipoAccion",editar:"TipoAccion"}
            };
          }
        }
      })
      .state('administracion-workflows.parametros', {
        url:'/parametros',
        templateUrl: 'views/administracion-item.html',
        controller: 'AdministracionItemCtrl',
        resolve:{
          tabData:function(){
            return [
              {
                heading: 'Parametros',
                route:   'administracion-workflows.parametros.lista-parametros',
                default:true
              },
              {
                heading: 'Tipo de Parametros',
                route:   'administracion-workflows.parametros.lista-tipo-parametros'
              }
            ];
          }
        }
      })
      .state('administracion-workflows.parametros.lista-parametros', {
        url:'/lista-parametros',
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Parámetros";
          },
          tituloBoton: function(){
            return "Crear Parámetro";
          },
          metodos: function(){
            return {
              listar:"Parametro"};
          },
          tableHeader: function(parametroHeaderTable){
            return parametroHeaderTable;
          },
          opcionesCrearEditar: function(formlyCustomOtecTypes){

            return {
              formulario:[{
                key: 'nombre',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombre',
                  placeholder: 'Nombre',
                  required: true
                }
              },{
                "key": "tipoParametro",
                "type": "horizontalUiSelect",
                "templateOptions": {
                  "placeholder": "Parámetro",
                  "valueProp": "id",
                  "label":"Tipo Parámentro",
                  "labelProp": "nombre",
                  "metodo":"TipoParametro"
                }
              }
                ,{
                key: 'descripcion',
                type: 'horizontalTextarea',
                templateOptions: {
                  label: 'Descripción',
                  placeholder: 'Descripción...',
                  required: true
                }
              }
              //,{
              //  key: 'detalleParametros',
              //  type: 'keyValueTable',
              //  templateOptions: {
              //    defaults:[{llave:'key',"isTemplateOption":false,obligatorio:true},{llave:'type',valorForm:[{
              //        "key": "valor",
              //        "type": "UISelectTable",
              //        "templateOptions": {
              //          "placeholder": "Parámetro",
              //          "valueProp": "nombre",
              //          "options":formlyCustomOtecTypes,
              //          "label":"",
              //          "labelProp": "nombre"
              //        }
              //      }],"isTemplateOption":false,obligatorio:true},{llave:'label',"isTemplateOption":true,obligatorio:true}],
              //    requerid:true,
              //    columns:[{ title: 'Llave',type:"input", field: 'llave', visible: true},
              //      { title: 'Valor',type:"formly", field: 'valor', visible: true}]
              //  }
              //}
              ],
              modalSize: "lg",
              tituloEditar:"Editar Parámetro",
              tituloCrear: "Nueva Parámetro",
              metodos:{crear:"Parametro",editar:"Parametro"}
            };
          }
        }
      })
      .state('administracion-workflows.parametros.lista-tipo-parametros', {
        url: "/lista-tipo-parametros",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Tipos de Parámetro";
          },
          tituloBoton: function(){
            return "Crear Tipo de Parámetro";
          },
          metodos: function(){
            return {
              listar:"TipoParametro"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            return tableTipoHeaderDefault;
          },
          opcionesCrearEditar: function(tipoDefaultForm){

            return {
              formulario:tipoDefaultForm,
              metodos:{crear:"TipoParametro",editar:"TipoParametro"}
            };
          }
        }
      })

  });
