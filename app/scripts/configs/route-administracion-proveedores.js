angular.module('otecApp')
  .config(function ($stateProvider,USER_ROLES) {

    $stateProvider
      .state('administracion-proveedores', {
        url:"/administracion-proveedores",
        templateUrl: 'views/administracion.html',
        controller: 'AdministracionCtrl',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        resolve:{
          titulo:function(){
            return "Administración de Proveedores";
          },
          tabData:function(){
            return [
              {
                heading: 'Proveedores',
                route:   'administracion-proveedores.proveedores'
              }
            ];
          }
        }
      })

      .state('administracion-proveedores.proveedores', {
        url:"/proveedores",
        templateUrl: 'views/administracion-item.html',
        controller: 'AdministracionItemCtrl',
        resolve:{
          tabData:function(){
            return [
              {
                heading: 'Proveedores',
                route:   'administracion-proveedores.proveedores.lista-proveedores',
                default:true
              },
              {
                heading: 'Tipos de Proveedor',
                route:   'administracion-proveedores.proveedores.lista-tipo-proveedores'
              }
            ];
          }
        }
      })
      .state('administracion-proveedores.proveedores.lista-proveedores', {
        url: "/lista-proveedores",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Proveedores";
          },
          tituloBoton: function(){
            return "Crear Proveedor";
          },
          metodos: function(){
            return {
              listar:"Proveedor"};
          },
          tableHeader: function(proveedorHeaderTable){
            return  proveedorHeaderTable;
          },
          opcionesCrearEditar: function(){
            return {
              formulario:[{
                key: 'nombre',
                type: 'horizontalInput',
                templateOptions: {
                  label: 'Nombre',
                  placeholder: 'Nombre',
                  required: true
                }
              },{
                "key": "tipoProveedor",
                "type": "horizontalUiSelect",
                "templateOptions": {
                  "placeholder": "Tipo Proveedor",
                  "label": "Tipo",
                  "valueProp": "id",
                  "labelProp": "nombre",
                  "metodo":"TipoProveedor",
                  required: true

                }
              },{
                "key": "rut",
                type: 'horizontalInputRut',
                "templateOptions": {
                  "placeholder": "Rut",
                  "label": "Rut",
                  required: true
                }
              },{
                "key": "email",
                type: 'horizontalInput',
                "templateOptions": {
                  "placeholder": "Email",
                  "label": "Email",
                  "type": "email",
                  required: true

                }
              },
                {
                "key": "telefono",
                type: 'horizontalInputMask',
                templateOptions: {
                  "label": 'Teléfono',
                  "placeholder": "Teléfono",
                  mask: '(9999) 99999999',
                  required: true
                }
              },{
                "key": "direccion",
                type: 'horizontalInput',
                "templateOptions": {
                  "placeholder": "Dirección",
                  "label": "Dirección",
                  required: true

                }
              }
                //,
              //  {
              //  "key": "serviciosProveedores",
              //  "type": "horizontalUiMultiSelect",
              //  "templateOptions": {
              //    "placeholder": "Servicios",
              //    "label": "Servicios",
              //    "valueProp": "id",
              //    "labelProp": "nombre",
              //    "metodo":"Servicio",
              //    required: true
              //  }
              //}
                ,{
                "key": "estado",
                "type": "horizontalRadios",
                "templateOptions": {
                  "label": "Estado",
                  "options": [
                    {
                      "name": "Activo",
                      "value": "A"
                    },
                    {
                      "name": "Inactivo",
                      "value": "I"
                    }
                  ]}
              }
              ],
              tituloEditar:"Editar Proveedor",
              tituloCrear: "Nuevo Proveedor",
              metodos:{crear:"Proveedor",editar:"Proveedor"}
            };
          }
        }
      })
      .state('administracion-proveedores.proveedores.lista-tipo-proveedores', {
        url: "/lista-tipo-proveedores",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Tipo de Proveedores";
          },
          tituloBoton: function(){
            return "Crear Tipo Proveedor";
          },
          metodos: function(){
            return {
              listar:"TipoProveedor"};
          },
          tableHeader: function(tableTipoHeaderDefault){
            return tableTipoHeaderDefault;
          },

          opcionesCrearEditar: function(tipoDefaultForm){
            return {
              formulario:tipoDefaultForm,
              tituloEditar:"Editar Tipo Proveedor",
              tituloCrear: "Nuevo Tipo Proveedor",
              metodos:{crear:"TipoProveedor",editar:"TipoProveedor"}
            };
          }
        }
      });


  });
