angular.module('otecApp')

  .config(function(formlyConfigProvider){

    formlyConfigProvider.setWrapper({
      name: 'horizontalBootstrapLabel',
      template: [
        '<label for="{{::id}}" class="col-lg-3 control-label">',
        '{{to.label}}{{to.required ? "*" : ""}}',
        '</label>',
        '<div class="col-lg-9">',
        '<div class="row">',
        '<formly-transclude></formly-transclude>',
        '</row>',
        '</div>'
      ].join(' ')
    });
    formlyConfigProvider.setWrapper({
      name: 'horizontalBootstrapNoLabel',
      template: [
        '<div class="col-lg-12">',
        '<formly-transclude></formly-transclude>',
        '</div>'
      ].join(' ')
    });

    formlyConfigProvider.setType({
      name: 'maskedInput',
      extends: 'input',
      template: '<input class="form-control" ng-model="model[options.key]" />',
      defaultOptions: {
        ngModelAttrs: {
          mask: {
            attribute: 'ui-mask'
          },
          maskPlaceholder: {
            attribute: 'ui-mask-placeholder'
          },
          modelViewValue:{
            attribute:"model-view-value"
          }
        },
        templateOptions: {
          maskPlaceholder: ''
        }
      }
    });

    formlyConfigProvider.setType({
      name: 'input-file',
      extends: 'input',
      template: '<input type="file" file-model="myFile" />'

    });

    //on-select="to.onChange($item, model)"
    formlyConfigProvider.setType({
      name: 'ui-select',
      extends: 'select',
      template:'<div tooltip="{{to.add.toolTip}}" ng-class="{\'col-lg-1\':!!to.add}" ng-if="!!to.add" ng-click="add()" class="btn btn-info"><i class="fa fa-plus-circle"></i></div>' +
      '<div ng-class="{\'col-lg-11 no-padding-right\':!!to.add}">'+
      '<ui-select  ng-model="model[options.key]"  theme="bootstrap" ng-required="{{to.required}}" ng-disabled="{{to.disabled}}" reset-search-input="false">' +
      ' <ui-select-match placeholder="{{to.placeholder}}"> {{$select.selected[to.labelProp || \'name\']}} </ui-select-match> ' +
      '<ui-select-choices group-by="to.groupBy" repeat="option as option in to.options | filter: $select.search"> ' +
      '<div ng-bind-html="option[to.labelProp || \'name\'] | highlight: $select.search"></div> ' +
      '</ui-select-choices>' +
      ' </ui-select>'+
      '<general-loader ng-show="generalLoader" ng-class="{\'spinner-general-loader-hidden\':!generalLoader}"></general-loader>'+
      '</div>'
    });

    formlyConfigProvider.setType({
      name: 'ui-select-table',
      extends: 'select',
      template: '<ui-select ng-model="model[options.key]" theme="bootstrap" ng-required="{{to.required}}" ng-disabled="{{to.disabled}}" reset-search-input="false">' +
      ' <ui-select-match placeholder="{{to.placeholder}}"> {{$select.selected}} </ui-select-match> ' +
      '<ui-select-choices group-by="to.groupBy" repeat="option in to.options | filter: $select.search"> ' +
      '<div ng-bind-html="option | highlight: $select.search"></div> ' +
      '</ui-select-choices>' +
      ' </ui-select>'
      ,
      controller:['$scope','$log','tipoService',function($scope,$log,tipoService){

      }]});

    formlyConfigProvider.setType({
      name: 'input-rut',
      extends: 'input',
      template: '<input ng-rut class="form-control" dv ng-model="model[options.key]" placeholder=""{{to.placeholder}}"" ng-required="{{to.required}}">'
    });

    formlyConfigProvider.setType({
      name: 'ui-multi-select',
      extends: 'select',
      template: '<ui-select ng-required="{{to.required}}" sortable="to.sortable" on-select="soloId($item, model[options.key]);ordenar()" ui-select-required="{{to.required}}" multiple ng-model="model[options.key]" theme="bootstrap" ng-disabled="{{to.disabled}}" reset-search-input="false">' +
      ' <ui-select-match ui-lock-choice="$item.inactive == true" placeholder="{{to.placeholder}}"> <span ng-show="to.sortable"><strong ng-show="$item.orden">Orden: {{$item.orden}}º - </strong></span>{{$item[to.labelProp]}} </ui-select-match> ' +
      '<ui-select-choices group-by="to.groupBy" repeat="option as option in to.options | filter: $select.search"> ' +
      '<div ng-bind-html="option[to.labelProp || \'name\'] | highlight: $select.search"></div> ' +
      '</ui-select-choices>' +
      ' </ui-select>'+
      '<general-loader-multi ng-show="generalLoader" ng-class="{\'spinner-general-loader-hidden\':!generalLoader}"></general-loader-multi>'
    });


    formlyConfigProvider.setType({
      name: 'ui-multi-select-input',
      extends: 'select',
      template: '<ui-select required="{{to.required}}" ui-select-required="{{to.required}}" multiple ng-model="model[options.key]" on-select="borrarSub($item, model[options.key]);onSelectForInput($item,model[options.key],this)" theme="bootstrap"  ng-disabled="{{to.disabled}}" reset-search-input="false" >' +
      ' <ui-select-match placeholder="{{to.placeholder}}" > {{$item[to.subkey][to.labelProp]}} <span>, {{to.input.label}}: <input type="{{to.input.type}}" min="{{to.input.min}}" max="{{to.input.max}}" style="color:#555555;"  ng-model="$item[to.input.inputProp]" placeholder="{{to.input.placeholder}}" required="{{to.input.required}}"/></span></ui-select-match>' +
      '<ui-select-choices group-by="to.groupBy" repeat="option as option in to.options | filter: $select.search"> ' +
      '<div ng-bind-html="option[to.subkey][to.labelProp || \'name\'] | highlight: $select.search"></div> ' +
      '</ui-select-choices>' +
      ' </ui-select>'+
      '<general-loader-multi ng-show="generalLoader" ng-class="{\'spinner-general-loader-hidden\':!generalLoader}"></general-loader-multi>'
    });

    formlyConfigProvider.setType({
      name: 'ui-multi-select-form',
      extends: 'select',
      templateUrl:'views/templates/ui-multi-select-form-template.html',
      controller:['$scope','tipoService',function($scope,tipoService){
        if(!$scope.to.options){
          $scope.to.options = [];

          tipoService.listar($scope.to.metodo).then(function(data){
            var dataEnvolved = [];
            _.each(data ,function(it){
              var newItem = {};
              newItem[$scope.to.subkey] = it;
              dataEnvolved.push(newItem);

            });
            $scope.to.options = dataEnvolved;
          });
        }
      }]
    });



    formlyConfigProvider.setWrapper({
      name: 'horizontalBootstrapCheckbox',
      template: [
        '<div class="col-sm-offset-3 col-lg-9">',
        '<formly-transclude></formly-transclude>',
        '</div>'
      ].join(' ')
    });

    formlyConfigProvider.setType({
      name: 'horizontalInput',
      extends: 'input',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalText',
      template:'<p ng-show="to.subAttrCascade" class="form-control-static">{{model[options.key][to.subAttrCascade]}}</p>\n<p ng-show="!to.subAttrCascade && to.attrCascade" class="form-control-static">{{model[options.key][to.attrCascade]}}</p>\n<p ng-show="!to.attrCascade && !to.subAttrCascade && to.currency" class="form-control-static">{{model[options.key] | currency}}</p>',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
      controller: ['$scope',function($scope) {

        var property = $scope.to.targetKey || $scope.options.key;
        $scope.$watch("model."+property,function(newValue,oldValue){


          $scope.generalLoader = false;
          if(!newValue) {

            if($scope.model[$scope.options.key] && oldValue) {
              $scope.model[$scope.options.key] = '';
            }
          }else{
            if($scope.to.parseAttr){
              newValue = JSON.parse(newValue[$scope.to.parseAttr]);
            }
            if($scope.to.subAttrCascade){
              $scope.model[$scope.options.key] =  newValue[$scope.to.attrCascade];
            }else{
              $scope.model[$scope.options.key] =  newValue;
            }
          }
        });
      }]
    });

    formlyConfigProvider.setType({
      name: 'horizontalSimpleText',
      template:'<p class="form-control-static">{{model[options.key] | picker:to.formatter}}</p>',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalSimpleTextNoLabel',
      template:'<p ng-class="{{to.ngClass}}" class="form-control-static {{to.class}}" ng-bind-html="model[options.key]"></p>'
    });

    formlyConfigProvider.setType({
      name: 'UISelectTable',
      extends: 'ui-select-table',
      wrapper: ['bootstrapHasError']
    });
    formlyConfigProvider.setType({
      name: 'horizontalTextarea',
      extends: 'textarea',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
      defaultOptions: {
        ngModelAttrs: {
          maxlength: {
            attribute: 'maxlength'
          }
        }
      }
    });

    formlyConfigProvider.setType({
      name: 'horizontalCheckbox',
      extends: 'checkbox',
      defaultOptions: {
        ngModelAttrs: {
          ngTrueValue: {
            attribute: 'ng-true-value'
          },
          ngFalseValue: {
            attribute: 'ng-false-value'
          }
        },
        templateOptions: {
          ngTrueValue: true,
          ngFalseValue: false
        }
      },
      link:function(scope, el, attrs){
        scope.$watch("model."+scope.options.key,function(newVal,oldVal){

          if(newVal === true){
            scope.model[scope.options.key] = true;
          }else{
            scope.model[scope.options.key] = false;
          }
        });

      },
      wrapper: ['horizontalBootstrapCheckbox', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalCheckboxNoLabel',
      extends: 'horizontalCheckbox',
      wrapper: ['bootstrapHasError']
    });


    formlyConfigProvider.setType({
      name: 'checkboxActivar',
      extends: 'checkbox',
      template:'<div class="checkbox">  <label> <input ng-true-value="\'A\'" ng-false-value="\'I\'" type="checkbox" class="formly-field-checkbox" ng-model="model[options.key]"> {{to.label}}{{to.required ? \'*\' : \'\'}}</label></div>'
      ,       wrapper: ['bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalRadios',
      extends: 'radio',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalUiSelect',
      extends: 'ui-select',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalUiSelectNoLabel',
      extends: 'ui-select',
      wrapper: ['bootstrapHasError']
    });


    formlyConfigProvider.setType({
      name: 'horizontalUiSelectGET',
      extends: 'ui-select',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
      controller:["$scope","tipoService","$timeout","$modal","$rootScope",function($scope,tipoService,$timeout,$modal,$rootScope){

        $scope.$watch("model."+$scope.options.key,function(newVal,oldVal){
          if(!!newVal){
            //if(!!$scope.to.onChange){
            //  $scope.to.onChange($scope.model,$scope);
            //}
            if(!!$scope.to.broadcastOnChange){
              $rootScope.$broadcast($scope.to.broadcastOnChange[0],$scope.to.broadcastOnChange[1]($scope.model,$rootScope,$scope));
            }
          }
        });

        $scope.add = function(){
          var modalInstance = $modal.open({
            templateUrl: 'views/templates/crear-editar.html',
            controller: 'ModalCrearEditarInstance',
            size: $scope.to.add.modalSize || '',
            backdrop:$scope.to.add.backdrop || true,
            resolve: {
              objeto: function () {
                return undefined;
              },
              opciones: function(){
                return $scope.to.add;
              },
              leyenda:function(){
                return undefined;
              }

            }
          });

          modalInstance.result.then(function (data) {
            console.log(data.objeto);
            $scope.to.options.push(data.objeto);
            $scope.model[$scope.options.key] = data.objeto;

          });
        };

        //$scope.soloId = function($item,model){
        //  if($scope.to.soloId){
        //
        //    var aux = {};
        //    aux[$scope.to.valueProp] = $item[$scope.to.valueProp];
        //    aux[$scope.to.labelProp] = $item[$scope.to.labelProp];
        //    $scope.model[$scope.options.key] = aux;
        //  }
        //};

        //if(!$scope.to.options){

        $scope.to.options = [];
        $scope.generalLoader = true;
        var parametros={};
        if( $scope.to.parametros){
          parametros = $scope.to.parametros($scope.model);
        }

        tipoService.listar($scope.to.metodo,$scope.to.action,parametros).then(function(data){

          if($scope.to.dataHandler){
            data = $scope.to.dataHandler(data);
          }

          if(!!$scope.to.rejectId){
            data = _.reject(data, function(item){ return item.id == $scope.to.rejectId; });
          }
          if($scope.to.concatAttr){

            _.each(data,function(item){

              item[$scope.to.labelProp]= item[$scope.to.labelProp] + " " + item[$scope.to.concatAttr];

            });
          }
          $timeout(function(){$scope.generalLoader = false;},500);
          $scope.to.options = data;
        });
        //}
      }]
    });

    formlyConfigProvider.setType({
      name: 'horizontalUiSelectGETNoLabel',
      extends: 'horizontalUiSelectGET',
      wrapper: ['bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalUiSelectGetCascade',
      extends: 'ui-select',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
      controller: ['$scope','tipoService','$timeout',function($scope, tipoService,$timeout) {
        $scope.$watch("model." + $scope.to.toWatch, function (newValue, oldValue, theScope) {
          $scope.generalLoader = false;
          if(newValue !== undefined || oldValue !== undefined) {
            // logic to reload this select's options asynchronusly based on state's value (newValue)
            if($scope.model[$scope.options.key] && oldValue) {
              // reset this select
              $scope.model[$scope.options.key] = '';
            }
            // Reload options
            if($scope.to.get){
              var parametros = undefined;
              $scope.generalLoader = true;
              if(typeof $scope.to.parametros == 'function'){
                parametros = $scope.to.parametros($scope.model);
              }
              if(typeof $scope.to.parametros == 'string'){
                parametros = $scope.$eval($scope.to.parametros);
              }
              $scope.to.loading = tipoService.listar($scope.to.metodo,$scope.to.action,parametros).then(function(data){
                if($scope.to.concatAttr){

                  _.each(data,function(item){

                    item[$scope.to.labelProp]= item[$scope.to.labelProp] + " " + item[$scope.to.concatAttr];

                  });
                }

                if($scope.to.concatAttrPrev){

                  _.each(data,function(item){

                    item[$scope.to.labelProp]= item[$scope.to.concatAttrPrev] + " - " + item[$scope.to.labelProp];

                  });
                }

                $scope.to.options = data;
                $timeout(function(){$scope.generalLoader = false;},500);
              });
            }else{
              var rol = $scope.model[$scope.to.attrCascade];
              $scope.to.options = $scope.model[$scope.to.attrCascade][$scope.to.subAttrCascade];
            }
          }
        });
      }]
    });

    formlyConfigProvider.setType({
      name: 'horizontalUiMultiSelect',
      extends: 'ui-multi-select',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
      controller:['$scope', function($scope) {
        $scope.soloId = function($item,model){
          if($scope.to.soloId){
            var aux = {};
            aux[$scope.to.valueProp] = $item[$scope.to.valueProp];
            aux[$scope.to.labelProp] = $item[$scope.to.labelProp];

            var index = _.indexOf($scope.model[$scope.options.key],_.findWhere($scope.model[$scope.options.key], $item));
            $scope.model[$scope.options.key][index] = aux;
          }
        }
      }]

    });

    formlyConfigProvider.setType({
      name: 'horizontalUiMultiSelectNoLabel',
      extends: 'ui-multi-select',
      wrapper: ['bootstrapHasError'],
      controller:['$scope', function($scope) {

        $scope.ordenar =  function (){
          _.each($scope.model[$scope.options.key],function(item,index){
            item.orden = index+1;
          });
        };


        $scope.$on('uiSelectSort:change', function(event, args) {
          $scope.model[$scope.options.key] = args.array;

          _.each($scope.model[$scope.options.key],function(item,index){
            item.orden = index+1;
          });
        });

        //$scope.soloId = function($item,model){
        //  if($scope.to.soloId){
        //    var aux = {};
        //    aux[$scope.to.valueProp] = $item[$scope.to.valueProp];
        //    aux[$scope.to.labelProp] = $item[$scope.to.labelProp];
        //
        //    var index = _.indexOf($scope.model[$scope.options.key],_.findWhere($scope.model[$scope.options.key], $item));
        //    $scope.model[$scope.options.key][index] = aux;
        //  }
        //}
      }]

    });

    formlyConfigProvider.setType({
      name: 'horizontalUiMultiSelectGET',
      extends: 'ui-multi-select',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
      controller:['$scope','tipoService','$timeout','$log', function($scope,tipoService,$timeout,$log) {

        $scope.$watch("model."+$scope.options.key,function(newVal,oldVal){
          if($scope.to.log){
            $log.debug("Selected "+ $scope.options.key,$scope.model[$scope.options.key]);
          }
        });
        $scope.soloId = function($item,model){
          if($scope.to.soloId){
            var aux = {};
            aux[$scope.to.valueProp] = $item[$scope.to.valueProp];
            aux[$scope.to.labelProp] = $item[$scope.to.labelProp];

            var index = _.indexOf($scope.model[$scope.options.key],_.findWhere($scope.model[$scope.options.key], $item));
            $scope.model[$scope.options.key][index] = aux;
          }
        };

        if(!$scope.to.options){
          var parametros={};
          if( $scope.to.parametros){
            parametros = $scope.to.parametros($scope.model);
          }

          $scope.to.options = [];
          $scope.generalLoader = true;
          if(!$scope.to.referenceId){

            tipoService.listar($scope.to.metodo,$scope.to.action,parametros).then(function(data){
              $scope.to.options = data;

              if(typeof $scope.to.defaultFunction === "function"){
                $scope.model[$scope.options.key] = $scope.to.defaultFunction($scope.model[$scope.options.key],$scope.to.options,$scope);
              }

              if($scope.to.log){
                $log.debug("Options "+ $scope.options.key,$scope.to.options);
              }
              $timeout(function(){$scope.generalLoader = false;},500);
            });
          }else{
            tipoService.listarPorIdChris($scope.to.action,$scope.to.referenceIdName,$scope.to.referenceId).then(function(data){
              var resultados = JSON.parse(data[$scope.to.attrToParse]);
              _.each(resultados,function(item){
                item.nombres = item.nombres +" " + item.apellidos;

              });
              $scope.to.options = resultados;
              $timeout(function(){$scope.generalLoader = false;},500);
            });
          }
        }
      }]

    });

    formlyConfigProvider.setType({
      name: 'horizontalUiMultiSelectInputGET',
      extends: 'ui-multi-select-input',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
      controller:['$scope','$log','tipoService','$timeout', function($scope,$log,tipoService,$timeout) {

        $scope.borrarSub = function($item,model){
          $item[$scope.to.input.inputProp] = undefined;
        };

        if(!$scope.to.options){
          var parametros={};
          if( $scope.to.parametros){
            parametros = $scope.to.parametros($scope.model);
          }
          $scope.to.options = [];
          $scope.generalLoader = true;
          if(!$scope.to.referenceId){

            tipoService.listar($scope.to.metodo,$scope.to.action,parametros).then(function(data){
              var dataEnvolved = [];
              _.each(fullReject({options:data,comparatorAttr:"id"},{options:$scope.to.rejectOptions,comparatorAttr:"id"}) ,function(it){
                var newItem = {};
                newItem[$scope.to.subkey] = it;
                dataEnvolved.push(newItem);
              });
              $scope.to.options = dataEnvolved;
              $timeout(function(){$scope.generalLoader = false;},500);
            });
          }else{
            tipoService.listarPorIdChris($scope.to.action,$scope.to.referenceIdName,$scope.to.referenceId).then(function(data){
              var resultados = fullReject({options:JSON.parse(data[$scope.to.attrToParse]),comparatorAttr:"id"},{options:$scope.to.rejectOptions,comparatorAttr:"id"});
              _.each(resultados,function(item){
                item.nombres = item.nombres +" " + item.apellidos;

              });
              $scope.to.options = resultados;
              $timeout(function(){$scope.generalLoader = false;},500);
            });
          }
        }else{
          $scope.to.options =  fullReject({options:$scope.to.options,comparatorAttr:"id"},{options:$scope.to.rejectOptions,comparatorAttr:"id"});
        }
      }]
    });

    formlyConfigProvider.setType({
      name: 'horizontalUiMultiSelectInputGETCascade',
      extends: 'ui-multi-select-input',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
      controller:['$scope','$log','tipoService','$timeout', function($scope,$log,tipoService,$timeout) {

        $scope.onSelectForInput = $scope.to.input.onSelect;

        $scope.borrarSub = function($item,model){
          $item[$scope.to.input.inputProp] = undefined;
        };

        $scope.$watch("model." + $scope.to.toWatch, function (newValue, oldValue, theScope) {

          if($scope.model[$scope.to.toWatch] == "" || $scope.model[$scope.to.toWatch] == undefined) {
            // reset this select
            $scope.model[$scope.options.key] = undefined;
          }

          //if($scope.to.options){
          var parametros={};
          if( $scope.to.parametros){
            parametros = $scope.to.parametros($scope.model);
          }
          $scope.to.options = [];
          $scope.generalLoader = true;
          if(!$scope.to.referenceId){

            tipoService.listar($scope.to.metodo,$scope.to.action,parametros).then(function(data){
              var dataEnvolved = [];
              _.each(fullReject({options:data,comparatorAttr:"id"},{options:$scope.to.rejectOptions,comparatorAttr:"id"}) ,function(it){
                var newItem = {};
                newItem[$scope.to.subkey] = it;
                dataEnvolved.push(newItem);
              });
              if( $scope.to.valoresDefecto){
                $scope.model[$scope.options.key] = $scope.to.valoresDefecto(dataEnvolved,$scope.model);
              }
              $timeout(function(){$scope.generalLoader = false;},500);
              $scope.to.options = dataEnvolved;
            });
          }else{
            tipoService.listarPorIdChris($scope.to.action,$scope.to.referenceIdName,$scope.to.referenceId).then(function(data){
              var resultados = fullReject({options:JSON.parse(data[$scope.to.attrToParse]),comparatorAttr:"id"},{options:$scope.to.rejectOptions,comparatorAttr:"id"});
              _.each(resultados,function(item){
                item.nombres = item.nombres +" " + item.apellidos;

              });
              $scope.to.options = resultados;
            });
          }
          //}else{
          //  $scope.to.options =  fullReject({options:$scope.to.options,comparatorAttr:"id"},{options:$scope.to.rejectOptions,comparatorAttr:"id"});
          //}
        });
      }]
    });

    formlyConfigProvider.setType({
      name: 'horizontalUiMultiSelectForm',
      extends: 'ui-multi-select-form',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalUiMultiSelectInputGETCascadeNoLabel',
      extends: 'horizontalUiMultiSelectInputGETCascade',
      wrapper: ['horizontalBootstrapNoLabel','bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'inputNoLabel',
      extends: 'input',
      wrapper: ['horizontalBootstrapNoLabel', 'bootstrapHasError'],
      defaultOptions: {
        ngModelAttrs: {
          max: {
            attribute: 'max'
          }
        }
      }
    });

    formlyConfigProvider.setType({
      name: 'horizontalInputRut',
      extends: 'input-rut',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalInputMask',
      extends: 'maskedInput',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalInputMaskNoLabel',
      extends: 'maskedInput',
      wrapper: ['bootstrapHasError']
    });


    formlyConfigProvider.setType({
      name: 'adjunto',
      extends: 'input-file',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    function fullReject(objA,objB){

      _.each(objB.options,function(arrayToReject){
        if(!!arrayToReject && arrayToReject.length>0){
          _.each(arrayToReject,function(arr){
            objA.options = _.reject(objA.options, function(item){ return item[objA.comparatorAttr] == arr[objB.comparatorAttr]; });
          });
        }
      });
      return objA.options;
    }


  }).run(function(formlyConfig,FileUploader) {
  var attributes = [
    'date-disabled',
    'custom-class',
    'show-weeks',
    'starting-day',
    'init-date',
    'min-mode',
    'max-mode',
    'format-day',
    'format-month',
    'format-year',
    'format-day-header',
    'format-day-title',
    'format-month-title',
    'year-range',
    'shortcut-propagation',
    'datepicker-popup',
    'show-button-bar',
    'current-text',
    'clear-text',
    'close-text',
    'close-on-date-selection',
    'datepicker-append-to-body'
  ];

  var bindings = [
    'datepicker-mode',
    'min-date',
    'max-date'
  ];

  var ngModelAttrs = {};

  angular.forEach(attributes, function(attr) {
    ngModelAttrs[camelize(attr)] = {attribute: attr};
  });

  angular.forEach(bindings, function(binding) {
    ngModelAttrs[camelize(binding)] = {bound: binding};
  });

  var today = new Date();

  formlyConfig.setType({
    name: 'datepicker',
    template: '<input close-text="Cerrar"  date-disabled="to.disabled" class="form-control" datepickerPopup ng-model="model[options.key]" is-open="to.isOpen" datepicker-options="to.datepickerOptions" />',
    wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    link:function(scope, el, attrs){

      if(!!scope.model[scope.options.key]){
        var validDate = moment(scope.model[scope.options.key]).format("DD-MM-YYYY");
        if(validDate !== 'Invalid date'){

          scope.model[scope.options.key] = validDate;
        }
      }
      if(scope.to.targetMinDateKey){
        scope.to.dateDisabled = true;
      }

      scope.$watch("model."+scope.to.targetMinDateKey ,function(newVal,oldVal){

        if(scope.to.targetMinDateKey && newVal !=undefined){

          if(scope.to.dateDisabled && newVal){
            scope.to.dateDisabled = false;
          }
          if(newVal)

            if(!moment(moment(newVal, "DD-MM-YYYY")._d).isBefore(moment(scope.model[scope.options.key], "DD-MM-YYYY")._d)){
              scope.model[scope.options.key] = undefined;
            }

          scope.to.minDate = moment(newVal, "DD-MM-YYYY")._d;
        }
      });

      scope.$watch("model."+scope.to.formatOnChangeKey ,function(newVal,oldVal){

        if(scope.to.formatOnChangeKey && newVal && oldVal){
          if(newVal.id != oldVal.id){
            scope.model[scope.options.key] = undefined;
          }

        }
      });
    },
    defaultOptions: {
      validators:{
        fecha:function($modelValue){
          var pattern =/^([0-9]{2})\-([0-9]{2})\-([0-9]{4})$/;
          return pattern.test($modelValue);
        }
      },
      ngModelAttrs: ngModelAttrs,
      templateOptions: {
        minDate: today,
        addonLeft: {
          class: 'glyphicon glyphicon-calendar',
          onClick: function(options, scope) {
            if(!scope.to.disabled){
              options.templateOptions.isOpen = !options.templateOptions.isOpen;
            }
          }
        },
        onFocus: function($viewValue, $modelValue, scope) {
          scope.to.isOpen = !scope.to.isOpen;
        },
        datepickerOptions: {
          startingDay:1
        }
      }
    }
  });

  formlyConfig.setType({
    name: 'horizontalDatepicker',
    extends: 'datepicker',
    wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
  });

  function camelize(string) {
    string = string.replace(/[\-_\s]+(.)?/g, function(match, chr) {
      return chr ? chr.toUpperCase() : '';
    });
    // Ensure 1st char is always lowercase
    return string.replace(/^([A-Z])/, function(match, chr) {
      return chr ? chr.toLowerCase() : '';
    });
  }


  formlyConfig.setType({
    name: 'keyValueTable',
    templateUrl:'views/templates/llave-valor-type-template.html',
    controller: "LlaveValorTemplateCtrl"
  });

  formlyConfig.setType({
    name: 'TableForm',
    templateUrl:'views/templates/table-template.html',
    controller: "TableTemplateCtrl"
  });

  formlyConfig.setType({
    name: 'multipleFileUpload',
    templateUrl:'views/templates/multiple-file-upload.html',
    controller: "MultipleFileUploadCtrl"
  });

  formlyConfig.setType({
    name: 'simpleTable',
    templateUrl:'views/templates/simple-table-template.html',
    controller: "SimpleTableCtrl"
  });

  formlyConfig.setType({
    name: 'label',
    template:'<strong><div class="{{to.class}}">{{to.label}}</div></strong>'

  });

  formlyConfig.setType({
    name: 'titleFancy',
    template:'<div ng-model="model[options.key]" class="row">\n  <div class="legend-button">\n    <div class="legend">{{to.title}}</div>\n    <div tooltip="{{button.tooltip}}" ng-repeat="button in to.buttons" ng-click="button.click(model,to,this)" class="btn btn-info">{{button.title}} <i class="{{button.icon}}"></i></div>\n  </div>\n</div>',
    controller:['$scope','$log','tipoService','$timeout', function($scope,$log,tipoService,$timeout) {}]
  });

  formlyConfig.setType({
    name:'horizontalSlider',
    template:'<rzslider ng-model="model[options.key]" rz-slider-model="model[options.key]" rz-slider-options="to"></rzslider>\n',
    controller:['$scope','$log','tipoService','$timeout', function($scope,$log,tipoService,$timeout) {
      if($scope.to.defaultValue){
        $scope.model[$scope.options.key] = $scope.to.defaultValue;
      }

      $scope.$watch("model."+$scope.options.key,function(newValue,oldValue) {

        $scope.to.onChangeModel($scope.model,newValue,oldValue,$scope);

      });
    }],
    wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']

  });
});
