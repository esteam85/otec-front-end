angular.module('otecApp')
  .config(function ($stateProvider,USER_ROLES) {

    $stateProvider
      .state('administracion-alarmas-notificaciones', {
        url:"/administracion-alarmas-notificaciones",
        templateUrl: 'views/administracion.html',
        controller: 'AdministracionCtrl',
        data:{
          authorizedRoles: [USER_ROLES.logged]
        },
        resolve:{
          titulo:function(){
            return "Administración de Alarmas y Notificaciones";
          },
          tabData:function(){
            return [
              {
                heading: 'Alarmas',
                route:   'administracion-alarmas-notificaciones.alarmas'
              }
            ];
          }
        }
      })
      .state('administracion-alarmas-notificaciones.alarmas', {
        url:"/alarmas",
        templateUrl: 'views/administracion-item.html',
        controller: 'AdministracionItemCtrl',
        resolve:{
          tabData:function(){
            return [
              {
                heading: 'Alarmas',
                route:   'administracion-alarmas-notificaciones.alarmas.lista-alarmas',
                default:true
              },
              {
                heading: 'Tipo de Alarma',
                route:   'administracion-alarmas-notificaciones.alarmas.lista-tipo-alarma'
              }
            ];
          }
        }
      })
      .state('administracion-alarmas-notificaciones.alarmas.lista-alarmas', {
        url: "/lista-alarmas",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Alarmas";
          },
          tituloBoton: function(){
            return "Crear Alarma";
          },
          metodos: function(){
            return {
              listar:"Alarma"};
          },
          tableHeader: function(alarmaHeaderTable){
            return alarmaHeaderTable;
          },
          opcionesCrearEditar: function(){

            return {
              formulario:[
                {
                  key: 'nombre',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Nombre',
                    placeholder: 'Nombre',
                    required: true
                  }
                },
                {
                  key: 'tiempo',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Días',
                    placeholder: 'Días',
                    type:'number',
                    required: true
                  }
                },{
                  "key": "tipoAlarma",
                  "type": "horizontalUiSelect",
                  "templateOptions": {
                    "placeholder": "Tipo de Alarma",
                    "label": "Tipo",
                    "labelProp": "nombre",
                    "metodo":"TipoAlarma",
                    required: true

                  }
                }
              ],
              metodos:{crear:"Alarma",editar:"Alarma"}
            };
          }
        }
      })
      .state('administracion-alarmas-notificaciones.alarmas.lista-tipo-alarma', {
        url: "/lista-tipo-alarma",
        templateUrl: 'views/templates/configuracion-component-template.html',
        controller: 'ConfiguracionComponentCtrl',
        resolve:{
          leyenda:function(){
            return "Tipos de Alarma";
          },
          tituloBoton: function(){
            return "Crear Tipo de Alarma";
          },
          metodos: function(){
            return {
              listar:"TipoAlarma"};
          },
          tableHeader: function(tipoAlarmaHeaderTable){
            return tipoAlarmaHeaderTable;
          },
          opcionesCrearEditar: function(){

            return {
              formulario:[
                {
                  key: 'nombre',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Nombre',
                    placeholder: 'Nombre',
                    required: true
                  }
                },
                {
                  key: 'identificador',
                  type: 'horizontalInput',
                  templateOptions: {
                    label: 'Identificador',
                    placeholder: 'Identificador',
                    type:'number',
                    required: true
                  }
                },{
                  "key": "estado",
                  "type": "horizontalRadios",
                  "templateOptions": {
                    "label": "Estado",
                    "options": [
                      {
                        "name": "Activo",
                        "value": "A"
                      },
                      {
                        "name": "Inactivo",
                        "value": "I"
                      }
                    ]}
                }
              ],
              metodos:{crear:"TipoAlarma",editar:"TipoAlarma"}
            };
          }
        }
      })


  });
