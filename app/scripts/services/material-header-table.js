'use strict';

/**
 * @ngdoc service
 * @name otecApp.proveedorHeaderTable
 * @description
 * # proveedorHeaderTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('materialHeaderTable', function () {

    return [
      { title: 'Nombre', field: 'nombre',visible: true},
      { title: 'Codigo', field: 'codigo',visible: true},
      { title: 'Codigo Sap', field: 'codigoSap',visible: true},
      { title: 'Descripcion', field: 'descripcion',visible: true},
      { title: 'Precio', field: 'precio',visible: true},
      { title: 'Stock Actual', field: 'stockActual',visible: true},
      { title: 'Tipo Material', field: 'familia', subfield: 'nombre', visible: true},
      { title: 'Tipo Moneda', field: 'tipoMoneda', subfield: 'nombre', visible: true},
      { title: 'Tipo Unidad Medida', field: 'tipoUnidadMedida', subfield: 'nombre', visible: true},
      { title: 'Tipo Unidad Medida', field: 'tipoUnidadMedida', subfield: 'nombre', visible: true},
      { title: 'Estado', field: 'estado', visible: true,formatter:"estado", filter: { 'name': 'text' },class:"text-center" }

    ];
  });
