'use strict';

/**
 * @ngdoc service
 * @name otecApp.idEventos
 * @description
 * # idEventos
 * Constant in the otecApp.
 */
angular.module('otecApp')
  .constant('ID_EVENTOS', {
    CREAR_OT: 1,
    CREAR_OT_SBE: 30,
    CREAR_OT_RAN: 23,
    ASIGNAR_TRABAJADOR: 2,
    INFORMAR_AVANCE:3,
    GENERAR_ACTA:4,
    VALIDA_ACTA:5,
    AUTORIZAR_PAGO:6,
    INGRESAR_PAGO:7,
    ACEPTAR_ACTA:9,
    ACEPTAR_ACTA_ORDINARIO:21,
    RECHAZAR_ACTA:10,
    RECHAZAR_ACTA_ORDINARIO:22,
    PAGO_TOTAL:11,
    PAGO_PARCIAL:12,
    ASIGNAR_COORDINADOR:15,
    VALIDAR_PMO:16,
    VALIDAR_IMPUTACION:17,
    ADJUNTAR_CARTA:18,
    GENERAR_ACTA_ORDINARIO:19,
    VALIDA_ACTA_ORDINARIO:20
  });
