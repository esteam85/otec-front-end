'use strict';

/**
 * @ngdoc service
 * @name otecApp.proveedorHeaderTable
 * @description
 * # proveedorHeaderTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('proveedorHeaderTable', function () {

    return [{ title: 'Nombre', field: 'nombre', visible: true},
      { title: 'Tipo', field: 'tipoProveedor', subfield: 'nombre', visible: true},
      { title: 'Rut', field: 'rut', field2:'dv', visible: true },
      { title: 'Email', field: 'email', visible: true },
      { title: 'Teléfono', field: 'telefono', visible: true },
      { title: 'Dirección', field: 'direccion', visible: true},
      { title: 'Estado', field: 'estado', visible: true,formatter:"estado", filter: { 'name': 'text' },class:"text-center" }];
  });
