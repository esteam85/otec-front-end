'use strict';

/**
 * @ngdoc service
 * @name otecApp.proveedorHeaderTable
 * @description
 * # proveedorHeaderTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('parametroHeaderTable', function () {

    return [{ title: 'Nombre', field: 'nombre', visible: true},
      { title: 'Tipo', field: 'tipoParametro', subfield: 'nombre', visible: true},
      { title: 'Descripción', field: 'descripcion', visible: true, filter: { 'name': 'text' } },
      { title: 'Detalles Parámetro', field: 'detalleParametros',
        lista:{
          usarTableHeader:true,tableHeader:[{ title: 'Llave', field: 'llave', visible: true, filter: { 'name': 'text' } },{ title: 'Valor', field: 'valor', visible: true, filter: { 'name': 'text' } },{ title: 'isTemplateOption', field: 'isTemplateOption', visible: true,icon:true, filter: { 'name': 'text' },class:"text-center" }]
          ,field:"detalleParametros",size:"lg"}
        , icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' }}
    ];
  });
