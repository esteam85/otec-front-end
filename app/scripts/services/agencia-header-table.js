'use strict';

/**
 * @ngdoc service
 * @name otecApp.agenciaHeaderTable
 * @description
 * # agenciaHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('agenciaHeaderTable', function (nombreHeaderTable) {
    return [
      { title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Descripción', field: 'descripcion', visible: true, filter: { 'name': 'text' } },
      { title: 'Region', field: 'regiones', subfield: 'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Estado', field: 'estado', visible: true,formatter:"estado", filter: { 'name': 'text' },class:"text-center" }
    ];
  });
