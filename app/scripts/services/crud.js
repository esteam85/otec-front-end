'use strict';

/**
 * @ngdoc service
 * @name otecApp.CRUD
 * @description
 * # CRUD
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('CRUD', function ($q,$log,Restangular,Session) {

    function crear(objCoreOT,parametros,action){

      var deferred = $q.defer();

      Restangular.all(action || "crear").customPOST($.param({objCoreOT:objCoreOT,parametros:JSON.stringify(parametros),usuarioId:Session.userId}),"",undefined,{'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"}).then(function(data){

        deferred.resolve(data.data);

      },function(error){
        deferred.reject(error);

      });
      return deferred.promise;
    };

    function crearChris(action,attrName,AttrValue){

      var deferred = $q.defer();
      var obj = {};
      obj[attrName]= AttrValue;
      Restangular.one(action || "crear").get(obj).then(function(data){

        deferred.resolve(data.data);

      },function(error){
        deferred.reject(error);

      });
      return deferred.promise;
    };

    function crearCustom(action,parametros){

      var deferred = $q.defer();
      _.extend(parametros,{usuarioId:Session.userId});
      Restangular.all(action).customPOST($.param(parametros) ,"",undefined,{'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"}).then(function(data){

        deferred.resolve(data.data);

      },function(error){
        deferred.reject(error);

      });
      return deferred.promise;
    };

    function listar(objCoreOT,action,parametros){

      var deferred = $q.defer();


      Restangular.one(action ||"listar").get(_.extend({objCoreOT:objCoreOT, usuarioId:Session.userId},parametros)).then(function(data){

        deferred.resolve(data.data);

      },function(error){
        $log.error("error en " + objCoreOT,error);
        deferred.reject(error);

      });
      return deferred.promise;
    };

    function listarCustom(action,parameters,configs){

      var deferred = $q.defer();

      _.extend(parameters,{usuarioId:Session.userId});
      Restangular.one(action ||"listar").withHttpConfig(configs).get(parameters).then(function(data){

        deferred.resolve(data.data);

      },function(error){
        $log.error("error en " + action,error);
        deferred.reject(error);

      });
      return deferred.promise;
    };


    function listarPorId(action,objectId,objValue){

      var deferred = $q.defer();
      var obj = {};
      obj[objectId]= objValue;
      Restangular.one(action || "listarPorId").get(obj).then(function(data){

        deferred.resolve(data.data);

      },function(error){
        $log.error("error en " + action,error);
        deferred.reject(error);

      });

      return deferred.promise;
    };


    function actualizar(objCoreOT,parametros,action){

      var deferred = $q.defer();
      Restangular.one(action || "actualizar").get({objCoreOT:objCoreOT,parametros:JSON.stringify(parametros),usuarioId:Session.userId}).then(function(data){

        deferred.resolve(data.data);

      },function(error){

        deferred.reject(error);

      });
      return deferred.promise;
    };

    return {
      crear:crear,
      actualizar:actualizar,
      listar:listar,
      listarPorId:listarPorId,
      crearChris:crearChris,
      listarCustom:listarCustom,
      crearCustom:crearCustom
    };
  });
