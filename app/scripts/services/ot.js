'use strict';

/**
 * @ngdoc service
 * @name otecApp.Ot
 * @description
 * # Ot
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('OtService', function ($q,CRUD) {

    function obtenerAccionesPorEvento(parametros){
      var deferred = $q.defer();

      CRUD.listarCustom("obtenerAccionesFrontendPorEvento",parametros).then(function(data){
        deferred.resolve(JSON.parse(data.retorno));
      },function(error){
        deferred.reject(error);
      });
      return deferred.promise;
    };



    function ejecutarServicioAccion(action,parametros,idEvento){
      var deferred = $q.defer();

      CRUD.crearCustom(action,{parametrosEvento:JSON.stringify(parametros),idEvento:idEvento}).then(function(data){
        deferred.resolve(data);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    };

    function obtenerAccionesPorOtId(otId){
      var deferred = $q.defer();

      CRUD.listarCustom("obtenerAccionesFrontendPorEvento",{otId:otId}).then(function(data){
        data.acciones = JSON.parse(data.retorno);
        deferred.resolve(data);
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    };

    return {
      obtenerAccionesPorEvento:obtenerAccionesPorEvento,
      obtenerAccionesPorOtId:obtenerAccionesPorOtId,
      ejecutarServicioAccion:ejecutarServicioAccion

    };
  });
