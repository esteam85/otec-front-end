'use strict';

/**
 * @ngdoc service
 * @name otecApp.Acciones
 * @description
 * # Acciones
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('AccionesService', function ($log,ID_EVENTOS,Session) {

    var headerTableActa = [
      {
        title: 'Descripción',
        type:"label",
        field: 'descripcion',
        visible: true
      },
      {
        title: 'Cantidad Costeada',
        type:"label",
        descripcion:"Cantidad original costeada.",
        field: 'cantidadCubicada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Acumulada Validada',
        descripcion:"Suma de todas las cantidades informadas y validadas por el Gestor.",
        type:"label",
        field: 'cantidadAcumuladaInformada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Actual Informada',
        type:"label",
        descripcion:"Cantidad Informada en el Acta actual.",
        field: 'cantidadActualInformada',
        class:"text-center",
        visible: true
      }
    ];
    var headerTableActaOrdinario = [
      {
        title: 'Item',
        type:"label",
        field: 'nombre',
        visible: true
      },
      {
        title: 'Cantidad Cubicada',
        type:"label",
        descripcion:"Cantidad original costeada.",
        field: 'cantidadCubicada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Acumulada Validada',
        descripcion:"Suma de todas las cantidades informadas y validadas por el Gestor.",
        type:"label",
        field: 'cantidadAcumuladaInformada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Actual Informada',
        type:"label",
        descripcion:"Cantidad Informada en el Acta actual.",
        field: 'cantidadActualInformada',
        class:"text-center",
        visible: true
      }
    ];
    var headerTableActaAdionales = [
      {
        title: 'Descripción',
        type:"label",
        field: 'descripcion',
        visible: true
      },
      {
        title: 'Cantidad costeada',
        type:"label",
        descripcion:"Cantidad original costeada.",
        field: 'cantidadCubicada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Acumulada Validada',
        descripcion:"Suma de todas las cantidades informadas y validadas por el Gestor.",
        type:"label",
        field: 'cantidadAcumuladaInformada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Actual Informada',
        type:"label",
        descripcion:"Cantidad Informada en el Acta actual.",
        field: 'cantidadActualInformada',
        class:"text-center",
        visible: true
      },{
        title: 'Validar',
        type:"validar",
        descripcion:"Validar adicional informado.",
        field: 'validar',
        formName:"aprobar",
        class:"text-center",
        visible: true
      }
    ];

    function toSelect(eventos){
      var form = [];
      _.each(eventos,function(evento,index){
        var comboConEvento = {
          key: index.toString(),
          type: 'horizontalUiSelect',
          templateOptions: {
            label: evento.evento,
            placeholder: 'Seleccionar Responsable',
            options:evento.usuarios,
            "valueProp": "id",
            "labelProp": "nombre",
            required: true
          }
        };
        form.push(comboConEvento);
      });
      return form;
    }

    function prepararObjeto(objeto,idEvento,botonData){
      var objetoFinal={};
      $log.info("listaFinal-antes",objeto);
      if(idEvento==ID_EVENTOS.CREAR_OT){
        objetoFinal.idEvento = ID_EVENTOS.CREAR_OT;
        if(objeto.AC){
          objeto.AC.gestor = {id:Session.userId};
          objeto.AC.estadoWorkflow = {id:1};
          objeto.AC.tipoSolicitud = {id:0};
          objeto.AC.central = {id:0};
          objetoFinal.datosOt= objeto.AC;
        }
        if(objeto["AC-FORM"]){
          objeto["AC-FORM"].asunto = objeto["AC-FORM"].asunto.text;
          objetoFinal.datosOt.jsonDetalleOt = objeto["AC-FORM"];
        }
        if(objeto["AC-ASIGNAR"]){
          objetoFinal.workflow = objeto["AC-ASIGNAR"]["asignar"];
        }
        objetoFinal.datosOt.tipoOt={id:1};


      }

      if(idEvento==ID_EVENTOS.CREAR_OT_SBE){
        objetoFinal.idEvento = ID_EVENTOS.CREAR_OT_SBE;
        if(objeto.AC){
          objeto.AC.gestor = {id:Session.userId};
          //objeto.AC.subGerente = {id:0};
          objeto.AC.central = {id:0};
          objeto.AC.tipoSolicitud = {id:0};
          objeto.AC.estadoWorkflow = {id:1};
          objetoFinal.datosOt= objeto.AC;
        }
        if(objeto["AC-FORM-SBE"]){
          objetoFinal.datosOt.jsonDetalleOt = objeto["AC-FORM-SBE"];
        }
        if(objeto["AC-ASIGNAR"]){
          objetoFinal.workflow = objeto["AC-ASIGNAR"]["asignar"];
        }
      }

      if(idEvento==ID_EVENTOS.CREAR_OT_RAN){
        objetoFinal.idEvento = ID_EVENTOS.CREAR_OT_RAN;
        if(objeto.AC){
          objeto.AC.gestor = {id:Session.userId};
          //objeto.AC.subGerente = {id:0};
          objeto.AC.central = {id:0};
          objeto.AC.tipoSolicitud = {id:0};
          objeto.AC.estadoWorkflow = {id:1};
          objetoFinal.datosOt= objeto.AC;
        }
        if(objeto["AC-FORM-RAN"]){
          objetoFinal.datosOt.jsonDetalleOt = objeto["AC-FORM-RAN"];
        }
        if(objeto["AC-ASIGNAR"]){
          objetoFinal.workflow = objeto["AC-ASIGNAR"]["asignar"];
        }
      }

      if(idEvento==ID_EVENTOS.ASIGNAR_TRABAJADOR){
        objetoFinal = objeto["AC-AS-TR"];
        //solo para caso evento editar trabajador hecho por coordinador en sbe
        objetoFinal.eventoId = ID_EVENTOS.INFORMAR_AVANCE;
        objetoFinal.datosOt = {id:objeto.id};
      }
      if(idEvento==ID_EVENTOS.ASIGNAR_COORDINADOR){
        objetoFinal = objeto["AC-AS-CO"];
        objetoFinal.datosOt = {id:objeto.id};
      }
      if(idEvento==ID_EVENTOS.INFORMAR_AVANCE){
        objetoFinal = objeto["AC-CER-AV"];
        objetoFinal.datosOt = {id:objeto.id};
      }
      if(idEvento==ID_EVENTOS.GENERAR_ACTA){
        objetoFinal = objeto["AC-GE-AC"];
        if(!objetoFinal.servicios){objetoFinal.servicios = [];}
        _.each(objetoFinal.serviciosAdicionales,function(item){

          objetoFinal.servicios.push({id:item.servicio.id.toString(),cantidad:item.cantidad,cantidadReal:item.cantidad});
        });

        _.each(objetoFinal.serviciosAdicionalesYRechazados,function(item){

          objetoFinal.servicios.push(item);
        });

        if(!objetoFinal.materiales){objetoFinal.materiales = [];}
        _.each(objetoFinal.materialesAdicionales,function(item){

          objetoFinal.materiales.push({id:item.material.id.toString(),cantidad:item.cantidad,cantidadReal:item.cantidad});
        });
        _.each(objetoFinal.materialesAdicionalesYRechazados,function(item){

          objetoFinal.materiales.push(item);
        });
        objetoFinal.datosOt = {id:objeto.id};
      }
      if(idEvento==ID_EVENTOS.GENERAR_ACTA_ORDINARIO){
        objetoFinal = objeto["AC-GE-AC.OR"];
        objetoFinal.datosOt = {id:objeto.id};
      }
      if(idEvento==ID_EVENTOS.ACEPTAR_ACTA){
        objetoFinal = objeto["AC-TI-PA"];
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.validacionActa = true;
        objetoFinal.opcionPago = botonData.opcion;
        objetoFinal.usuarioId = Session.userId;
        objetoFinal.idActa = objeto["AC-VA-AC"].idActa;
      }
      if(idEvento==ID_EVENTOS.ACEPTAR_ACTA_ORDINARIO){
        objetoFinal = objeto["AC-TI-PA.OR"];
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.validacionActa = true;
        objetoFinal.opcionPago = botonData.opcion;
        objetoFinal.usuarioId = Session.userId;
        objetoFinal.idActa = objeto["AC-VA-AC.OR"].idActa;
      }
      if(idEvento==ID_EVENTOS.RECHAZAR_ACTA){
        objetoFinal = objeto["AC-REC-AC"];
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.validacionActa = false;
        objetoFinal.usuarioId = Session.userId;
        objetoFinal.detalleServiciosAdicionalesActa = objeto["AC-VA-AC"].detalleServiciosAdicionalesActa;
        objetoFinal.idActa = objeto["AC-VA-AC"].idActa;
        objetoFinal.detalleMaterialesAdicionalesActa = objeto["AC-VA-AC"].detalleMaterialesAdicionalesActa;
      }
      if(idEvento==ID_EVENTOS.RECHAZAR_ACTA_ORDINARIO){
        objetoFinal = objeto["AC-REC-AC.OR"];
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.validacionActa = false;
        objetoFinal.usuarioId = Session.userId;
        objetoFinal.idActa = objeto["AC-VA-AC.OR"].idActa;
      }
      if(idEvento==ID_EVENTOS.VALIDA_ACTA){
        objetoFinal = objeto["AC-FORM-PAGO"];
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.usuarioId = Session.userId;
      }
      if(idEvento==ID_EVENTOS.PAGO_TOTAL){
        objetoFinal = objeto["AC-TI-PA"];
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.usuarioId = Session.userId;
      }
      if(idEvento==ID_EVENTOS.PAGO_PARCIAL){
        objetoFinal = objeto["AC-TI-PA"];
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.usuarioId = Session.userId;
      }
      if(idEvento==ID_EVENTOS.AUTORIZAR_PAGO){
        objetoFinal = objeto["AC-AU-PA"];
        objetoFinal.opcionAutorizacion = botonData.opcion;
        objetoFinal.idEvento = botonData.eventoId;
        objetoFinal.eventoId = botonData.eventoId;
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.usuarioId = Session.userId;
      }
      if(idEvento==ID_EVENTOS.INGRESAR_PAGO){
        objetoFinal = objeto["AC-IN-PA"];
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.usuarioId = Session.userId;
      }
      if(idEvento==ID_EVENTOS.ADJUNTAR_CARTA){
        objetoFinal = objeto["AC-AD-CA"];
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.usuarioId = Session.userId;
      }
      if(idEvento==ID_EVENTOS.VALIDAR_PMO){
        objetoFinal = objeto["AC-VA-PM"];
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.idEvento = ID_EVENTOS.VALIDAR_PMO;
        objetoFinal.usuarioId = Session.userId;
      }
      if(idEvento==ID_EVENTOS.VALIDAR_IMPUTACION){
        objetoFinal = objeto["AC-VA-IM"];
        objetoFinal.datosOt = {id:objeto.id};
        objetoFinal.idEvento = ID_EVENTOS.VALIDAR_IMPUTACION;
        objetoFinal.usuarioId = Session.userId;
      }
      objetoFinal.usuarioId = Session.userId;

      $log.info("listaFinal-despues",objetoFinal);
      return objetoFinal;
    };

    function prepareEjecutarEvento(objeto,incremento,AccionesPorEvento,ID_EVENTO,paso){

      var opciones = {ID_EVENTO:ID_EVENTO,incremento:incremento,paso:paso};
      if(AccionesPorEvento[incremento].codigo === "AC-FORM" || AccionesPorEvento[incremento].codigo === "AC-AU-PA" || AccionesPorEvento[incremento].codigo === "AC-IN-PA" ){

        var formulario = JSON.parse(AccionesPorEvento[incremento].parametros.formulario);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);

        objeto[AccionesPorEvento[incremento].codigo] = {asunto:{}};
        objeto[AccionesPorEvento[incremento].codigo].asunto.text = "Comunicación de Adjudicación";

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:AccionesPorEvento[incremento],
          formulario:formulario,
          modal:modal
        });

      }
      if(AccionesPorEvento[incremento].codigo === "AC-FORM-SBE" ){
        opciones.ID_EVENTO = ID_EVENTOS.CREAR_OT_SBE;
        var formulario = JSON.parse(AccionesPorEvento[incremento].parametros.formulario);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:AccionesPorEvento[incremento],
          formulario:formulario,
          modal:modal
        });

      }

      if( AccionesPorEvento[incremento].codigo === "AC-FORM-RAN" ){
        opciones.ID_EVENTO = ID_EVENTOS.CREAR_OT_RAN;
        var formulario = JSON.parse(AccionesPorEvento[incremento].parametros.formulario);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:AccionesPorEvento[incremento],
          formulario:formulario,
          modal:modal
        });

      }


      if(AccionesPorEvento[incremento].codigo === "AC-ASIGNAR"){



        objeto["AC-ASIGNAR"] = {asignar:[]};
        var eventos = AccionesPorEvento[incremento].parametros.eventos;
        var decision = "";
        _.each(eventos,function(item,index){
          //var defaultValue;
          //if(item.decision) {
          //  decision= 'horizontalUiMultiSelectNoLabel';
          //  var usr = undefined;
          //  //if(item.evento == "Aceptar Acta"){
          //  //  usr = _.find(item.usuarios,function(user){ return user.id == Session.userId ;});
          //  //}
          //  //if(usr){
          //  //  usr.orden = 1;
          //  //  defaultValue = [usr];
          //  //}else{
          //  //  item.usuarios[0].orden = 1;
          //  //  defaultValue = [item.usuarios[0]];
          //  //}
          //}else{
          //  decision="horizontalUiSelectNoLabel";
          //  //defaultValue = item.usuarios[0];
          //}

          objeto["AC-ASIGNAR"].asignar.push({
            "evento":{
              nombre:item.nombre,
              id:item.id
            },
            "valorFormUsuario":{
              "key": "usuario",
              "type": 'horizontalUiMultiSelectNoLabel',
              "defaultValue":item.usuarios,
              "templateOptions": {
                "placeholder": "Seleccionar Responsable*",
                "options":[],
                //"disabled":item.disabled,
                "sortable":true,
                "label":"",
                "valueProp": "id",
                "labelProp": "nombre",
                "required": true
              }
            },
            "decision":item.decision,
            valorFormDuracion:{
              "key": "tiempo",
              "defaultValue":item.duracion,
              type: 'inputNoLabel',
              "templateOptions": {
                "placeholder": "Duración en Días*",
                "type":'number',
                "required": true
              }
            }
          });

        });
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);
        var form = [{
          key: 'asignar',
          type: 'TableForm',
          templateOptions: {
            requerid:true,
            columns:[{
              title: 'Evento',
              type:"label",
              field: 'evento',
              subfield:"nombre",
              visible: true},
              {
                title: 'Usuario',
                type:"formly",
                valorFormField:"valorFormUsuario",
                field: 'usuario',
                visible: true},
              {
                title: 'Duración en Días',type:"formly",
                valorFormField:"valorFormDuracion",
                field: 'tiempo',
                visible: true
              }]
          }
        }];
        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          formulario:form,
          modal:modal});
      }

      if(AccionesPorEvento[incremento].codigo === "AC-AS-TR"){
        var asignar = JSON.parse(AccionesPorEvento[incremento].parametros.asignar);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);
        var form = [{
          "key": "trabajador",
          "type": "horizontalUiSelect",
          "templateOptions": {
            "placeholder": "Seleccionar Trabajador",
            "label": "Trabajador",
            "options":asignar,
            "valueProp": "id",
            "labelProp": "nombre",
            required: true
          }
        }];

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          formulario:form,modal:modal});

      }

      if(AccionesPorEvento[incremento].codigo === "AC-AS-CO"){
        var asignar = JSON.parse(AccionesPorEvento[incremento].parametros.asignar);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);
        var form = [{
          "key": "coordinador",
          "type": "horizontalUiSelect",
          "templateOptions": {
            "placeholder": "Seleccionar Coordinador",
            "label": "Coordinador",
            "options":asignar,
            "valueProp": "id",
            "labelProp": "nombre",
            required: true
          }
        }];

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:AccionesPorEvento[incremento],
          formulario:form,modal:modal});

      }

      if(AccionesPorEvento[incremento].codigo === "AC-INF-AV"){
        var form = JSON.parse(AccionesPorEvento[incremento].parametros.formulario);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);


        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:undefined,
          formulario:form,
          modal:modal});
      }
      //Pregunta
      if(AccionesPorEvento[incremento].codigo === "AC-CER-AV" || AccionesPorEvento[incremento].codigo === "AC-VAL-AC" || AccionesPorEvento[incremento].codigo === "AC-VAL-AC.OR" || AccionesPorEvento[incremento].codigo === "AC-REC-AC" || AccionesPorEvento[incremento].codigo === "AC-REC-AC.OR" ){
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);

        var form = [{
          "key": "label",
          "type": "label",
          "templateOptions": {
            "label": modal.label,
            "class":""
          }
        }];

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:undefined,
          formulario:form,
          modal:modal});
      }

      if(AccionesPorEvento[incremento].codigo === "AC-GE-AC.OR"){

        objeto["AC-GE-AC.OR"] = {materiales:[]};
        var formularioActa = JSON.parse(AccionesPorEvento[incremento].parametros.formularioActa);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);

        objeto["AC-GE-AC.OR"].observaciones = formularioActa.observaciones;
        _.each(formularioActa.materiales,function(item,index){

          objeto["AC-GE-AC.OR"].materiales.push({
              id:item.id,
              nombre:item.nombre,
              "cantidad":item.cantidad,
              "cantidadInformada":item.cantidadInformada,
              "valorForm":{
                "key": "cantidadReal",
                "defaultValue":item.cantidadReal,
                "type": "inputNoLabel",
                "templateOptions": {
                  "placeholder": "Ingresar Cantidad Real",
                  "max":(item.cantidad - item.cantidadInformada),
                  "label":"",
                  "type":'number',
                  "required": true
                }
              }
            }
          );

        });


        var tabs = [
          {
            title: 'Items',
            active: true,
            form: {
              options: {},
              fields:[
                {
                  className: 'row',
                  fieldGroup: [
                    {
                      key: 'titulo',
                      type: 'titleFancy',
                      templateOptions: {
                        title:"Items Cubicados"
                      }
                    },
                    {
                      key: 'materiales',
                      type: 'TableForm',
                      templateOptions: {
                        sorting:{nombre:'asc'},
                        requerid:true,
                        columns:[
                          {
                            title: 'Item',
                            type:"label",
                            field:'nombre',
                            visible: true
                          },
                          {
                            title: 'Cantidad Cubicada',
                            type:"label",
                            field: 'cantidad',
                            class:"text-center",
                            visible: true
                          },{
                            title: 'Cantidad Informada',
                            type:"label",
                            field: 'cantidadInformada',
                            class:"text-center",
                            visible: true
                          },
                          {
                            title: 'Cantidad Real',
                            type:"formly",
                            valorFormField:"valorForm",
                            field: 'cantidadReal',
                            visible: true
                          }
                        ]
                      }
                    }
                  ]
                }
              ]
            }
          }];

        var form = [
          {
            template: '<hr />'
          },
          {
            className: 'row',
            fieldGroup: [
              {
                className: 'row',
                fieldGroup: [

                  {
                    key: 'obs',
                    type: 'horizontalTextarea',
                    templateOptions: {
                      label: 'Observaciones',
                      placeholder: 'Observaciones...',
                      required:true
                    }
                  },
                  {
                    key: 'observaciones',
                    type: 'TableForm',
                    templateOptions: {
                      "sorting":{fecha:"desc"},
                      requerid:true,
                      columns:[
                        {
                          title: 'Nombre',
                          type:"label",
                          field:'usuario',
                          visible: true
                        },
                        {
                          title: 'Observación',
                          type:"label",
                          field: 'observaciones',
                          visible: true
                        },
                        {
                          title: 'Fecha',
                          type:"label",
                          formatter:"date",
                          field: 'fecha',
                          class:"text-center",
                          visible: true
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ];

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:undefined,
          formulario:form,
          tabs:tabs,
          modal:modal});
      }

      if(AccionesPorEvento[incremento].codigo === "AC-GE-AC"){

        objeto["AC-GE-AC"] = {materiales:[],servicios:[],serviciosAdicionalesYRechazados:[],materialesAdicionalesYRechazados:[]};
        var formularioActa = JSON.parse(AccionesPorEvento[incremento].parametros.formularioActa);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);

        objeto["AC-GE-AC"].observaciones = formularioActa.observaciones;
        _.each(formularioActa.materiales,function(item,index){

          objeto["AC-GE-AC"].materiales.push({
              id:item.id,
              nombre:item.nombre,
              "cantidad":item.cantidad,
              "cantidadInformada":item.cantidadInformada,
              "valorForm":{
                "key": "cantidadReal",
                "defaultValue":item.cantidadReal,
                "type": "inputNoLabel",
                "templateOptions": {
                  "placeholder": "Ingresar Cantidad Real",
                  "label":"",
                  "type":'number',
                  "required": true
                }
              }
            }
          );

        });
        _.each(formularioActa.servicios,function(item,index){

          objeto["AC-GE-AC"].servicios.push({
              id:item.id,
              nombre:item.nombre,
              descripcion:item.descripcion,
              "cantidadInformada":item.cantidadInformada,
              "cantidad":item.cantidad,
              "valorForm":{
                "key": "cantidadReal",
                "defaultValue":item.cantidadReal,
                "type": "inputNoLabel",
                "templateOptions": {
                  "placeholder": "Ingresar Cantidad Real",
                  "label":"",
                  "type":'number',
                  "required": true
                }
              }
            }
          );

        });

        _.each(formularioActa.serviciosAdicionales,function(item,index){

          objeto["AC-GE-AC"].serviciosAdicionalesYRechazados.push({
              id:item.id,
              nombre:item.nombre,
              descripcion:item.descripcion,
              "cantidadInformada":item.cantidadInformada,
              "cantidad":item.cantidad,
              "validacionUsuario":item.validacionUsuario,
              "valorForm":{
                "key": "cantidadReal",
                "defaultValue":item.cantidadReal,
                "type": "inputNoLabel",
                "templateOptions": {
                  "placeholder": "Ingresar Cantidad Real",
                  "label":"",
                  "type":'number',
                  "required": true
                }
              }
            }
          );

        });

        _.each(formularioActa.materialesAdicionales,function(item,index){

          objeto["AC-GE-AC"].materialesAdicionalesYRechazados.push({
              id:item.id,
              nombre:item.nombre,
              descripcion:item.descripcion,
              "cantidadInformada":item.cantidadInformada,
              "cantidad":item.cantidad,
              "validacionUsuario":item.validacionUsuario,
              "valorForm":{
                "key": "cantidadReal",
                "defaultValue":item.cantidadReal,
                "type": "inputNoLabel",
                "templateOptions": {
                  "placeholder": "Ingresar Cantidad Real",
                  "label":"",
                  "type":'number',
                  "required": true
                }
              }
            }
          );

        });

        var form = [
          {
            template: '<hr />'
          },
          {
            className: 'row',
            fieldGroup: [
              {
                className: 'row',
                fieldGroup: [

                  {
                    key: 'obs',
                    type: 'horizontalTextarea',
                    templateOptions: {
                      label: 'Observaciones',
                      placeholder: 'Observaciones...',
                      required:true
                    }
                  },
                  {
                    key: 'observaciones',
                    type: 'TableForm',
                    templateOptions: {
                      "sorting":{fecha:"desc"},
                      requerid:true,
                      columns:[
                        {
                          title: 'Nombre',
                          type:"label",
                          field:'usuario',
                          visible: true
                        },
                        {
                          title: 'Observación',
                          type:"label",
                          field: 'observaciones',
                          visible: true
                        },
                        {
                          title: 'Fecha',
                          type:"label",
                          formatter:"date",
                          field: 'fecha',
                          class:"text-center",
                          visible: true
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ];

        var tabs = [
          {
            title: 'Servicios',
            active: true,
            form: {
              options: {},
              fields:[
                {
                  className: 'row',
                  fieldGroup: [
                    {
                      key: 'titulo',
                      type: 'titleFancy',
                      templateOptions: {
                        title:"Servicios Costeados"
                      }
                    },
                    {
                      key: 'servicios',
                      type: 'TableForm',
                      templateOptions: {
                        requerid:true,
                        columns:[
                          {
                            title: 'Descripción',
                            type:"label",
                            field:'descripcion',
                            visible: true
                          },
                          {
                            title: 'Cantidad Costeada',
                            type:"label",
                            field: 'cantidad',
                            class:"text-center",
                            visible: true
                          },{
                            title: 'Cantidad Informada',
                            type:"label",
                            field: 'cantidadInformada',
                            class:"text-center",
                            visible: true
                          },
                          {
                            title: 'Cantidad Real',
                            type:"formly",
                            valorFormField:"valorForm",
                            field: 'cantidadReal',
                            visible: true
                          }
                        ]
                      }
                    }
                  ]
                },
                {
                  className: 'row',
                  fieldGroup: [
                    {
                      key: 'titulo',
                      type: 'titleFancy',
                      templateOptions: {
                        title:"Servicios Adicionales Aprobados y Rechazados"
                      }
                    },
                    {
                      key: 'serviciosAdicionalesYRechazados',
                      type: 'TableForm',
                      templateOptions: {
                        "sorting":{validacionUsuario:"desc"},
                        requerid:true,
                        columns:[
                          {
                            title: 'Descripción',
                            type:"label",
                            field:'descripcion',
                            visible: true
                          },
                          {
                            title: 'Aprobado',
                            type:"iconBoolean",
                            field: 'validacionUsuario',
                            class:"text-center",
                            visible: true
                          },
                          {
                            title: 'Cantidad',
                            type:"formly",
                            valorFormField:"valorForm",
                            field: 'cantidadReal',
                            visible: true
                          },
                          {
                            title: 'Remover',
                            type:"remover",
                            class:"text-center",
                            visible: true
                          }
                        ]
                      }
                    }
                  ]
                },
                {
                  className: 'row',
                  fieldGroup: [
                    {
                      key: 'titulo',
                      type: 'titleFancy',
                      templateOptions: {
                        title:"Servicios Adicionales"
                      }
                    },
                    {
                      "key": "tipoServicio",
                      "type": "horizontalUiSelectGET",
                      "templateOptions": {
                        "placeholder": "Tipo Servicio",
                        "label": "Tipo Servicio",
                        "labelProp": "nombre",
                        "action":"listarTipoServicioPorContrato",
                        "parametros":function(model){
                          return{contratoId:objeto.contrato.id}
                        },
                      }
                    },
                    {
                      "key": "serviciosAdicionales",
                      "type": "horizontalUiMultiSelectInputGETCascadeNoLabel",
                      "templateOptions": {
                        "rejectOptions": [objeto["AC-GE-AC"].servicios,objeto["AC-GE-AC"].serviciosAdicionalesYRechazados],
                        "subkey":"servicio",
                        "placeholder": "Servicios",
                        "label": "Servicios",
                        "labelProp": "descripcion",
                        "parametros":function(model){
                          return{otId:model.otId,tipoServicioId:model.tipoServicio.id};
                        },
                        "toWatch":'tipoServicio',
                        "get":true,
                        "action":"listarServiciosExtrasPorOt",
                        "input":{label:"Cantidad", min:0, "placeholder": "Cantidad",inputProp:"cantidad",onSelect:function(item,model,scope){
                          item.cantidad=1;
                        }, required:true,type:"number"}
                      },
                      hideExpression:"model.tipoServicio == undefined"
                    },
                    {
                      className: 'row',
                      fieldGroup: [{
                        key:"tableServicios",
                        type:"simpleTable",
                        templateOptions:{
                          title:"Resumen Servicios Adicionales",
                          targetkey:"serviciosAdicionales",
                          columns:[{
                            nestedField:"descripcion",
                            field: "servicio",
                            title: 'Descripción',
                            sortable: "descripcion",
                            visible: true
                          }, {
                            field: "cantidad",
                            title: 'Cantidad',
                            sortable: "name",
                            visible: true,
                            class:"text-center",
                            groupable: "name"
                          }, {
                            nestedField:"precio",
                            field: "servicio",
                            title: 'Precio',
                            currency:true,
                            class:"text-center",
                            sortable: "precio",
                            visible: true
                          },{
                            expression:function(objeto){
                              var total = 0;
                              var cantidad = 0;
                              if(objeto.cantidad){ cantidad = objeto.cantidad}
                              total = cantidad * objeto.servicio.precio;
                              objeto.montoTotal = total;
                              return total;
                            },
                            field: "total",
                            currency:true,
                            class:"text-center",
                            title: 'Total',
                            sortable: "total",
                            visible: true
                          }]
                        }
                      }
                      ]
                    }
                  ]
                }
              ]
            }
          },
          {
            title: 'Materiales',
            form: {
              options: {},
              fields:  [
                {
                  className: 'row',
                  fieldGroup:[
                    {
                      className: 'row',
                      fieldGroup: [
                        {
                          key: 'titulo',
                          type: 'titleFancy',
                          templateOptions: {
                            title:"Materiales Costeados"
                          }
                        },
                        {
                          key: 'materiales',
                          type: 'TableForm',
                          templateOptions: {
                            requerid:true,
                            columns:[
                              {
                                title: 'Nombre',
                                type:"label",
                                field: 'nombre',
                                visible: true
                              },
                              {
                                title: 'Cantidad Costeada',
                                type:"label",
                                field: 'cantidad',
                                class:"text-center",
                                visible: true
                              },{
                                title: 'Cantidad Informada',
                                type:"label",
                                field: 'cantidadInformada',
                                class:"text-center",
                                visible: true
                              },
                              {
                                title: 'Cantidad Real',
                                type:"formly",
                                valorFormField:"valorForm",
                                field: 'cantidadReal',
                                visible: true
                              }
                            ]
                          }
                        }]
                    },
                    {
                      className: 'row',
                      fieldGroup: [
                        {
                          key: 'titulo',
                          type: 'titleFancy',
                          templateOptions: {
                            title:"Materiales Adicionales Aprobados y Rechazados"
                          }
                        },
                        {
                          key: 'materialesAdicionalesYRechazados',
                          type: 'TableForm',
                          templateOptions: {
                            "sorting":{validacionUsuario:"desc"},
                            requerid:true,
                            columns:[
                              {
                                title: 'Descripción',
                                type:"label",
                                field:'descripcion',
                                visible: true
                              },
                              {
                                title: 'Aprobado',
                                type:"iconBoolean",
                                field: 'validacionUsuario',
                                class:"text-center",
                                visible: true
                              },
                              {
                                title: 'Cantidad',
                                type:"formly",
                                valorFormField:"valorForm",
                                field: 'cantidadReal',
                                visible: true
                              },
                              {
                                title: 'Remover',
                                type:"remover",
                                class:"text-center",
                                visible: true
                              }
                            ]
                          }
                        }
                      ]
                    },
                    {
                      className: 'row',
                      fieldGroup: [
                        {
                          key: 'titulo',
                          type: 'titleFancy',
                          templateOptions: {
                            title:"Materiales Adicionales"
                          }
                        },
                        {
                          "key": "materialesAdicionales",
                          "type": "horizontalUiMultiSelectInputGETCascadeNoLabel",
                          "templateOptions": {
                            "rejectOptions": [objeto["AC-GE-AC"].materiales,objeto["AC-GE-AC"].materialesAdicionalesYRechazados],
                            "subkey":"material",
                            "placeholder": "Materiales",
                            "label": "Materiales",
                            "labelProp": "nombre",
                            "metodo":"Material",
                            "input":{label:"Cantidad", "placeholder": "Cantidad",inputProp:"cantidad",required:true,type:"number"}
                          }
                        },
                        {
                          className: 'row',
                          fieldGroup: [{
                            key:"tableMateriales",
                            type:"simpleTable",
                            templateOptions:{
                              title:"Resumen Materiales Adicionales",
                              targetkey:"materialesAdicionales",
                              columns:[{
                                nestedField:"nombre",
                                field: "material",
                                title: 'Nombre',
                                sortable: "nombre",
                                visible: true
                              }, {
                                field: "cantidad",
                                title: 'Cantidad',
                                sortable: "name",
                                visible: true,
                                class:"text-center",
                                groupable: "name"
                              }, {
                                nestedField:"precio",
                                field: "material",
                                title: 'Precio',
                                sortable: "precio",
                                currency:true,
                                class:"text-center",
                                visible: true
                              },{
                                expression:function(objeto){
                                  var total = 0;
                                  var cantidad = 0;
                                  if(objeto.cantidad){ cantidad = objeto.cantidad}
                                  total = cantidad * objeto.material.precio;
                                  objeto.montoTotal = total;
                                  return total;
                                },
                                field: "total",
                                title: 'Total',
                                currency:true,
                                sortable: "total",
                                class:"text-center",
                                visible: true
                              }]
                            }
                          }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          }
        ];

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:undefined,
          formulario:form,
          tabs:tabs,
          modal:modal});

      }
      //"AC-VA-AC"
      if(AccionesPorEvento[incremento].codigo === "AC-VA-AC.OR") {
        objeto["AC-VA-AC.OR"] = JSON.parse(AccionesPorEvento[incremento].parametros.formularioActa);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);

        objeto["AC-VA-AC.OR"].validacionSistemaText = " Se ha validado esta Acta <i class='icon-thumbs icon-thumbs-up glyphicon glyphicon-thumbs-up'></i>";

        //borrar o no el Boton de Rechazo, si esque la acta es exactamente igual en servicios que la cubicación
        var borrarBotonRechazar = true;
          _.each(objeto["AC-VA-AC.OR"].detalleMaterialesActa,function(item){
            if(item.cantidadActualInformada !== item.cantidadCubicada){
              borrarBotonRechazar = false
            }
          });
        if(borrarBotonRechazar){
          modal.botones.shift();
        }

        var form = [
          {
            className: 'row',
            fieldGroup: [
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Fechas"
                }
              },
              {
                "key": "fechaCreacion",
                "type": "horizontalDatepicker",
                "templateOptions": {
                  "fecha":true,
                  "label": "Fecha Creación",
                  "disabled":true,
                  "type": "text",
                  "datepickerPopup": "dd-MM-yyyy"
                }
              },
              {
                "key": "fechaTerminoEstimada",
                "type": "horizontalDatepicker",
                "templateOptions": {
                  "fecha":true,
                  "disabled":true,
                  "label": "Fecha Término Estimada",
                  "type": "text",
                  "datepickerPopup": "dd-MM-yyyy"
                }
              },
              {
                template:"<div class=\'form-spacing\'></div>"
              },
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Validación de Sistema"
                },
                hideExpression:"model.validacionSistema == 'false'"
              },
              {
                key:"validacionSistemaText",
                type:"horizontalSimpleTextNoLabel",
                templateOptions: {
                  ngClass:'legend',
                  class:'text-center'
                },
                hideExpression:"model.validacionSistema == 'false'"
              },
              {
                template:"<div class=\'form-spacing\'></div>"
              },
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Observaciones"
                }
              },
              {
                key: 'obs',
                type: 'horizontalTextarea',
                templateOptions: {
                  label: 'Observaciones',
                  placeholder: 'Observaciones...',
                  required:true
                }
              },
              {
                template:"<div class=\'form-spacing\'></div>"
              },
              {
                key: 'observaciones',
                type: 'TableForm',
                templateOptions: {
                  "sorting":{fecha:"desc"},
                  requerid:true,
                  columns:[
                    {
                      title: 'Nombre',
                      type:"label",
                      field:'usuario',
                      visible: true
                    },
                    {
                      title: 'Observación',
                      type:"label",
                      field: 'observaciones',
                      visible: true
                    },
                    {
                      title: 'Fecha',
                      type:"label",
                      field: 'fecha',
                      formatter:"date",
                      class:"text-center",
                      visible: true
                    }
                  ]
                }
              }
            ]
          }
        ];

        var tabs = [
          {
            title: 'Items',
            active: true,
            form: {
              options: {},
              fields: [
                {
                  className: 'row',
                  fieldGroup: [
                    {
                      key: 'titulo',
                      type: 'titleFancy',
                      templateOptions: {
                        title:"Items Cubicados"
                      }
                    },
                    {
                      key: 'detalleMaterialesActa',
                      type: 'TableForm',
                      templateOptions: {
                        sorting:{nombre:'asc'},
                        requerid:true,
                        columns:headerTableActaOrdinario
                      }
                    }
                  ]
                }
              ]
            }
          }];

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:undefined,
          formulario:form,
          tabs:tabs,
          modal:modal});
      }

      if(AccionesPorEvento[incremento].codigo === "AC-VA-AC"){
        objeto["AC-VA-AC"] = JSON.parse(AccionesPorEvento[incremento].parametros.formularioActa);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);

        objeto["AC-VA-AC"].validacionSistemaText = " Se ha validado esta Acta <i class='icon-thumbs icon-thumbs-up glyphicon glyphicon-thumbs-up'></i>";

        //borrar o no el Boton de Rechazo, si esque la acta es exactamente igual en servicios que la cubicación
        var borrarBotonRechazar = true;
        if(objeto["AC-VA-AC"].detalleServiciosAdicionalesActa.length == 0){
          _.each(objeto["AC-VA-AC"].detalleServiciosActa,function(item){
            if(item.cantidadActualInformada !== item.cantidadCubicada){
             borrarBotonRechazar = false
            }
          });
        }else{
          borrarBotonRechazar = false
        }

        if(borrarBotonRechazar){
          modal.botones.shift();
        }

        var form = [
          {
            className: 'row',
            fieldGroup: [
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Fechas"
                }
              },
              {
                "key": "fechaCreacion",
                "type": "horizontalDatepicker",
                "templateOptions": {
                  "fecha":true,
                  "label": "Fecha Creación",
                  "disabled":true,
                  "type": "text",
                  "datepickerPopup": "dd-MM-yyyy"
                }
              },
              {
                "key": "fechaTerminoEstimada",
                "type": "horizontalDatepicker",
                "templateOptions": {
                  "fecha":true,
                  "disabled":true,
                  "label": "Fecha Término Estimada",
                  "type": "text",
                  "datepickerPopup": "dd-MM-yyyy"
                }
              },
              {
                template:"<div class=\'form-spacing\'></div>"
              },
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Validación de Sistema"
                },
                hideExpression:"model.validacionSistema == 'false'"
              },
              {
                key:"validacionSistemaText",
                type:"horizontalSimpleTextNoLabel",
                templateOptions: {
                  ngClass:'legend',
                  class:'text-center'
                },
                hideExpression:"model.validacionSistema == 'false'"
              },
              {
                template:"<div class=\'form-spacing\'></div>"
              },
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Observaciones"
                }
              },
              {
                key: 'obs',
                type: 'horizontalTextarea',
                templateOptions: {
                  label: 'Observaciones',
                  placeholder: 'Observaciones...',
                  required:true
                }
              },
              {
                template:"<div class=\'form-spacing\'></div>"
              },
              {
                key: 'observaciones',
                type: 'TableForm',
                templateOptions: {
                  "sorting":{fecha:"desc"},
                  requerid:true,
                  columns:[
                    {
                      title: 'Nombre',
                      type:"label",
                      field:'usuario',
                      visible: true
                    },
                    {
                      title: 'Observación',
                      type:"label",
                      field: 'observaciones',
                      visible: true
                    },
                    {
                      title: 'Fecha',
                      type:"label",
                      field: 'fecha',
                      formatter:"date",
                      class:"text-center",
                      visible: true
                    }
                  ]
                }
              }
            ]
          }
        ];


        var tabs = [
          {
            title: 'Servicios',
            active: true,
            form: {
              options: {},
              fields: [
                {
                  className: 'row',
                  fieldGroup: [
                    {
                      key: 'titulo',
                      type: 'titleFancy',
                      templateOptions: {
                        title:"Servicios Costeados"
                      }
                    },
                    {
                      key: 'detalleServiciosActa',
                      type: 'TableForm',
                      templateOptions: {
                        requerid:true,
                        columns:headerTableActa
                      }
                    },
                    {
                      key: 'titulo',
                      type: 'titleFancy',
                      templateOptions: {
                        title:"Servicios Adicionales"
                      }
                    },
                    {
                      key: 'detalleServiciosAdicionalesActa',
                      type: 'TableForm',
                      templateOptions: {
                        requerid:true,
                        columns:headerTableActaAdionales
                      }
                    }
                  ]
                }
              ]
            }
          },
          {
            title: 'Materiales',
            form: {
              options: {},
              fields: [
                {
                  className: 'row',
                  fieldGroup: [
                    {
                      key: 'titulo',
                      type: 'titleFancy',
                      templateOptions: {
                        title:"Materiales Costeados"
                      }
                    },
                    {
                      key: 'detalleMaterialesActa',
                      type: 'TableForm',
                      templateOptions: {
                        requerid:true,
                        columns: headerTableActa
                      }
                    },
                    {
                      key: 'titulo',
                      type: 'titleFancy',
                      templateOptions: {
                        title:"Materiales Adicionales"
                      }
                    },
                    {
                      key: 'detalleMaterialesAdicionalesActa',
                      type: 'TableForm',
                      templateOptions: {
                        requerid:true,
                        columns:headerTableActaAdionales
                      }
                    }
                  ]
                }
              ]
            }
          }
        ];
        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:undefined,
          formulario:form,
          tabs:tabs,
          modal:modal});
      }

      if(AccionesPorEvento[incremento].codigo === "AC-TI-PA"){
        //objeto["AC-VA-AC"] = JSON.parse(AccionesPorEvento[incremento].parametros.formularioActa);
        var form = JSON.parse(AccionesPorEvento[incremento].parametros.formulario);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);


        form.push(
          {
            key: 'montoDiponiblePagoPorcentage',
            type: 'horizontalSlider',
            templateOptions:{
              defaultValue:50,
              ceil:100,
              floor:0,
              step:0.1,
              precision: 1,
              label:'Porcentaje',
              translate: function(value) {
                return value+'%';
              },
              onChangeModel:function(model,newVal,oldVal,scope){
                if(!model.montoDiponiblePagoAux){model.montoDiponiblePagoAux = model.montoDiponiblePago;}
                model.montoDiponiblePago = model.montoDiponiblePagoAux * (model.montoDiponiblePagoPorcentage/100);
              }
            },
            "validators":{
              "archivosVal":"$modelValue == 100"
            }
          });
        //form.push(
        //  {
        //    "key": "montoDiponiblePagoPorcentage",
        //    type: 'horizontalInput',
        //    templateOptions: {
        //      type:'number',
        //      label:'%',
        //    }
        //  });

        //form.push(
        //  {
        //    "key": "montoDiponiblePagoPorcentage",
        //    type: 'horizontalInput',
        //    templateOptions: {
        //      type:'number',
        //      label:'%',
        //    }
        //  });

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:undefined,
          formulario:form,
          modal:modal});
      }
      if(AccionesPorEvento[incremento].codigo === "AC-TI-PA.OR"){
        //objeto["AC-VA-AC"] = JSON.parse(AccionesPorEvento[incremento].parametros.formularioActa);
        var form = JSON.parse(AccionesPorEvento[incremento].parametros.formulario);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);

        var formCubicado = _.find(form, function(item) { return item.key === "montoCubicado"; });
        var formActa = _.find(form, function(item) { return item.key === "montoActa"; });
        var porcentajeAux = (100*formActa.defaultValue)/formCubicado.defaultValue;
        var porcentaje = Math.round(porcentajeAux*100)/100;

        objeto["AC-TI-PA.OR"] = {texto:"<strong>Se pagará el "+porcentaje+"% del monto cubicado total.</strong>"};

        form.push({
          key:"texto",
          type:"horizontalSimpleTextNoLabel",
          templateOptions: {
            ngClass:'legend',
            class:'text-center'
          }
        });

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:undefined,
          formulario:form,
          modal:modal});
      }

      if(AccionesPorEvento[incremento].codigo === "AC-VA-PM"){
        //objeto["AC-VA-AC"] = JSON.parse(AccionesPorEvento[incremento].parametros.formularioActa);
        var datos = JSON.parse(AccionesPorEvento[incremento].parametros.formulario);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);
        objeto["AC-VA-PM"] = {idPmo:datos.detalleOt.idPmo.id, idActa:datos.idActa,lp:datos.detalleOt.lp.id};
        var form = [
          {
            "key": "idPmo",
            "type": "horizontalSimpleText",
            "templateOptions": {
              "label": "ID-PMO"
            }
          },
          {
            "key": "lp",
            "type": "horizontalSimpleText",
            "templateOptions": {
              "label": "LP"
            }
          }
        ];

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:undefined,
          formulario:form,
          modal:modal});
      }
      if(AccionesPorEvento[incremento].codigo === "AC-VA-IM"){
        var datos = JSON.parse(AccionesPorEvento[incremento].parametros.formulario);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);
        objeto["AC-VA-IM"] = {pep2:datos.detalleOt.pep2, idActa:datos.idActa};
        var form = [
          {
            "key": "pep2",
            "type": "horizontalSimpleText",
            "templateOptions": {
              "label": "PEP2"
            }
          }
        ];

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:undefined,
          formulario:form,
          modal:modal});
      }
      if(AccionesPorEvento[incremento].codigo === "AC-AD-CA"){
        var form = JSON.parse(AccionesPorEvento[incremento].parametros.formulario);
        var modal = JSON.parse(AccionesPorEvento[incremento].parametros.modal);

        //var form = [
        //  {
        //    "key": "archivos",
        //    "type": "multipleFileUpload",
        //    "templateOptions": {
        //      "action": "adjuntarCartaAdjudicacion",
        //      "objCoreOt": "Adjunto",
        //      "noObservaciones": true,
        //      "uploadButtonTitle": "Adjuntar Carta de Adjudicación",
        //      "mensajeExito": "Carta Adjuntada con Éxito",
        //      "mensajeError": "Error al Adjuntar Carta de Adjudicación",
        //      "filesRequeried": true,
        //      "required":true
        //    },
        //    "validators":{
        //      "archivosVal":"$modelValue"
        //    }
        //  }
        //];

        _.extend(opciones,{
          codigo:AccionesPorEvento[incremento].codigo,
          AccionesPorEvento:AccionesPorEvento,
          proximaAccion:undefined,
          formulario:form,
          modal:modal});
      }

      var objFinal = {opciones:opciones,objeto:objeto};

      return objFinal;
    }


    return {
      toSelect:toSelect,
      prepararObjeto:prepararObjeto,
      prepareEjecutarEvento:prepareEjecutarEvento
    };
  });
