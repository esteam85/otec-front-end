'use strict';

/**
 * @ngdoc service
 * @name otecApp.formlyCustomOtecTypes
 * @description
 * # formlyCustomOtecTypes
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('formlyCustomOtecTypes', function () {

      var valores =  ["horizontalInput","horizontalTextarea","horizontalCheckbox","horizontalRadios","horizontalUiSelect","horizontalUiMultiSelect","horizontalUiMultiSelectInput","horizontalInputRut","horizontalInputMask","horizontalDatepicker","keyValueTable","UISelectTable"];

    return valores.sort(function (a, b) {
          if (a < b) return 1;
          if (b < a) return -1;
          return 0;
        });
  }
  );
