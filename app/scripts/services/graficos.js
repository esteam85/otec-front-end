'use strict';

/**
 * @ngdoc service
 * @name otecApp.graficos
 * @description
 * # graficos
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('GraficosService', function ($timeout) {
    //Para Sub-Gerente

    //Para Gestor
    //Cantidad de Otes Cerradas vs En Curso
    function QotCerradasVSEnCurso() {
      return {
        options:{
          chart:{
            type:"spline"
          }
        },
        title: {
          text: 'Q Ot Cerradas vs En Curso',
        },
        subtitle: {
          text: 'Cantidad de Ot Cerradas vs Ot En Curso',
        },
        xAxis: {
          categories: ['Ene', 'Feb', 'Mar', 'Abri', 'May', 'Jun',
            'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
          title: {
            text: 'Cantidad'
          },
          plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
          }]
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          borderWidth: 0
        },
        series: [{
          name: 'Cerradas',
          data: randomNaturales(12,30)
        }, {
          name: 'En Curso',
          data: randomNaturales(12,30)
        }]
      };
    }

    function QapCerradasVSEnCurso() {
      return {
        options:{
          chart:{
            type:"spline"
          }
        },
        title: {
          text: 'Q Ap Cerradas vs En Curso',
        },
        subtitle: {
          text: 'Cantidad de Ap Cerradas vs AP En Curso',
        },
        xAxis: {
          categories: ['Ene', 'Feb', 'Mar', 'Abri', 'May', 'Jun',
            'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
          title: {
            text: 'Cantidad'
          },
          plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
          }]
        },
        legend: {
        },
        series: [{
          name: 'Cerradas',
          data: randomNaturales(12,30)
        }, {
          name: 'En Curso',
          data: randomNaturales(12,30)
        }]
      };
    }

    function QotPorProveedor(){
      return {
        options: {
          chart: {
            type: 'pie',
            options3d: {
              enabled: true,
              alpha: 45,
              beta: 0,
              depth: 70
            }
          }},
        title: {
          text: 'Q de OT En Curso por Empresa Colaboradora'
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}</b>'
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
              enabled: true,
              format: '{point.name}'
            }
          }
        },
        series: [{
          type: 'pie',
          name: 'Q de Ot',
          data: [
            ['Nokia', randomNatural(100)],
            ['Huawei', randomNatural(100)],
            ['AJ Ingenieros', randomNatural(100)],
            ['ZTE',     randomNatural(100)]
          ]
        }]
      };
    };

    function MontoPagadosPorProveedor(){

      return {
        options: {
          chart: {
            type: 'column',
            margin: 75,
            options3d: {
              enabled: true,
              alpha: 10,
              beta: 25
            }
          }
        },
        title: {
          text: 'Montos Pagado por Proveedor'
        },
        subtitle: {
          text: 'Monto actualmente pagado por proveedor'
        },
        xAxis: {
          categories: ['Huawei', 'Nokia', 'ZTE', 'AJ Ingenieros'],
          title: {
            text: null
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Monto'
          },
          labels: {
            overflow: 'justify'
          }
        },
        plotOptions: {
          bar: {
            dataLabels: {
              enabled: true
            }
          }
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'Monto',
          data: randomNaturales(4,100000000)
        }]
      };
    };


    //Subgerente
    function PagosDevengadosPorPrograma(){

      return  {
        options: {
          tooltip: {
            valuePrefix: '$'
          },
          chart: {
            type: 'column',
            options3d: {
              enabled: false,
              alpha: 10,
              beta: 25
            }
          }
        },
        title: {
          text: 'Pagos Devengados Por Programa'
        },
        subtitle: {
          text: 'Pagos Devengados Por Programa'
        },
        xAxis: {
          categories: ["Reing. Y Traslados","Small Cells 3G - Indoor Empresas","Indoor Masivo - Llave en mano","Mineras","Polígonos 700 - Saltos Tx","Small Cells 3G - Micro","Small Cells 4G - Micro","Sectores adicionales","Small Cells 3G - Indoor Masivo","3P - Micro","Indoor Empresas","Small Cells 4G - Indoor Masivo","Polígonos 2600","Reing. y Traslados","Voz Poligonos Satelitales","Indoor Masivo","HBTS","Polígonos 700","3G","Swap Vendor LTE","4P","LTE 700","3P","LTE 2600"],
          title: {
            text: null
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Pagos Devengados',
          },
          labels: {
            overflow: 'justify'
          }
        },
        tooltip: {
          valuePrefix: '$'
        },
        plotOptions: {
          bar: {
            dataLabels: {
              enabled: true
            }
          }
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'Pagos',
          data: randomNaturales(24,100000000)
        }]
      };
    }

    function QotPorContratoPorPeriodo(){
      return {
        options:{
          drilldown: {
            series:[{
              id: 'ago',
              name:"Contratos",
              data: [
                ['Ran', randomNatural(100)],
                ['SBE', randomNatural(100)],
                ['Ordinario', randomNatural(100)],
                ['Unificado', randomNatural(100)],
                ['Salas', randomNatural(100)]
                  ['Bucle', randomNatural(100)]
              ]
            },
              {
                id: 'sep',
                name:"Contratos",
                data: [
                  ['Ran', randomNatural(100)],
                  ['SBE', randomNatural(100)],
                  ['Ordinario', randomNatural(100)],
                  ['Unificado', randomNatural(100)],
                  ['Salas', randomNatural(100)]
                    ['Bucle', randomNatural(100)]
                ]
              },
              {
                id: 'oct',
                name:"Contratos",
                data: [
                  ['Ran', randomNatural(100)],
                  ['SBE', randomNatural(100)],
                  ['Ordinario', randomNatural(100)],
                  ['Unificado', randomNatural(100)],
                  ['Salas', randomNatural(100)]
                    ['Bucle', randomNatural(100)]
                ]
              },
              {
                id: 'nov',
                name:"Contratos",
                data: [
                  ['Ran', randomNatural(100)],
                  ['SBE', randomNatural(100)],
                  ['Ordinario', randomNatural(100)],
                  ['Unificado', randomNatural(100)],
                  ['Salas', randomNatural(100)]
                    ['Bucle', randomNatural(100)]
                ]
              },
              {
                id: 'dic',
                name:"Contratos",
                data: [
                  ['Ran', randomNatural(100)],
                  ['SBE', randomNatural(100)],
                  ['Ordinario', randomNatural(100)],
                  ['Unificado', randomNatural(100)],
                  ['Salas', randomNatural(100)]
                    ['Bucle', randomNatural(100)]
                ]
              }]
          },
          chart: {
            type: 'column'
          }
        },
        title: {
          text: 'Q de Ot por Contrato por Periodo'
        },
        xAxis: {
          type: 'category'
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Cantidad de Ot'
          }
        },
        tooltip: {
          pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
          shared: true
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'Ot',
          colorByPoint: true,
          data: [{name:'Ago',y:randomNatural(100),drilldown:"ago"},{name:'Sep',y:randomNatural(100),drilldown:"sep"},{name:'Oct',y:randomNatural(100),drilldown:"oct"},{name:'Nov',y:randomNatural(100),drilldown:"nov"},{name:'Dic',y:randomNatural(100),drilldown:"dic"}]
        }]
      };
    }

    function MontoDeOtPorContratoPorPeriodo(){
      return {
        options:{
          chart: {
            type: 'column'
          },
          tooltip: {
            valuePrefix: '$',
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
            shared: true
          },
          "plotOptions":
          {"series":
          {borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{point.y:f}'
            },
            //"stacking":"normal"
          }
          }
        },
        title: {
          text: 'Monto de Ot por Contrato por Periodo'
        },
        xAxis: {
          categories: ['Agos', 'Sep', 'Oct', 'Nov', 'Dic']
        },

        yAxis: {
          min: 0,
          title: {
            text: 'Monto por Contrato'
          }
        },
        credits: {
          enabled: false
        },

        series: [{
          name: 'Ran',
          data: randomNaturales(5,100000)
        }, {
          name: 'SBE',
          data: randomNaturales(5,100000)
        }, {
          name: 'Ordinario',
          data: randomNaturales(5,100000)
        },
          {
            name: 'Unificado',
            data: randomNaturales(5,100000)
          },
          {
            name: 'Salas',
            data: randomNaturales(5,100000)
          },
          {
            name: 'Bucle',
            data: randomNaturales(5,100000)
          }]
      };
    }

    function PagosEstimadosVsPagosReales(){
      return {
        options:{
          tooltip: {
            valuePrefix: '$'
          },
          chart:{
            type:"line"
          }
        },
        title: {
          text: 'Pagos Estimados vs Pagos Reales'
        },
        subtitle: {
          text: 'Pagos Estimados vs Pagos Reales - 2015'
        },
        xAxis: {
          categories: ['Ene', 'Feb', 'Mar', 'Abri', 'May', 'Jun',
            'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
          title: {
            text: 'Pagos'
          },
          plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
          }]
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          borderWidth: 0
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'Estimados',
          data: randomNaturales(12,10000000)
        }, {
          name: 'Reales',
          data: randomNaturales(12,10000000)
        }]
      };
    }

    //GEstion economica

    function PagosPorProveedorPorPeriodoEncurso(){
      return {
        options:{
          tooltip: {
            valuePrefix: '$'
          },
          chart:{
            type:"column"
          }
        },
        title: {
          text: 'Pagos por Proveedor - En curso'
        },
        subtitle: {
          text: 'Pagos por Proveedor Aun no Cancelados - último cuatrimestre 2015'
        },
        xAxis: {
          categories: [ 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
          title: {
            text: 'Pagos'
          },
          plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
          }]
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          borderWidth: 0
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'Nokia',
          data: randomNaturales(4,10000000)
        }, {
          name: 'Huawei',
          data: randomNaturales(4,10000000)
        },{
          name: 'AJ Ingenieros',
          data: randomNaturales(4,10000000)
        },{
          name: 'ZTE',
          data: randomNaturales(4,10000000)
        }]
      };
    }

    function PagosPorProveedorPorPeriodoFinalizados(){
      return {
        options:{
          tooltip: {
            valuePrefix: '$'
          },
          chart:{
            type:"column"
          }
        },
        title: {
          text: 'Pagos por Proveedor - Finalizados'
        },
        subtitle: {
          text: 'Pagos por Proveedor Ya Cancelados - último cuatrimestre 2015'
        },
        xAxis: {
          categories: ['Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
          title: {
            text: 'Pagos'
          },
          plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
          }]
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          borderWidth: 0
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'Nokia',
          data: randomNaturales(4,10000000)
        }, {
          name: 'Huawei',
          data: randomNaturales(4,10000000)
        },{
          name: 'AJ Ingenieros',
          data: randomNaturales(4,10000000)
        },{
          name: 'ZTE',
          data: randomNaturales(4,10000000)
        }]
      };
    }

    function CargaTotalPorProveedorPorPeriodo(){
      return {
        options:{
          tooltip: {
            valuePrefix: '$'
          },
          chart:{
            type:"spline"
          }
        },
        title: {
          text: 'Carga Total por Proveedor'
        },
        subtitle: {
          text: 'Carga Total por Proveedor - último cuatrimestre 2015'
        },
        xAxis: {
          categories: ['Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
          title: {
            text: 'Pagos'
          },
          plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
          }]
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          borderWidth: 0
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'Nokia',
          data: randomNaturales(4,10000000)
        }, {
          name: 'Huawei',
          data: randomNaturales(4,10000000)
        },{
          name: 'AJ Ingenieros',
          data: randomNaturales(4,10000000)
        },{
          name: 'ZTE',
          data: randomNaturales(4,10000000)
        }]
      };
    }


    function CargaTotalPorProveedorPorPeriodoTrimestreAnterior(){
      return {
        options:{
          tooltip: {
            valuePrefix: '$'
          },
          chart:{
            type:"spline"
          }
        },
        title: {
          text: 'Carga Total por Proveedor Cuatrimestre Anterior'
        },
        subtitle: {
          text: 'Carga Total por Proveedor - penúltimo cuatrimestre 2015'
        },
        xAxis: {
          categories: ['May', 'Jun', 'Jul', 'Ago']
        },
        yAxis: {
          title: {
            text: 'Pagos'
          },
          plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
          }]
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          borderWidth: 0
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'Nokia',
          data: randomNaturales(4,10000000)
        }, {
          name: 'Huawei',
          data: randomNaturales(4,10000000)
        },{
          name: 'AJ Ingenieros',
          data: randomNaturales(4,10000000)
        },{
          name: 'ZTE',
          data: randomNaturales(4,10000000)
        }]
      };
    }

//Proveedor
    function PagosPorPeriodoEncurso(){
      return {
        options:{
          tooltip: {
            valuePrefix: '$'
          },
          chart:{
            type:"column",
            margin: 75,
            options3d: {
              enabled: true,
              alpha: 10,
              beta: 25
            }
          }
        },
        title: {
          text: 'Pagos En curso'
        },
        subtitle: {
          text: 'Pagos Aun no Cancelados - último cuatrimestre 2015'
        },
        xAxis: {
          categories: [ 'Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
          title: {
            text: 'Pagos'
          },
          plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
          }]
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          borderWidth: 0
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'Pagos',
          data: randomNaturales(4,10000000)
        }]
      };
    }

    function PagosPorPeriodoFinalizados(){
      return {
        options:{
          tooltip: {
            valuePrefix: '$'
          },
          chart:{
            margin: 75,
            options3d: {
              enabled: true,
              alpha: 10,
              beta: 25
            },
            type:"column"
          }
        },
        title: {
          text: 'Pagos Finalizados'
        },
        subtitle: {
          text: 'Pagos Ya Cancelados - último cuatrimestre 2015'
        },
        xAxis: {
          categories: ['Sep', 'Oct', 'Nov', 'Dic']
        },
        yAxis: {
          title: {
            text: 'Pagos'
          },
          plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
          }]
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          borderWidth: 0
        },
        credits: {
          enabled: false
        },
        series: [{
          name: 'Pagos',
          data: randomNaturales(4,10000000)
        }]
      };
    }


    function randomNaturales (cantidad,limite){
      var respuesta = [];
      for (var i = 0; i < cantidad; i++) {
        respuesta.push(Math.floor(Math.random() * limite) + 1);
      }
      return respuesta;
    }
    function randomNatural (limite){

      return (Math.floor(Math.random() * limite) + 1);
    }
    return {
      QotCerradasVSEnCurso:QotCerradasVSEnCurso,
      QapCerradasVSEnCurso:QapCerradasVSEnCurso,
      QotPorProveedor:QotPorProveedor,
      MontoPagadosPorProveedor:MontoPagadosPorProveedor,
      PagosDevengadosPorPrograma:PagosDevengadosPorPrograma,
      QotPorContratoPorPeriodo:QotPorContratoPorPeriodo,
      MontoDeOtPorContratoPorPeriodo:MontoDeOtPorContratoPorPeriodo,
      PagosEstimadosVsPagosReales:PagosEstimadosVsPagosReales,
      PagosPorProveedorPorPeriodoEncurso:PagosPorProveedorPorPeriodoEncurso,
      PagosPorProveedorPorPeriodoFinalizados:PagosPorProveedorPorPeriodoFinalizados,
      CargaTotalPorProveedorPorPeriodo:CargaTotalPorProveedorPorPeriodo,
      CargaTotalPorProveedorPorPeriodoTrimestreAnterior:CargaTotalPorProveedorPorPeriodoTrimestreAnterior,
      PagosPorPeriodoEncurso:PagosPorPeriodoEncurso,
      PagosPorPeriodoFinalizados:PagosPorPeriodoFinalizados

    };



  });
