'use strict';

/**
 * @ngdoc service
 * @name otecApp.contratoHeaderTable
 * @description
 * # contratoHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('contratoHeaderTable', function () {

    return [{ title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' }},
      { title: 'Fecha Inicio', field: 'fechaInicio', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }},
      { title: 'Fecha Fin', field: 'fechaTermino', visible: true,formatter:"date",class:"text-center", filter: { 'name': 'text' }},
      { title: 'Workflow', field: 'workflow',subfield:'nombre', lista:true, visible: true, filter: { 'name': 'text' }},
      { title: 'Tipo de Planta', field: 'tipoPlanta',subfield:'nombre', visible: true, filter: { 'name': 'text' }},
      { title: 'Proveedores', field: 'contratosProveedoreses', lista:{usarTableHeader:true,tableHeader:[{ title: 'Proveedores', field: 'proveedores',subfield:'nombre', visible: true, filter: { 'name': 'text' } },{ title: 'Código Acuerdo', field: 'codigoAcuerdo', visible: true,class:'text-center', filter: { 'name': 'text' } }],field:"contratosProveedoreses",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' }},
      { title: 'Estado', field: 'estado',formatter :"estado", class:"text-center", visible: true, filter: { 'name': 'text' } }];
  });
