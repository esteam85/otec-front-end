'use strict';

/**
 * @ngdoc service
 * @name otecApp.proveedorHeaderTable
 * @description
 * # proveedorHeaderTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('repositorioHeaderTable', function () {

    return [{ title: 'Servidor', field: 'servidor', visible: true},
      { title: 'Nombre', field: 'nombre',visible: true},
      { title: 'Usuario', field: 'usuario',visible: true},
      { title: 'Password', field: 'password',visible: true},
      { title: 'Puerto', field: 'puerto',visible: true},
      { title: 'Url', field: 'url',visible: true},
      { title: 'Descripcion', field: 'descripcion',visible: true}
     ];
  });
