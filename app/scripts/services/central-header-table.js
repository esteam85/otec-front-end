'use strict';

/**
 * @ngdoc service
 * @name otecApp.centralHeaderTable
 * @description
 * # centralHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('centralHeaderTable', function () {
    return [
      { title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Agencia', field: 'agencia',subfield:'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Descripción', field: 'descripcion', visible: true, filter: { 'name': 'text' } },
      { title: 'Estado', field: 'estado',formatter :"estado", class:"text-center", visible: true, filter: { 'name': 'text' } }
    ];

  });
