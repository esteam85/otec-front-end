'use strict';

/**
 * @ngdoc service
 * @name otecApp.contratoHeaderTable
 * @description
 * # contratoHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('usuarioHeaderTable', function (rolHeaderTable,contratoHeaderTable) {

    return [{ title: 'Nombre', field: 'nombres', visible: true, filter: { 'name': 'text' }},
      { title: 'Apellido', field: 'apellidos', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }},
      { title: 'Usuario', field: 'nombreUsuario', visible: true,formatter:"date",class:"text-center", filter: { 'name': 'text' }},
      { title: 'Rut', field: 'rut', field2:'dv', visible: true },
      { title: 'Email', field: 'email', visible: true, filter: { 'name': 'text' }},
      { title: 'Celular', field: 'celular', visible: true, filter: { 'name': 'text' }},
      { title: 'Roles', field: 'usuariosRoles', lista:{tableHeader:rolHeaderTable,field:"usuariosRoles",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' }},
      { title: 'Contratos', field: 'usuariosContratos', lista:{tableHeader:contratoHeaderTable,field:"usuariosContratos",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' }},
      { title: 'Estado', field: 'estado',formatter :"estado", class:"text-center", visible: true, filter: { 'name': 'text' } }];
  });
