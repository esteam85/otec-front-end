'use strict';

/**
 * @ngdoc service
 * @name otecApp.Session
 * @description
 * # Session
 * Service in the otecApp.
 */
angular.module('otecApp')
  .service('Session', function ($window) {

    this.recovery = function (){
      if($window.sessionStorage["sessionInfo"] != "undefined") {
        var session = JSON.parse($window.sessionStorage["sessionInfo"]);
        var tk =$window.sessionStorage["TK"];
        this.id = session.id;
        this.userId = session.userId;
        this.userRole = session.userRole;
        this.userLogType = session.userLogType;
        this.tk = tk;
      }
    };
    this.create = function (sessionId, userId, userRole,tk) {
      this.id = sessionId;
      this.userId = userId;
      this.userRole = userRole;
      this.userLogType = "logged";
      this.tk = tk;
      $window.sessionStorage["sessionInfo"] = JSON.stringify(this);
    };
    this.destroy = function () {
      $window.sessionStorage["sessionInfo"] = undefined;
      this.id = null;
      this.userId = null;
      this.userRole = null;
      this.tk = null;
      this.userLogType = "guest";

    };
  });
