'use strict';

/**
 * @ngdoc service
 * @name otecApp.tipoAlarmaHeaderTable
 * @description
 * # tipoAlarmaHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('tipoAlarmaHeaderTable',function (){

    return [
      { title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Identificador', field: 'identificador', visible: true, filter: { 'name': 'text' } },
      { title: 'Estado', field: 'estado', visible: true,formatter:"estado", filter: { 'name': 'text' },class:"text-center" }
    ]
  }
);
