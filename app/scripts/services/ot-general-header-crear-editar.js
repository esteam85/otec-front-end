'use strict';

/**
 * @ngdoc service
 * @name otecApp.proveedorHeaderTable
 * @description
 * # proveedorHeaderTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('otGeneralHeaderCrearEditarTable', function () {

    return {
      formulario:[
        {
          className: 'row',
          fieldGroup: [
            {
              key: 'nombre',
              type: 'horizontalInput',
              templateOptions: {
                label: 'Nombre',
                placeholder: 'Nombre',
                required: true
              }
            },
            {
              "key": "contrato",
              "type": "horizontalUiSelectGET",
              "templateOptions": {
                "placeholder": "Contrato",
                "label": "Contrato",
                "valueProp": "id",
                "labelProp": "nombre",
                "onChange":function(item,formly,scope){
                  if(item.id == 3 || item.id == 2){
                    scope.model.tipoOt = {id:1,nombre:"OT"};
                  }
                },
                "action":"listarContratosPorUsuario",
                "soloId":true,
                required: true
              }
            },
            {
              "key": "tipoOt",
              "type": "horizontalUiSelectGET",
              "templateOptions": {
                "placeholder": "Tipo de Orden de Trabajo",
                "label": "Tipo",
                "valueProp": "id",
                "labelProp": "nombre",
                "broadcastOnChange":["cambiar:modal",function(model,rootScope,scope){
                  var modal = {};
                  if(model.tipoOt.id==1){
                    modal = {tituloModal: "Crear Órden de Trabajo"};
                  }
                  if(model.tipoOt.id==2){
                    modal = {tituloModal: "Crear Activación de Proyecto"};
                  }

                  return modal;
                }],
                "metodo":"TipoOt",
                required: true
              },
              hideExpression:"model.contrato.id == 2 || model.contrato.id == 3"
            },
            {
              "key": "tipoOtText",
              "type": "horizontalText",
              "templateOptions": {
                "label": "Tipo",
                "valueProp": "id",
                "attrCascade":"nombre",
                "targetKey":"tipoOt",
                required: true
              },
              hideExpression:"model.contrato.id !== 3 && model.contrato.id !== 2"
            },
            {
              "key": "cubicador",
              "type": "horizontalUiSelectGetCascade",
              "templateOptions": {
                "placeholder": "Cubicación",
                "toWatch":'contrato',
                "label": "Cubicación",
                "get":true,
                "valueProp": "id",
                "labelProp": "nombre",
                "parametros":function(model){
                  return {IdContrato:model.contrato.id};
                },
                "action":"listarCubicacionesSinOtPorUsuarioId",
                required: true
              },
              hideExpression:"model.contrato.id == undefined",
              expressionProperties: {
                'templateOptions.label': function($viewValue, $modelValue, scope) {
                  if(scope.model.contrato.id === 1 || scope.model.contrato.id === 3)
                    return "Costeo";

                  return "Cubicación"
                },
                'templateOptions.placeholder': function($viewValue, $modelValue, scope) {
                  if(scope.model.contrato.id === 1 || scope.model.contrato.id === 3)
                    return "Costeo";

                  return "Cubicación"
                }
              }
            },
            {
              "key": "proveedor",
              "type": "horizontalText",
              "templateOptions": {
                "placeholder": "Proveedor",
                "label": "Proveedor",
                "valueProp": "id",
                "attrCascade":"proveedor",
                "subAttrCascade":"nombre",
                "targetKey":"cubicador",
                required: true
              },
              hideExpression:"model.contrato.id == undefined"
            },
            {
              "key": "numeroAp",
              "type": "horizontalUiSelectGET",
              "templateOptions": {
                "placeholder": "Número Ap",
                "label": "Ap",
                "valueProp": "id",
                "labelProp": "nombre",
                "parametros":function(model){
                  return {IdContrato:model.contrato.id};
                },
                "action":"obtenerListadoOtSbe"
              },
              hideExpression:"(model.tipoOt.id !== 1) || (model.contrato.id !== 1)"
            },
            {
              "key": "planDeProyecto",
              "type": "horizontalUiSelectGET",
              "templateOptions": {
                "placeholder": "Plan de Proyecto",
                "label": "Plan de Proyecto",
                "valueProp": "id",
                "labelProp": "nombre",
                "metodo":"PlanDeProyecto",
                required: true
              },
              hideExpression:"(model.contrato.id !== 1 || model.numeroAp.id !== undefined) && (model.contrato.id !== 3 || model.tipoOt.id !== 1)"
            },
            {
              "key": "planDeProyecto",
              "type": "horizontalText",
              "templateOptions": {
                "placeholder": "Plan de Proyecto",
                "label": "Plan de Proyecto",
                "attrCascade":"planDeProyecto",
                "subAttrCascade":"nombre",
                "targetKey":"numeroAp",
                required: true
              },
              hideExpression:"(model.contrato.id !== 1) || model.numeroAp.id === undefined"
            },
            {
              "key": "sitio",
              "type": "horizontalUiSelectGetCascade",
              "templateOptions": {
                "action":"listarSitiosPorPlanDeProyecto",
                "parametros":function(model){
                  return{planDeProyectoId:model.planDeProyecto.id};
                },
                "toWatch":'planDeProyecto',
                "concatAttrPrev":"codigo",
                "get":true,
                "placeholder": "Sitio",
                "label": "Sitio",
                "valueProp": "id",
                "labelProp": "nombre",
                required: true
              },
              hideExpression:"(model.contrato.id !== 1 || model.numeroAp.id !== undefined) && (model.contrato.id !== 3 || model.tipoOt.id !== 1)"
            },
            {
              "key": "sitio",
              "type": "horizontalText",
              "templateOptions": {
                "placeholder": "Sitio",
                "label": "Sitio",
                "valueProp": "id",
                "attrCascade":"sitio",
                "subAttrCascade":"nombre",
                "targetKey":"numeroAp"
              },
              hideExpression:"(model.contrato.id !== 1) || model.numeroAp.id === undefined"
            },
            //{
            //  "key": "central",
            //  "type": "horizontalUiSelectGET",
            //  "templateOptions": {
            //    "placeholder": "Central",
            //    "label": "Central",
            //    "valueProp": "id",
            //    "labelProp": "nombre",
            //    "metodo":"Central",
            //    required: true
            //  },
            //  hideExpression:"model.contrato.id == 1 || model.contrato.id == 2"
            //},
            //{
            //  "key": "tipoSolicitud",
            //  "type": "horizontalUiSelectGET",
            //  "templateOptions": {
            //    "placeholder": "Solicitud",
            //    "label": "Solicitud",
            //    "valueProp": "id",
            //    "labelProp": "nombre",
            //    "metodo":"TipoSolicitud",
            //    required: true
            //  },
            //  hideExpression:"model.contrato.id == 1"
            //},
            //{
            //  "key": "tipoEstadoOt",
            //  "type": "horizontalUiSelectGET",
            //  "templateOptions": {
            //    "placeholder": "Estado de OT",
            //    "label": "Estado OT",
            //    "valueProp": "id",
            //    "labelProp": "nombre",
            //    "metodo":"TipoEstadoOt",
            //    required: true
            //  },
            //  hideExpression:"model.tipoOt.id == 2"
            //},
            {
              "key": "subGerente",
              "type": "horizontalUiSelectGET",
              "templateOptions": {
                "placeholder": "Administrador de Contrato",
                "label": "Adm. Contrato",
                "concatAttr":"apellidos",
                "action":"listarRolesPorPerfilAction",
                "parametros":function(){return {perfilId:2}},
                "dataHandler":function(data){
                  var handledData = _.reject(data, function(item){ return item.id !== 5; })[0];
                  return handledData.usuariosRoles;
                },
                "valueProp": "id",
                "labelProp": "nombres",
                "get":false,
                required: true
              }
              //hideExpression:"model.contrato.id == 1"
            },
            {
              template: '<hr />'
            },
            {
              "key": "fechaInicioEstimada",
              "type": "horizontalDatepicker",
              "templateOptions": {
                "fecha":true,
                "minDate":"",
                "label": "Fecha Inicio",
                "type": "text",
                "formatOnChangeKey":"contrato",
                "datepickerPopup": "dd-MM-yyyy",
                required: true
              },
              hideExpression:"model.contrato.id !== 2"
            },
            {
              "key": "fechaInicioEstimada",
              "type": "horizontalDatepicker",
              "templateOptions": {
                "fecha":true,
                "label": "Fecha Inicio",
                "formatOnChangeKey":"contrato",
                "type": "text",
                "datepickerPopup": "dd-MM-yyyy",
                required: true
              },
              hideExpression:"model.contrato.id === 2"
            },
            {
              "key": "fechaTerminoEstimada",
              "type": "horizontalDatepicker",
              "templateOptions": {
                "fecha":true,
                "label": "Fecha Término",
                "formatOnChangeKey":"contrato",
                "type": "text",
                "targetMinDateKey":"fechaInicioEstimada",
                "datepickerPopup": "dd-MM-yyyy",
                required: true
              },
              hideExpression:"model.contrato.id !== 2"
            },
            {
              "key": "fechaTerminoEstimada",
              "type": "horizontalDatepicker",
              "templateOptions": {
                "fecha":true,
                "targetMinDateKey":"fechaInicioEstimada",
                "label": "Fecha Término",
                "formatOnChangeKey":"contrato",
                "type": "text",
                "datepickerPopup": "dd-MM-yyyy",
                required: true
              },
              hideExpression:"model.contrato.id === 2"
            },
            {
              template: '<hr />'
            },
            {
              key: 'observaciones',
              type: 'horizontalTextarea',
              templateOptions: {
                label: 'Observaciones',
                placeholder: 'Observaciones...',
                maxlength:1000,
                required: true
              }
            }
          ]
        }
      ],
      backdrop:'static'
    };
  });
