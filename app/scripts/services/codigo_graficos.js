'use strict';

/**
 * @ngdoc service
 * @name otecApp.CODIGOGRAFICOS
 * @description
 * # CODIGOGRAFICOS
 * Constant in the otecApp.
 */
angular.module('otecApp')
  .constant('CODIGOGRAFICOS', {
    LINE_ATRASADAS_INICIO_ESTIMADO_VS_REAL: 1
  });
