'use strict';

/**
 * @ngdoc service
 * @name otecApp.contratoHeaderTable
 * @description
 * # contratoHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('cubicadorHeaderTable', function () {

    return [{ title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' }},
      { title: 'Descripcion', field: 'descripcion', visible: true, filter: { 'name': 'text' }},
      { title: 'Fecha Creacion', field: 'fechaCreacion', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }},
      { title: 'Region', field: 'region',subfield:'nombre', lista:true,class:"text-center" , visible: true, filter: { 'name': 'text' }},
      { title: 'Contrato', field: 'contrato',subfield:'nombre',class:"text-center" , visible: true, filter: { 'name': 'text' }}
      //{ title: 'Servicios', field: 'cubicadorServicios', lista:{usarTableHeader:true,tableHeader:[{ title: 'Servicios', field: 'servicio',subfield:'nombre', visible: true, filter: { 'name': 'text' } },{ title: 'Cantidad', field: 'cantidad', visible: true,class:'text-center', filter: { 'name': 'text' } }],field:"cubicadorServicios",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' }},
      //{ title: 'Materiales', field: 'cubicadorMateriales', lista:{usarTableHeader:true,tableHeader:[{ title: 'Materiales', field: 'material',subfield:'nombre', visible: true, filter: { 'name': 'text' } },{ title: 'Cantidad', field: 'cantidad', visible: true,class:'text-center', filter: { 'name': 'text' } }],field:"cubicadorMateriales",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' }}
      ];
  });
