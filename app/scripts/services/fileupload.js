'use strict';

/**
 * @ngdoc service
 * @name otecApp.fileUpload
 * @description
 * # fileUpload
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .service('fileUpload', ['$http','$q','Restangular', function ($http,$q,Restangular) {

    this.uploadFileToUrl = function(file, uploadUrl,nombreImagen){
      var deferred = $q.defer();
      var fd = new FormData();
      fd.append('image', file);
      fd.append('nombre', nombreImagen + ".jpg");

      Restangular.all(uploadUrl).withHttpConfig({transformRequest: angular.identity})
        .customPOST(fd, undefined, undefined,
        { 'Content-Type': undefined })
        .then(function(data){
          deferred.resolve(data.data);
        },function(error){
          deferred.reject(error);
        });
      return deferred.promise;
    };

    this.uploadMultipleFileToUrl = function(files, uploadUrl,parametros){
      var deferred = $q.defer();
      var fd = new FormData();
      _.each(files,function(item,index){

        fd.append('archivo', item);
      });

      fd.append('observaciones',parametros.observaciones);
      fd.append('usuarioId',parametros.usuarioId);
      fd.append('objCoreOT',parametros.objCoreOT);
      fd.append('otId',parametros.otId);
      //fd.append('roles',);

      Restangular.all(uploadUrl).withHttpConfig({transformRequest: angular.identity})
        .customPOST(fd, undefined, {roles:parametros.roles},
        { 'Content-Type': undefined })
        .then(function(data){
          deferred.resolve(data.data);
        },
        function(error){
          deferred.reject(error);
        });

      return deferred.promise;
    }
  }]);

