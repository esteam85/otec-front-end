'use strict';

/**
 * @ngdoc service
 * @name otecApp.tipoAlarmaHeaderTable
 * @description
 * # tipoAlarmaHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('alarmaHeaderTable',function (){

    return [
      { title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Días', field: 'tiempo', class:"text-center", visible: true, filter: { 'name': 'text' } },
      { title: 'Tipo', field: 'tipoAlarma',subfield:'nombre', visible: true, filter: { 'name': 'text' }}

    ]
  }
);
