'use strict';

/**
 * @ngdoc service
 * @name otecApp.contrato
 * @description
 * # contrato
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('contratoService',function ($q,$log,CRUD) {


    //crearEditarWorkflow
    function crearEditarContrato(workflow){

      var deferred = $q.defer();

      if(workflow.accion == "crear"){
        CRUD.crear("Contrato",workflow).then(function(data){
          deferred.resolve(data);
        },function(error){
          deferred.reject(error);
          $log.info("ocurrio un error al crear contrato",error);

        });
      }else{
        //implementar editar
      }

      return deferred.promise;
    };

    //listarWorkflow
    function listarContrato(){

      var deferred = $q.defer();

      CRUD.listar("listarContratos").then(function(data){
        deferred.resolve(JSON.parse(data.listadoContratos));
      },function(error){
        $log.info("ocurrio un error al listar contrato",error);
        deferred.reject(error);

      });
      return deferred.promise;
    };


    return{
      crearEditarContrato:crearEditarContrato,
      listarContrato:listarContrato

    };
  });
