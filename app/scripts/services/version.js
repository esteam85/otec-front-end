'use strict';

/**
 * @ngdoc service
 * @name otecApp.VERSION
 * @description
 * # VERSION
 * Constant in the otecApp.
 */
angular.module('otecApp')
  .constant('VERSION', "v.1.4.1");


// *v.1.4.1
//  - Se agrega Título por ruta
//  - Desde ahora se puede crear ot sin ap en sbe.
//  - Se agrega tipo moneda en cubicación para sbe y ran.
// *v.1.4.0
//  - Se agregan Usuarios validadores en detalle OT.
//  - Se agrega contrato RAN
//  - Se agrega Contrato Unificado
//*v.1.3.9
//  - Se agrega Bower de slider, se agrega formly horizontalSlider y AC-TI-PA se le agrega horizontalSlider para porcentaje de pago.
//  - Se arregla pagos en ordinario, parcial
//*v.1.3.8
//  - Se cambia validación de sistema Chekbox a Texto.
//  - Arreglo de datos en detalle ot
//  - Se crea carpeta resources con fechas en formato dd-mm-yyyy para producción
