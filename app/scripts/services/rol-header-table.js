'use strict';

/**
 * @ngdoc service
 * @name otecApp.contratoHeaderTable
 * @description
 * # contratoHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('rolHeaderTable', function (privilegioHeaderTable) {

    return [{ title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' }},
      { title: 'Perfil', field: 'perfil',subfield:'nombre', lista:true, visible: true, filter: { 'name': 'text' }},
      { title: 'Opciones de Menú', field: 'rolesOpciones', lista:{usarTableHeader:true,tableHeader:[ { title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' } },{ title: 'Código', field: 'codigoMenu', class:'text-center' ,visible: true, filter: { 'name': 'text' } }],field:"rolesOpciones",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' }},
      { title: 'Privilegios', field: 'rolesPrivilegios', lista:{tableHeader:privilegioHeaderTable,field:"rolesPrivilegios",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' }},
      { title: 'Estado', field: 'estado',formatter :"estado", class:"text-center", visible: true, filter: { 'name': 'text' } }];
  });
