'use strict';

/**
 * @ngdoc service
 * @name otecApp.agenciaHeaderTable
 * @description
 * # agenciaHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('detalleParametroHeaderTable', function (nombreHeaderTable) {
    return [{ title: 'Llave', field: 'llave', visible: true, filter: { 'name': 'text' }},
      { title: 'Valor', field: 'valor',visible: true, filter: { 'name': 'text' }},
      { title: 'Parametro', field: 'parametros',subfield:'nombre', visible: true, filter: { 'name': 'text' }},
      { title: 'Template Option', field: 'istemplateoption',visible: true, filter: { 'name': 'text' }}];
  });
