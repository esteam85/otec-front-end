'use strict';

/**
 * @ngdoc service
 * @name otecApp.tipoDefaultForm
 * @description
 * # tipoDefaultForm
 * Value in the otecApp.
 */
angular.module('otecApp')
  .value('tipoDefaultForm', [
    {
      key: 'nombre',
      type: 'horizontalInput',
      templateOptions: {
        label: 'Nombre',
        placeholder: 'Nombre',
        required: true
      }
    },
    {
      key: 'descripcion',
      type: 'horizontalTextarea',
      templateOptions: {
        label: 'Descripción',
        placeholder: 'Descripción...',
        required: true
      }
    },{
      "key": "estado",
      "type": "horizontalRadios",
      "templateOptions": {
        "label": "Estado",
        "options": [
          {
            "name": "Activo",
            "value": "A"
          },
          {
            "name": "Inactivo",
            "value": "I"
          }
        ]}
    }
  ]);
