'use strict';

/**
 * @ngdoc service
 * @name otecApp.userRoles
 * @description
 * # userRoles
 * Constant in the otecApp.
 */
angular.module('otecApp')
  .constant('USER_ROLES', {
    logged: 'logged',
    guest: 'guest'
  });
