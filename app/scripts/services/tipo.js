'use strict';

/**
 * @ngdoc service
 * @name otecApp.workflow
 * @description
 * # workflow
 * Service in the otecApp.
 */
angular.module('otecApp')
  .factory('tipoService', function ($q,$log,CRUD) {

    function listar(metodoListar, action,parametros){

      var deferred = $q.defer();
      CRUD.listar(metodoListar, action,parametros).then(function(data){
        var dataParsed = JSON.parse(data.retorno);
        deferred.resolve(dataParsed);
      },function(error){
        $log.info("ocurrio un error al listar",error);
        deferred.reject(error);
      });
      return deferred.promise;

    }


    function listarPorId(action,objectId,objValue){

      var deferred = $q.defer();
      CRUD.listarPorId(action,objectId,objValue).then(function(data){
        var dataParsed = JSON.parse(data.retorno);
        dataParsed = _.reject(dataParsed, function(item){ return item.estado == 'E'; });
        deferred.resolve(dataParsed);
        //$log.info("listar",dataParsed)
      },function(error){
        $log.info("ocurrio un error al listar",error);
        deferred.reject(error);
      });
      return deferred.promise;

    }
 function listarPorIdChris(action,objectId,objValue){

      var deferred = $q.defer();
      CRUD.listarPorId(action,objectId,objValue).then(function(data){
        var dataParsed = data;
        deferred.resolve(dataParsed);
        $log.info("listar",dataParsed)
      },function(error){
        $log.info("ocurrio un error al listar",error);
        deferred.reject(error);
      });
      return deferred.promise;

    }

    //crearEditarWorkflow
    function crearEditar(objeto,metodoCrear,metodoEditar,leyenda,action){

      var deferred = $q.defer();

      if(objeto.accion == "crear"){
        CRUD.crear(metodoCrear,objeto,action).then(function(data){
          var pre = objeto.nombre || leyenda;
          data.success = pre + " creado con exito.";
          deferred.resolve(data);
        },function(error){
          error.danger ="Error al crear "+objeto.nombre+ ", intente mas tarde.";
          deferred.reject(error);
          $log.info("ocurrio un error al crear "+ objeto.nombre,error);

        });
      }else{
        CRUD.actualizar(metodoEditar,objeto,action).then(function(data){
          data.success = objeto.nombre+" actualizado con exito.";
          deferred.resolve(data);
        },function(error){
          error.danger ="Error al actualizar "+objeto.nombre+ ", intente mas tarde.";
          deferred.reject(error);
          $log.info("ocurrio un error al editar "+ objeto.nombre,error);

        });
      }

      return deferred.promise;
    };

    function crearEditarChris(accion,action,attrName,AttrValue){

      var deferred = $q.defer();

      if(accion == "crear"){
        CRUD.crearChris(action,attrName,AttrValue).then(function(data){
          //var pre = objeto.nombre || leyenda;
          //data.success = pre + " creado con exito.";
          deferred.resolve(data);
        },function(error){
          //error.danger ="Error al crear "+objeto.nombre+ ", intente mas tarde.";
          deferred.reject(error);
          //$log.info("ocurrio un error al crear "+ objeto.nombre,error);

        });
      }else{
        CRUD.actualizar(metodoEditar,objeto,action).then(function(data){
          //data.success = objeto.nombre+" actualizado con exito.";
          deferred.resolve(data);
        },function(error){
          //error.danger ="Error al actualizar "+objeto.nombre+ ", intente mas tarde.";
          deferred.reject(error);
          //$log.info("ocurrio un error al editar "+ objeto.nombre,error);

        });
      }

      return deferred.promise;
    };

    function eliminar(objeto,metodoEditar){
      var deferred = $q.defer();
      objeto.estado = 'E';
      CRUD.actualizar(metodoEditar,objeto).then(function(data){
        data.success = objeto.nombre+" elminado con exito.";
        deferred.resolve(data);
      },function(error){
        error.danger ="Error al eliminar "+objeto.nombre+ ", intente mas tarde.";
        deferred.reject(error);
        $log.info("ocurrio un error al eliminar "+ objeto.nombre,error);

      });
      return deferred.promise;
    };

    return{
      listar:listar,
      crearEditar:crearEditar,
      eliminar:eliminar,
      listarPorId:listarPorId,
      listarPorIdChris:listarPorIdChris,
      crearEditarChris:crearEditarChris

    }
  });
