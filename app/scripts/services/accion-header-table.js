'use strict';

/**
 * @ngdoc service
 * @name otecApp.proveedorHeaderTable
 * @description
 * # proveedorHeaderTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('accionHeaderTable', function (parametroHeaderTable) {

    return [{ title: 'Nombre', field: 'nombre', visible: true},
      { title: 'Tipo', field: 'tipoAccion', subfield: 'nombre', visible: true},
      { title: 'Parametros', field: 'accionesParametroses', lista:{tableHeader:parametroHeaderTable,field:"accionesParametroses",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' } }];
  });
