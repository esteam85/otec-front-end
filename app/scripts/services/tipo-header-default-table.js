'use strict';

/**
 * @ngdoc service
 * @name otecApp.tipoDefaultTable
 * @description
 * # tipoDefaultTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('tableTipoHeaderDefault',function (){

    return [
      { title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Descripción', field: 'descripcion', visible: true, filter: { 'name': 'text' } },
      { title: 'Estado', field: 'estado', visible: true,formatter:"estado", filter: { 'name': 'text' },class:"text-center" }
    ]
  }
);
