'use strict';

/**
 * @ngdoc service
 * @name otecApp.proyectoHeaderTable
 * @description
 * # proyectoHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('proyectoHeaderTable', function () {
    return [
      { title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Descripción', field: 'descripcion', visible: true, filter: { 'name': 'text' } },
      { title: 'Contrato', field: 'contrato',subfield:'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Pep1', field: 'pep1', visible: true, filter: { 'name': 'text' } },
      { title: 'Responsable', field: 'responsable', visible: true, filter: { 'name': 'text' } },
      { title: 'Fecha Creación', field: 'fechaCreacion', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }},
      { title: 'Estado', field: 'estado',formatter :"estado", class:"text-center", visible: true, filter: { 'name': 'text' } }
    ];
  });
