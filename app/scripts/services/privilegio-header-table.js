'use strict';

/**
 * @ngdoc service
 * @name otecApp.agenciaHeaderTable
 * @description
 * # agenciaHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('privilegioHeaderTable', function (nombreHeaderTable) {
    return [{ title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' }},
      { title: 'Descripcion', field: 'descripcion',visible: true, filter: { 'name': 'text' }},
      { title: 'Evento', field: 'evento',subfield:'nombre', visible: true, filter: { 'name': 'text' } }];
  });
