'use strict';

/**
 * @ngdoc service
 * @name otecApp.agenciaHeaderTable
 * @description
 * # agenciaHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('workflowHeaderTable', function () {
    return [{ title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' }},
      { title: 'Descripcion', field: 'descripcion',visible: true, filter: { 'name': 'text' }},
      { title: 'Fecha Creación', field: 'fechaCreacion', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }},
      { title: 'Estado', field: 'estado', visible: true,formatter:"estado", filter: { 'name': 'text' },class:"text-center" }
      ];
  });
