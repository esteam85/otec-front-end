'use strict';

/**
 * @ngdoc service
 * @name otecApp.highchartExamples
 * @description
 * # highchartExamples
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('highchartExamples', function () {

    return [{
      options: {
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 45,
          beta: 0,
          depth: 70
        }
      }},
      title: {
        text: 'Distribución de OTes por Empresa Colaboradora'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          depth: 35,
          dataLabels: {
            enabled: true,
            format: '{point.name}'
          }
        }
      },
      series: [{
        type: 'pie',
        name: 'Browser share',
        data: [
          ['Coasin', 45.0],
          ['Huawei', 26.8],
          {
            name: 'Nokia',
            y: 12.8,
            sliced: true,
            selected: true
          },
          ['AJ Ingenieros', 8.5],
          ['ZTE',6.9],
        ]
      }]
    },{
      options: {
        chart: {
          type: 'column',
          margin: 75,
          options3d: {
            enabled: true,
            alpha: 10,
            beta: 25
          }
        }
      },
      title: {
        text: 'Ordenes de Trabajo Terminadas'
      },
      subtitle: {
        text: 'Ordenes de Trabajo iniciadas por la EECC que han finalizado'
      },
      plotOptions: {
        column: {
          depth: 25
        }
      },
      xAxis: {
        categories: Highcharts.getOptions().lang.shortMonths
      },
      yAxis: {
        title: {
          text: null
        }
      },
      series: [{
        name: 'Ordenes de Trabajo',
        data: [20, 39, null, 42, 40, 75, 21, 34, 46, 13]
      }]
    },{
      chart: {
        type: 'line'
      },
      title: {
        text: 'Comparación Tiempo Real vs Tiempo Esperado'
      },
      subtitle: {
        text: 'Comparación en días cantidad de Órdenes '
      },
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        title: {
          text: 'Días'
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [{
        name: 'Real',
        data: [7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9]
      }, {
        name: 'Estimado',
        data: [3, 4, 5, 8, 11, 15, 17, 16, 14, 10, 6, 4]
      }]
    }];
  });
