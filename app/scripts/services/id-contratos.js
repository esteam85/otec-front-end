'use strict';

/**
 * @ngdoc service
 * @name otecApp.idContratos
 * @description
 * # idContratos
 * Constant in the otecApp.
 */
angular.module('otecApp')
  .constant('ID_CONTRATOS', {
    CONTRATO_ORDINARIO:2,
    CONTRATO_SBE:1
  });
