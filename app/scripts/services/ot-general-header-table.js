'use strict';

/**
 * @ngdoc service
 * @name otecApp.proveedorHeaderTable
 * @description
 * # proveedorHeaderTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('otGeneralHeaderTable', function () {

    return [
      { title: 'Número', field: 'id', class:"text-center",visible: true},
      { title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Fecha Creacion', field: 'fechaCreacion', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }},
      { title: 'Fecha Inicio', field: 'fechaInicioEstimada', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }},
      { title: 'Fecha Termino', field: 'fechaTerminoEstimada', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }},
      { title: 'Contrato', field: 'contrato', subfield: 'nombre',class:"text-center", visible: true},
      { title: 'Proveedor', field: 'proveedor', subfield: 'nombre',class:"text-center", visible: true},
      { title: 'Gestor', field: 'gestor', subfield: 'nombres',subfieldplus:"apellidos",class:"text-center", visible: true},
      //{ title: 'Tipo Estado', field: 'tipoEstadoOt', subfield: 'nombre',class:"text-center", visible: true},
      { title: 'Tipo Ot', field: 'tipoOt', subfield: 'nombre', visible: true,class:"text-center"},
      //{ title: 'Acción a Ejecturar', field: 'current', visible: true,class:"text-center"},
      { title: 'Flujo',sorteableDisabled:true, field: 'participacion',
        lista:{
          request:{action:"obtenerAccionesOt",parametros:function(objeto,column,scope){ return {otId:objeto.id}}},
          usarTableHeader:true,tableHeader:[{ title: 'Acciones', field: 'nombre', visible: true, filter: { 'name': 'text' } },{ title: 'Por Ejecutar', field: 'current', visible: true,icon: true, filter: { 'name': 'text' },class:"text-center" }]
          ,field:"participacion"}
        , icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' }}
    ];
  });
