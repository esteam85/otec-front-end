'use strict';

/**
 * @ngdoc service
 * @name otecApp.proveedorHeaderTable
 * @description
 * # proveedorHeaderTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('regionHeaderTable', function () {

    return [{ title: 'Codigo', field: 'codigo', visible: true},
      { title: 'Nombre', field: 'nombre',visible: true},
      { title: 'Factor', field: 'factor',visible: true}
     ];
  });
