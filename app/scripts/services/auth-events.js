'use strict';

/**
 * @ngdoc service
 * @name otecApp.authEvents
 * @description
 * # authEvents
 * Constant in the otecApp.
 */
angular.module('otecApp')
  .constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
  });
