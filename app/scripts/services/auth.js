'use strict';

/**
 * @ngdoc service
 * @name otecApp.Auth
 * @description
 * # Auth
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('AuthService', function (Restangular,$log,$cookieStore,Session) {
    var authService = {};

    function findRole(rol){
      return !!_.findWhere(USER_ROLES,rol);
    };

    authService.cambiarPass = function (credentials) {
      return Restangular
        .one('setearClaveUsuario').get(credentials)
        .then(function (data) {
          var res = JSON.parse(data.data.estadoOK);
          return res;
        });
    };

    authService.login = function (credentials) {
      Restangular.setDefaultRequestParams({ tk:Session.tk});
      return Restangular
        .one('login').get(credentials)
        .then(function (data) {
          //$log.info(data.headers("Set-Cookie"));
          data.data.datosUsuario = JSON.parse(data.data.datosUsuario);
          Restangular.setDefaultRequestParams({ tk:data.data.tk });
          return data.data;
        });
    };

    authService.logOut = function () {
      return Restangular
        .one('cerrarSesion').get({usuarioId:Session.userId})
        .then(function (data) {
          return data.data;
        });
    };

    authService.logRequired = function(authorizedRoles){
      if(_.find(authorizedRoles) == 'guest'){
        return false;
      }
      return true
    }

    authService.isAuthenticated = function () {
      Session.recovery();
      Restangular.setDefaultRequestParams({ tk:Session.tk});
      return !!Session.userId;
    };

    authService.isAuthorized = function (authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }
      var aux1 = authService.isAuthenticated();
      var aux2 = authorizedRoles.indexOf(Session.userLogType);
      return (aux1 &&
      aux2 !== -1);
    };

    return authService;
  });
