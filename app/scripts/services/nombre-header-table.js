'use strict';

/**
 * @ngdoc service
 * @name otecApp.nombreHeaderTable
 * @description
 * # nombreHeaderTable
 * Factory in the otecApp.
 */
angular.module('otecApp')
  .factory('nombreHeaderTable', function () {
    return [
      { title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' } }
    ];

  });
