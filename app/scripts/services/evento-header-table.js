angular.module('otecApp')
  .factory('eventoHeaderTable', function (accionHeaderTable) {

    return [{ title: 'Nombre', field: 'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Tipo', field: 'tipoEvento',subfield:'nombre', visible: true, filter: { 'name': 'text' } },
      { title: 'Mensaje', field: 'mensajeEvento',subfield:'mensajeNotificacion', visible: true, filter: { 'name': 'text' } },
      { title: 'Acciones', field: 'eventosAccioneses', lista:{tableHeader:accionHeaderTable,field:"eventosAccioneses",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' } },
      { title: 'Alarmas', field: 'eventosAlarmas', lista:{usarTableHeader:true,tableHeader:[{ title: 'Nombre', field: 'alarmas',subfield:'nombre', visible: true, filter: { 'name': 'text' } },{ title: 'Estado', field: 'estadoAlarma',formatter:"estado",class:"text-center", visible: true, filter: { 'name': 'text' } }],field:"eventosAlarmas",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' }},
      { title: 'Duración en días', field: 'duracion', visible: true, filter: { 'name': 'text' },class:"text-center" }];
  });
