'use strict';

/**
 * @ngdoc service
 * @name otecApp.proveedorHeaderTable
 * @description
 * # proveedorHeaderTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('servicioHeaderTable', function (parametroHeaderTable) {

    return [{ title: 'Nombre', field: 'nombre', visible: true},
      { title: 'Descripcion', field: 'descripcion', visible: true},
      { title: 'Codigo', field: 'codigo', visible: true},
      { title: 'Precio', field: 'precio', visible: true},
      { title: 'Tipo Servicio', field: 'tipoServicio', subfield: 'nombre', visible: true},
      { title: 'Tipo Unidad Medida', field: 'tipoUnidadMedida', subfield: 'nombre', visible: true},
      { title: 'Materiales', field: 'serviciosMaterialeses', lista:{usarTableHeader:true,tableHeader:[{ title: 'Material', field: 'materiales',subfield:'nombre', visible: true, filter: { 'name': 'text' } },{ title: 'Cantidad', field: 'cantidad', visible: true,class:'text-center', filter: { 'name': 'text' } }],field:"serviciosMaterialeses",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' }},
      { title: 'Estado', field: 'estado', visible: true,formatter:"estado", filter: { 'name': 'text' },class:"text-center" }];
  });
