'use strict';

/**
 * @ngdoc service
 * @name otecApp.proveedorHeaderTable
 * @description
 * # proveedorHeaderTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('pendienteHeaderTable', function () {

    return [{ title: 'Número', field: 'id', class:"text-center",visible: true},
      { title: 'Contrato', field: 'contrato', subfield: 'nombre', visible: true},
      { title: 'Proveedor', field: 'proveedor', subfield: 'nombre',class:"text-center", visible: true},
      { title: 'Gestor', field: 'gestor', subfield: 'nombres',subfieldplus:"apellidos",class:"text-center", visible: true},
      { title: 'Tipo Estado', field: 'tipoEstadoOt', subfield: 'nombre',class:"text-center", visible: true},
      { title: 'Tipo Ot', field: 'tipoOt', subfield: 'nombre', visible: true,class:"text-center"},
      //{ title: 'Fecha Adjudicación', field: 'fechaAdjudicacion', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }},
      //{ title: 'Fecha Creación', field: 'fechaCreacion', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }},
      //{ title: 'Fecha Inicio', field: 'fechaInicioReal', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }},
      //{ title: 'Fecha Termino', field: 'fechaTerminoReal', formatter:"date",class:"text-center" , visible: true, filter: { 'name': 'text' }}
    ];
  });
