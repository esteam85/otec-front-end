'use strict';

/**
 * @ngdoc service
 * @name otecApp.proveedorHeaderTable
 * @description
 * # proveedorHeaderTable
 * Value in the otecApp.
 */
angular.module('otecApp')
  .factory('decisionHeaderTable', function (parametroHeaderTable) {

    return [{ title: 'Respuesta', field: 'respuesta', visible: true},
      { title: 'Decisiones', field: 'decisiones', lista:{tableHeader:parametroHeaderTable,field:"decisiones",size:"lg"}, icon:'fa fa-list',class:"text-center", visible: true, filter: { 'name': 'text' } }];
  });
