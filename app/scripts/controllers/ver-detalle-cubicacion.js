'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:VerDetalleCubicacionCtrl
 * @description
 * # VerDetalleCubicacionCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('VerDetalleCubicacionCtrl', function ($scope,$log,$q,$filter,$modalInstance,ID_EVENTOS,objeto){

    var headerTableItems = [
      {
        title: 'Item',
        type:"formlyAdd",
        valorFormField:{
          "key": "item",
          "type": "input",
          "templateOptions": {
            "placeholder": "Item",
            "required": true
          },
          expressionProperties: {
            'templateOptions.disabled': 'model.disabled'}
        },
        field: 'item',
        visible: true
      },
      {
        title: 'Unidad',
        type:"formlyAdd",
        valorFormField:{
          "key": "tipoUnidadMedida",
          "type": "horizontalUiSelectGETNoLabel",
          "templateOptions": {
            "valueProp": "id",
            "labelProp": "nombre",
            "placeholder": "Unidad",
            "metodo":"TipoUnidadMedida",
            "required": true
          },
          expressionProperties: {
            'templateOptions.disabled': 'model.disabled'}
        },
        field: 'precio',
        visible: true
      },
      {
        title: 'Cantidad',
        type:"formlyAdd",
        valorFormField:{
          "key": "cantidad",
          "type": "input",
          "templateOptions": {
            "min":0,
            "type":"number",
            "placeholder": "Cantidad",
            "required": true
          },
          expressionProperties: {
            'templateOptions.disabled': 'model.disabled'}
        },
        field: 'cantidad',
        visible: true
      },
      {
        title: 'Precio',
        type:"formlyAdd",
        valorFormField:{
          "key": "precio",
          "type": "input",
          "templateOptions": {
            "type":"number",
            "placeholder": "Precio",
            "required": true
          },
          expressionProperties: {
            'templateOptions.disabled': 'model.disabled'}
        },
        field: 'precio',
        visible: true
      },
      {
        title: 'Moneda',
        type:"formlyAdd",
        valorFormField:{
          "key": "tipoMoneda",
          "type": "horizontalUiSelectGETNoLabel",
          "templateOptions": {
            "valueProp": "id",
            "labelProp": "nombre",
            "placeholder": "Moneda",
            "metodo":"TipoMoneda",
            "required": true
          },
          expressionProperties: {
            'templateOptions.disabled': 'model.disabled'}
        },
        field: 'precio',
        visible: true
      },
      {
        title: 'Quitar',
        type:"remover",
        class:"text-center icon-options",
        visible: true
      }
    ];
    var form = [
      {
        className: 'row',
        fieldGroup:[
          {
            className: 'col-lg-6',
            fieldGroup:[
              {
                className: 'row',
                fieldGroup: [
                  {
                    "key": "contrato",
                    "type": "horizontalText",
                    "templateOptions": {
                      "label": "Contrato",
                      "valueProp": "id",
                      "attrCascade":"nombre"
                    }
                  },
                  {
                    "key": "region",
                    "type": "horizontalText",
                    "templateOptions": {
                      "label": "Región",
                      "valueProp": "id",
                      "attrCascade":"nombre"
                    },
                    hideExpression:"(model.contrato.id !== 1 && model.contrato.id !== 3)"
                  },
                  {
                    "key": "proveedor",
                    "type": "horizontalText",
                    "templateOptions": {
                      "label": "Proveedor",
                      "valueProp": "id",
                      "attrCascade":"nombre"
                    },
                    hideExpression:"(model.contrato.id !== 1 && model.contrato.id !== 3) || model.region === undefined "
                  }
                ]
              }
            ]
          },
          {
            className: 'col-lg-6',
            fieldGroup:[
              {
                className: 'row',
                fieldGroup: [
                  {
                    key: 'nombre',
                    type: 'horizontalSimpleText',
                    templateOptions: {
                      label: 'Nombre'
                    }
                  },
                  {
                    "key": "descripcion",
                    type: 'horizontalSimpleText',
                    "templateOptions": {
                      "label": "Descripción"
                    }
                  },
                ]
              }
            ]
          },
        ]
      },
      {
        className:'row',
        fieldGroup:[
          {
            key:"cubicadorServiciosTitulo",
            "type":"titleFancy",
            "templateOptions":{
              "buttons":[{
                "icon":"fa fa-trash-o",
                "click":function(model,to,scope){
                  model.cubicadorServicios = [];
                },
                tooltip:"Borrar Todo"
              }],
              "title":"Servicios"
            },
            hideExpression:"(model.contrato.id !== 1 && model.contrato.id !== 3) || model.proveedor === undefined || model.region === undefined || model.contrato === undefined || model.tipoServicio === undefined || model.disabled === true"
          }
        ]
      },
      {
        className: 'row',
        fieldGroup:[
          {
            key:"tableServicios",
            type:"simpleTable",
            templateOptions:{
              title:"Resumen Servicios",
              targetkey:"cubicadorServicios",
              totalKey:"totalServicios",
              columns:[{
                nestedField:"descripcion",
                field: "servicio",
                title: 'Descripción',
                sortable: "descripcion",
                visible: true
              },
                {
                  field: "cantidad",
                  title: 'Cantidad',
                  sortable: "name",
                  visible: true,
                  class:"text-center",
                  groupable: "name"
                },
                {
                  nestedField:"precio",
                  field: "servicio",
                  title: 'Precio',
                  currency:true,
                  class:"text-center",
                  sortable: "precio",
                  visible: true
                },
                {
                  expression:function(objeto){
                    var total = 0;
                    var cantidad = 0;
                    if(objeto.cantidad){ cantidad = objeto.cantidad}
                    total = cantidad * objeto.servicio.precio;
                    objeto.total = total;
                    return total;
                  },
                  field: "total",
                  class:"text-center",
                  currency:true,
                  title: 'Total',
                  sortable: "total",
                  visible: true
                }]
            },
            hideExpression:"(model.contrato.id !== 1 && model.contrato.id !== 3) || !model.cubicadorServicios"
          }
        ]
      },
      {
        className: 'row',
        fieldGroup:[
          {
            className: 'row',
            fieldGroup: [
              {
                key:"tableItemsCLP",
                type:"simpleTable",
                templateOptions:{
                  totalKey:"totalCLP",
                  condition:function(objeto){
                    if(objeto.tipoMoneda)
                      if(objeto.tipoMoneda.id == 2)return true;

                    return false;
                  },
                  title:"Resumen Items $(CLP)",
                  targetkey:"cubicadorOrdinario",
                  columns:[
                    {
                      field: "item",
                      title: 'Items',
                      sortable: "item",
                      visible: true
                    },
                    {
                      field: "cantidad",
                      title: 'Cantidad',
                      sortable: "name",
                      visible: true,
                      class:"text-center",
                      groupable: "name"
                    },
                    {
                      field: "precio",
                      title: 'Precio',
                      currency:true,
                      class:"text-center",
                      sortable: "precio",
                      visible: true
                    },
                    {
                      expression:function(objeto){
                        var total = 0;
                        var cantidad = 0;
                        if(objeto.cantidad){ cantidad = objeto.cantidad}
                        total = cantidad * objeto.precio;
                        objeto.total = total;
                        return total;
                      },
                      field: "total",
                      class:"text-center",
                      currency:true,
                      title: 'Total',
                      sortable: "total",
                      visible: true
                    }]
                },
                hideExpression:"model.contrato.id !== 2"
              }
            ]
          },
          {
            className: 'row',
            fieldGroup: [
              {
                key:"tableItemsUS",
                type:"simpleTable",
                templateOptions:{
                  totalKey:"totalUS",
                  title:"Resumen Items US$",
                  condition:function(objeto){
                    if(objeto.tipoMoneda)
                      if(objeto.tipoMoneda.id == 1)return true;

                    return false;
                  },
                  targetkey:"cubicadorOrdinario",
                  columns:[
                    {
                      field: "item",
                      title: 'Items',
                      sortable: "item",
                      visible: true
                    },
                    {
                      field: "cantidad",
                      title: 'Cantidad',
                      sortable: "name",
                      visible: true,
                      class:"text-center",
                      groupable: "name"
                    },
                    {
                      field: "precio",
                      title: 'Precio',
                      sortable: "precio",
                      currency:true,
                      currencySimbol:"US$",
                      class:"text-center",
                      visible: true
                    },
                    {
                      expression:function(objeto){
                        var total = 0;
                        var cantidad = 0;
                        if(objeto.cantidad){ cantidad = objeto.cantidad}
                        total = cantidad * objeto.precio;
                        objeto.total = total;
                        return total;
                      },
                      field: "total",
                      title: 'Total',
                      sortable: "total",
                      currency:true,
                      currencySimbol:"US$",
                      class:"text-center",
                      visible: true
                    }
                  ]
                },
                hideExpression:"model.contrato.id !== 2"
              }
            ]
          },
          {
            className: 'row',
            fieldGroup: [
              {
                key:"tableItemsUF",
                type:"simpleTable",
                templateOptions:{
                  totalKey:"totalUF",
                  title:"Resumen Items UF",
                  targetkey:"cubicadorOrdinario",
                  condition:function(objeto){
                    if(objeto.tipoMoneda)
                      if(objeto.tipoMoneda.id == 3)return true;

                    return false;
                  },
                  columns:[
                    {
                      field: "item",
                      title: 'Items',
                      sortable: "item",
                      visible: true
                    },
                    {
                      field: "cantidad",
                      title: 'Cantidad',
                      sortable: "name",
                      visible: true,
                      class:"text-center",
                      groupable: "name"
                    },
                    {
                      field: "precio",
                      title: 'Precio',
                      sortable: "precio",
                      "sufijo":"UF",
                      class:"text-center",
                      visible: true
                    },{
                      expression:function(objeto){
                        var total = 0;
                        var cantidad = 0;
                        if(objeto.cantidad){ cantidad = objeto.cantidad}
                        total = cantidad * objeto.precio;
                        objeto.total = total;
                        return total;
                      },
                      field: "total",
                      title: 'Total',
                      sortable: "total",
                      sufijo:"UF",
                      class:"text-center",
                      visible: true
                    }]
                },
                hideExpression:"model.contrato.id !== 2"
              }
            ]
          },
          {
            className: 'row',
            fieldGroup: [
              {
                key:"tableItemsEURO",
                type:"simpleTable",
                templateOptions:{
                  totalKey:"totalEURO",
                  title:"Resumen Items EURO",
                  targetkey:"cubicadorOrdinario",
                  condition:function(objeto){
                    if(objeto.tipoMoneda)
                      if(objeto.tipoMoneda.id == 4)return true;

                    return false;
                  },
                  columns:[
                    {
                      field: "item",
                      title: 'Items',
                      sortable: "item",
                      visible: true
                    },
                    {
                      field: "cantidad",
                      title: 'Cantidad',
                      sortable: "name",
                      visible: true,
                      class:"text-center",
                      groupable: "name"
                    },
                    {
                      field: "precio",
                      title: 'Precio',
                      sortable: "precio",
                      currency:true,
                      currencySimbol:"€",
                      class:"text-center",
                      visible: true
                    },{
                      expression:function(objeto){
                        var total = 0;
                        var cantidad = 0;
                        if(objeto.cantidad){ cantidad = objeto.cantidad}
                        total = cantidad * objeto.precio;
                        objeto.total = total;
                        return total;
                      },
                      field: "total",
                      title: 'Total',
                      sortable: "total",
                      currency:true,
                      currencySimbol:"€",
                      class:"text-center",
                      visible: true
                    }]
                },
                hideExpression:"model.contrato.id !== 2"
              }
            ]
          }
        ]
      }

    ];


    $scope.objeto = objeto;
    $scope.modal = {};
    $scope.modal.tituloModal = "Detalle " + objeto.nombre;
    $scope.formulario = form;

    $scope.cancel = function () {
      $modalInstance.dismiss();
    };

  });
