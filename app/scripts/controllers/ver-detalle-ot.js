'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ModalVerDetalleOtInstanceCtrl
 * @description
 * # ModalVerDetalleOtInstanceCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('VerDetalleOtCtrl', function ($scope,$filter,$modal,$q,$log,ngTableParams,CRUD,Session,$stateParams,tipoService) {
    var headerTableActa = [
      {
        title: 'Descripción',
        type:"label",
        field: 'descripcion',
        visible: true
      },
      {
        title: 'Cantidad Costeada',
        type:"label",
        descripcion:"Cantidad original costeada.",
        field: 'cantidadCubicada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Acomulada Validada',
        descripcion:"Suma de todas las cantidades informadas y validadas por el Gestor.",
        type:"label",
        field: 'cantidadAcumuladaInformada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Actual Informada',
        type:"label",
        descripcion:"Cantidad Informada en el Acta actual.",
        field: 'cantidadActualInformada',
        class:"text-center",
        visible: true
      }
    ];
    var headerTableActaAdionales = [
      {
        title: 'Descripción',
        type:"label",
        field: 'descripcion',
        visible: true
      },
      {
        title: 'Cantidad Costeada',
        type:"label",
        descripcion:"Cantidad original cubicada.",
        field: 'cantidadCubicada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Acomulada Validada',
        descripcion:"Suma de todas las cantidades informadas y validadas por el Gestor.",
        type:"label",
        field: 'cantidadAcumuladaInformada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Actual Informada',
        type:"label",
        descripcion:"Cantidad Informada en el Acta actual.",
        field: 'cantidadActualInformada',
        class:"text-center",
        visible: true
      }
    ];


    $scope.tipo = $stateParams.tipo;
    $scope.nombre = $stateParams.nombre;
    $scope.id = $stateParams.id;
    $scope.templateUrl = 'views/templates/loader-custom-tamplate.html';
    $scope.libroActas = {};
    $scope.libroActas.actas = [];
    $scope.libroActas.librosDeObras = [];
    $scope.libroActas.resumen =[];
    $scope.cubicacion = {servicios:[],materiales:[]};
    $scope.adjuntos = [];
    $scope.promise = tipoService.listar(undefined,"obtenerDetalleOt",{otId:$scope.id}).then(function(objeto) {

      $scope.libroActas.actas = JSON.parse(objeto.detalleOt.actas);
      $scope.libroActas.librosDeObras = JSON.parse(objeto.detalleOt.librosDeObras);
      $scope.detalleOt = JSON.parse(objeto.detalleOt.detalleOt);
      $scope.adjuntos = JSON.parse(objeto.detalleOt.adjuntos);
      //Se ignora por mientras si es ordinario
      if(objeto.contrato.id !==2){
      $scope.cubicacion = JSON.parse(objeto.detalleOt.cubicacion);
      }
      $scope.objeto = objeto;

    });

    $scope.changeTab = function(tab) {
      $scope.view_tab = tab;
      $scope.currentActa = undefined;
      $scope.currentLibroDeObre = undefined;
    };

    $scope.view_tab = "tabCosteos";

    var headerTable = [
      {
        title: 'Descripción',
        type:"label",
        field: 'descripcion',
        visible: true
      },
      {
        title: 'Cantidad',
        type:"label",
        descripcion:"Cantidad original de Costeo.",
        field: 'cantidad',
        class:"text-center",
        visible: true
      }
    ];

    $scope.headerTableLibrodeObras = [
      {
        title: 'Creador',
        type:"label",
        field: 'creador',
        visible: true
      },
      {
        title: 'Fecha Creación',
        type:"label",
        field: 'fechaCreacion',
        class:"text-center",
        visible: true
      },
      {
        title: 'Observaciones',
        type:"label",
        field: 'observaciones',
        class:"text-center",
        visible: true
      }
    ];

    $scope.headerTableActas = [

      {
        title: 'Fecha Creación',
        type:"label",
        field: 'fechaCreacion',
        class:"text-center",
        visible: true
      }
    ];

    $scope.headerTableResumenActas = [
      {
        title: 'Creador',
        type:"label",
        field: 'creador',
        visible: true
      },
      {
        title: 'Fecha Creación',
        type:"label",
        field: 'fechaCreacion',
        class:"text-center",
        visible: true
      },
      {
        title: 'Observaciones',
        type:"label",
        field: 'observaciones',
        class:"text-center",
        visible: true
      }
    ];

    $scope.headerTableResumenLibroObras = [
      {
        title: 'Nombre Archivo',
        type:"label",
        field: 'nombreArchivo',
        visible: true
      },
      {
        title: 'Extensión',
        type:"label",
        field: 'extension',
        class:"text-center",
        visible: true
      },
      {
        title: 'Descargar',
        type:"download",
        class:"text-center",
        visible: true
      }
    ];

    $scope.descargar = function(obj){
      window.location="descargarArchivo?libroDeObraId="+$scope.currentLibroDeObre.id+"&nombreArchivo="+obj.nombreArchivo+"&otId="+$scope.id+"&usuarioId="+Session.userId+"&tk="+Session.tk;
    };
    $scope.descargarAdjunto = function(obj){
      window.location="descargarArchivo?&nombreArchivo="+obj.nombreArchivo+"&otId="+$scope.id+"&usuarioId="+Session.userId+"&tk="+Session.tk;
    };

    $scope.tableCosteosMateriles= {
      key: 'materiales',
      type: 'TableForm',
      templateOptions: {
        requerid: true,
        columns: headerTable
      }
    };

    $scope.tableCosteosServicios = {
      key: 'servicios',
      type: 'TableForm',
      templateOptions: {
        requerid: true,
        columns: headerTable
      }
    };





    $scope.tableParamsResumen = new ngTableParams({
        page: 1,            // show first page
        count: 100,          // count per page
        sorting: {
          creador: 'asc'     // initial sorting
        }
      },
      {
        total:  $scope.libroActas.resumen.length, // length of data
        getData: function($defer, params) {
          // use build-in angular filter
          var orderedData = params.sorting() ?
            $filter('orderBy')( $scope.libroActas.resumen, params.orderBy()) :
            $scope.libroActas.resumen;
          params.total(orderedData.length);
          $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
      });

    $scope.mostrarResumenLibroObras = true;

    $scope.setDetalleDataLibroObras = function(obj){
      $scope.currentLibroDeObre  = obj;
      $log.info(obj);
      $scope.libroActas.resumen = obj.librosDeObras;
    };

    $scope.setDetalleDataActas = function(objeto){
      $scope.currentActa = objeto;
      $scope.tabActaFields = [{
        key: 'detalleServiciosActa',
        type: 'TableForm',
        templateOptions: {
          requerid:true,
          columns: headerTableActa
        }
      }];
    };

    $scope.setCurrentTabActaServiciosMateriales = function(tipo){

      if(tipo=="servicios"){

        $scope.tabActaFields = [{
          key: 'detalleServiciosActa',
          type: 'TableForm',
          templateOptions: {
            requerid:true,
            columns: headerTableActa
          }
        }];
      }
      if(tipo=="serviciosAdicionales"){

        $scope.tabActaFields = [{
          key: 'detalleServiciosAdicionalesActa',
          type: 'TableForm',
          templateOptions: {
            requerid:true,
            columns: headerTableActaAdionales
          }
        }];
      }
      if(tipo=="materiales"){

        $scope.tabActaFields = [{
          key: 'detalleMaterialesActa',
          type: 'TableForm',
          templateOptions: {
            requerid:true,
            columns: headerTableActa
          }
        }];
      }
      if(tipo=="materialesAdicionales"){

        $scope.tabActaFields = [{
          key: 'detalleMaterialesAdicionalesActa',
          type: 'TableForm',
          templateOptions: {
            requerid:true,
            columns: headerTableActaAdionales
          }
        }];
      }

    };





    $scope.formulario = [
      {
        className: 'row',
        fieldGroup:[
          {
            className: 'col-lg-6',
            fieldGroup:[
              {
                className: 'row',
                fieldGroup: [{
                  key: 'titulo',
                  type: 'titleFancy',
                  templateOptions: {
                    title:"Materiales Costeados"
                  }
                },
                  {
                    key: 'detalleMaterialesActa',
                    type: 'TableForm',
                    templateOptions: {
                      requerid:true,
                      columns: headerTableActa
                    }
                  }]
              },
              {
                className: 'row',
                fieldGroup: []
              }
            ]
          },
          {
            className: 'col-lg-6',
            fieldGroup:[
              {
                className: 'row',
                fieldGroup: [{
                  key: 'titulo',
                  type: 'titleFancy',
                  templateOptions: {
                    title:"Servicios Costeados"
                  }
                },
                  {
                    key: 'detalleServiciosActa',
                    type: 'TableForm',
                    templateOptions: {
                      requerid:true,
                      columns:headerTableActa
                    }
                  }
                ]
              },
              {
                className: 'row',
                fieldGroup: []
              }
            ]
          }
        ]
      },
      {
        className: 'row',
        fieldGroup: [
          {
            className: 'col-lg-6',
            fieldGroup: [
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Materiales Adicionales"
                }
              },
              {
                key: 'detalleMaterialesAdicionalesActa',
                type: 'TableForm',
                templateOptions: {
                  requerid:true,
                  columns:headerTableActaAdionales
                }
              }
            ]
          },
          {
            className: 'col-lg-6',
            fieldGroup: [
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Servicios Adicionales"
                }
              },
              {
                key: 'detalleServiciosAdicionalesActa',
                type: 'TableForm',
                templateOptions: {
                  requerid:true,
                  columns:headerTableActaAdionales
                }
              }
            ]
          }]
      },
      {
        className: 'row',
        fieldGroup: [
          {
            template: '<hr />'
          }
        ]
      },
      {
        className: 'row',
        fieldGroup: [
          {
            className: 'col-lg-6',
            fieldGroup: [
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Fechas"
                }
              },
              {
                "key": "fechaCreacion",
                "type": "horizontalDatepicker",
                "templateOptions": {
                  "fecha":true,
                  "label": "Fecha Creación",
                  "disabled":true,
                  "type": "text",
                  "datepickerPopup": "dd-MM-yyyy"
                }
              },{
                "key": "fechaTerminoEstimada",
                "type": "horizontalDatepicker",
                "templateOptions": {
                  "fecha":true,
                  "disabled":true,
                  "label": "Fecha Término Estimada",
                  "type": "text",
                  "datepickerPopup": "dd-MM-yyyy"
                }
              },
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Validación"
                }
              },
              {
                key:"validacionSistema",
                type:"horizontalCheckbox",
                //defaultValue:objeto.validacionSistema.toString(),
                templateOptions: {
                  disabled:true,
                  label:"Validación Sistema"
                }
              }
            ]
          },
          {
            className: 'col-lg-6',
            fieldGroup: [
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Observaciones"
                }
              },
              {
                key: 'observaciones',
                type: 'TableForm',
                templateOptions: {
                  "sorting":{fecha:"desc"},
                  requerid:true,
                  columns:[
                    {
                      title: 'Nombre',
                      type:"label",
                      field:'usuario',
                      visible: true
                    },
                    {
                      title: 'Observación',
                      type:"label",
                      field: 'observaciones',
                      visible: true
                    },
                    {
                      title: 'Fecha',
                      type:"label",
                      formatter:"date",
                      field: 'fecha',
                      class:"text-center",
                      visible: true
                    }
                  ]
                }
              }
            ]
          }]
      }
    ];

  });
