'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ModalVerDetalleActaCtrl
 * @description
 * # ModalVerDetalleActaCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('ModalVerDetalleActaCtrl', function ($scope,$log,$q,$modalInstance,objeto) {


    var headerTableActa = [
      {
        title: 'Descripción',
        type:"label",
        field: 'descripcion',
        visible: true
      },
      {
        title: 'Cantidad Cubicada',
        type:"label",
        descripcion:"Cantidad original cubicada.",
        field: 'cantidadCubicada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Acomulada Validada',
        descripcion:"Suma de todas las cantidades informadas y validadas por el Gestor.",
        type:"label",
        field: 'cantidadAcumuladaInformada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Actual Informada',
        type:"label",
        descripcion:"Cantidad Informada en el Acta actual.",
        field: 'cantidadActualInformada',
        class:"text-center",
        visible: true
      }
    ];
    var headerTableActaAdionales = [
      {
        title: 'Descripción',
        type:"label",
        field: 'descripcion',
        visible: true
      },
      {
        title: 'Cantidad Costeada',
        type:"label",
        descripcion:"Cantidad original cubicada.",
        field: 'cantidadCubicada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Acomulada Validada',
        descripcion:"Suma de todas las cantidades informadas y validadas por el Gestor.",
        type:"label",
        field: 'cantidadAcumuladaInformada',
        class:"text-center",
        visible: true
      },
      {
        title: 'Cantidad Actual Informada',
        type:"label",
        descripcion:"Cantidad Informada en el Acta actual.",
        field: 'cantidadActualInformada',
        class:"text-center",
        visible: true
      }
    ];

    $scope.formulario = [
      {
        className: 'row',
        fieldGroup:[
          {
            className: 'col-lg-6',
            fieldGroup:[
              {
                className: 'row',
                fieldGroup: [{
                  key: 'titulo',
                  type: 'titleFancy',
                  templateOptions: {
                    title:"Materiales Costeados"
                  }
                },
                  {
                    key: 'detalleMaterialesActa',
                    type: 'TableForm',
                    templateOptions: {
                      requerid:true,
                      columns: headerTableActa
                    }
                  }]
              },
              {
                className: 'row',
                fieldGroup: []
              }
            ]
          },
          {
            className: 'col-lg-6',
            fieldGroup:[
              {
                className: 'row',
                fieldGroup: [{
                  key: 'titulo',
                  type: 'titleFancy',
                  templateOptions: {
                    title:"Servicios Costeados"
                  }
                },
                  {
                    key: 'detalleServiciosActa',
                    type: 'TableForm',
                    templateOptions: {
                      requerid:true,
                      columns:headerTableActa
                    }
                  }
                ]
              },
              {
                className: 'row',
                fieldGroup: []
              }
            ]
          }
        ]
      },
      {
        className: 'row',
        fieldGroup: [
          {
            className: 'col-lg-6',
            fieldGroup: [
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Materiales Adicionales"
                }
              },
              {
                key: 'detalleMaterialesAdicionalesActa',
                type: 'TableForm',
                templateOptions: {
                  requerid:true,
                  columns:headerTableActaAdionales
                }
              }
            ]
          },
          {
            className: 'col-lg-6',
            fieldGroup: [
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Servicios Adicionales"
                }
              },
              {
                key: 'detalleServiciosAdicionalesActa',
                type: 'TableForm',
                templateOptions: {
                  requerid:true,
                  columns:headerTableActaAdionales
                }
              }
            ]
          }]
      },
      {
        className: 'row',
        fieldGroup: [
          {
            template: '<hr />'
          }
        ]
      },
      {
        className: 'row',
        fieldGroup: [
          {
            className: 'col-lg-6',
            fieldGroup: [
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Fechas"
                }
              },
              {
                "key": "fechaCreacion",
                "type": "horizontalDatepicker",
                "templateOptions": {
                  "fecha":true,
                  "label": "Fecha Creación",
                  "disabled":true,
                  "type": "text",
                  "datepickerPopup": "dd-MM-yyyy"
                }
              },{
                "key": "fechaTerminoEstimada",
                "type": "horizontalDatepicker",
                "templateOptions": {
                  "fecha":true,
                  "disabled":true,
                  "label": "Fecha Término Estimada",
                  "type": "text",
                  "datepickerPopup": "dd-MM-yyyy"
                }
              },
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Validación"
                }
              },
              {
                key:"validacionSistema",
                type:"horizontalCheckbox",
                //defaultValue:objeto.validacionSistema.toString(),
                templateOptions: {
                  disabled:true,
                  label:"Validación Sistema"
                }
              }
            ]
          },
          {
            className: 'col-lg-6',
            fieldGroup: [
              {
                key: 'titulo',
                type: 'titleFancy',
                templateOptions: {
                  title:"Observaciones"
                }
              },
              {
                key: 'observaciones',
                type: 'TableForm',
                templateOptions: {
                  "sorting":{fecha:"desc"},
                  requerid:true,
                  columns:[
                    {
                      title: 'Nombre',
                      type:"label",
                      field:'usuario',
                      visible: true
                    },
                    {
                      title: 'Observación',
                      type:"label",
                      field: 'observaciones',
                      visible: true
                    },
                    {
                      title: 'Fecha',
                      type:"label",
                      formatter:"date",
                      field: 'fecha',
                      class:"text-center",
                      visible: true
                    }
                  ]
                }
              }
            ]
          }]
      }
    ];

    $scope.cancel = function () {
      $modalInstance.dismiss();
    };
    $scope.objeto = objeto;
  });
