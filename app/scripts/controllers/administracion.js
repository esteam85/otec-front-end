'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:GestionAlarmasNotificacionesCtrl
 * @description
 * # GestionAlarmasNotificacionesCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('AdministracionCtrl', function ($scope,titulo,tabData) {

    $scope.titulo = titulo;

    $scope.tabData = tabData;
  });
