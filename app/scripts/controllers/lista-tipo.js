'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ListaTipoCtrl
 * @description
 * # ListaTipoCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('ListaTipoCtrl', function ($scope,$modal,$log,$filter,ngTableParams,tipoService,leyenda,tituloBoton,metodos,opcionesCrearEditar) {

    $scope.leyenda = leyenda;
    $scope.tituloBoton = tituloBoton || 'Crear '+leyenda;

    //Objeto Json a mostrar en tabla
    var objetos = [];

    function refresh(){
      tipoService.listar(metodos.listar).then(function(data){
        objetos = data;
        $scope.tableParams.reload();

      },function(error){

      });
    };

    $scope.tableParams = new ngTableParams({
      page: 1,            // show first page
      count: 10,          // count per page
      sorting: {
        nombre: 'asc'     // initial sorting
      }
    }, {
      total: objetos.length, // length of data
      getData: function($defer, params) {
        // use build-in angular filter
        var orderedData = params.sorting() ?
          $filter('orderBy')(objetos, params.orderBy()) :
          objetos;
        params.total(orderedData.length);
        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
      }
    });

    //Carga inicial de lista.
    refresh();

    $scope.crearEditar = function (objeto) {

      var modalInstance = $modal.open({
        templateUrl: 'views/templates/crear-editar-tipo.html',
        controller: 'ModalCrearEditarTipoInstance',
        resolve: {
          objeto: function () {
            return objeto;
          },
          opciones: function(){
            return opcionesCrearEditar;
          },
          leyenda :function(){
            return leyenda;
          }

        }
      });

      modalInstance.result.then(function (data) {

        refresh();

      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };


  });
