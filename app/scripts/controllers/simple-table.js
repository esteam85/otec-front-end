'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:SimpleTableCtrl
 * @description
 * # SimpleTableCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('SimpleTableCtrl', function ($scope,$log,$filter,tipoService,ngTableParams,$timeout) {

    var watchPromise = undefined;
    $scope.objetos = [];
    $scope.angular = angular;

    function consolidarColumnas(columns){
      var newColumns = angular.copy(columns);
      _.each(newColumns,function(column){
        if(column.nestedField !== undefined)column.field = column.nestedField;
      });
      return newColumns;
    }

    $scope.tableParams = new ngTableParams({
      page: 1,            // show first page
      count: 10,          // count per page
      sorting: {
        descripcion: 'asc'     // initial sorting
      }
    }, {
      total:  $scope.objetos.length, // length of data
      getData: function($defer, params) {
        // use build-in angular filter

        var orderedData = params.sorting() ?
          $filter('orderBy')( $scope.objetos, params.orderBy()) :
          $scope.objetos;
        params.total(orderedData.length);
        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

      }
    });
    $scope.to.columnsB = consolidarColumnas($scope.to.columns);
    function refresh(){
      $scope.tableParams.reload();
    }

    $scope.$watch("model." +$scope.to.targetkey,function(newVal,oldVal){
      if(watchPromise){
        $timeout.cancel(watchPromise);
      }
      watchPromise = $timeout(function(){

      if(newVal != undefined && oldVal != undefined){
        if($scope.to.consolidarDatos){
          $scope.to.consolidarDatos(newVal,$scope.to.columns,$scope)
        }else{
        consolidarDatos(newVal,$scope.to.columns);
        }
      }

      refresh();
        watchPromise = false;
      },1000);

    },true);

    function consolidarDatos(data,columns){
      $scope.objetos = [];

      _.each(data,function(itemData){

        if($scope.to.condition){
          if(!$scope.to.condition(itemData)){
            return;
          }
        }

        var newObj = {};
        _.each(columns,function(column){

          if(column.nestedField == undefined){
            if(column.expression){
              newObj[column.field] = column.expression(itemData) || undefined;
            }else{
              newObj[column.field] = itemData[column.field] || undefined;
            }
          }else{
            newObj[column.nestedField] = itemData[column.field][column.nestedField] || undefined;
          }


        });


        $scope.objetos.push(newObj);

      });
    }

    $scope.refresh = refresh;
    refresh();

    $scope.sum = sum;

    function sum(data, field){
      $scope.model[$scope.to.totalKey] = _.sum(data, field);
      return $scope.model[$scope.to.totalKey];
    }

  });
