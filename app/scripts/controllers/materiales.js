'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:RolesCtrl
 * @description
 * # RolesCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('MaterialesCtrl',function ($scope,$modal,$log,$filter,ngTableParams,usuariosService) {


    var materiales = [{codigo: "FRTS-20", nombre: "Tipo Servicio 1", descripcion: "lalalala", valor:23.3, stockActual: 0, estado:"A"},
      {codigo: "FRTS-21", nombre: "Tipo Servicio 2", descripcion: "lalalala", valor:23.3, stockActual: 1, estado:"A"},
      {codigo: "FRTS-22", nombre: "Tipo Servicio 3", descripcion: "cacacacac", valor:23.3, stockActual: 2, estado:"A"},
      {codigo: "FRTS-23", nombre: "Tipo Servicio 4", descripcion: "sasasasas", valor:23.3, stockActual: 3, estado:"A"},
      {codigo: "FRTS-24", nombre: "Tipo Servicio 5", descripcion: "wawawawawaw", valor:23.3, stockActual: 4, estado:"A"}
    ];

    //
    function refreshMateriales(){
      //usuariosService.listarRoles().then(function(data){
      //  roles = data;
      //  $scope.tableParamsRoles.reload();
      //},function(error){
      //  //mostrar toast
      //});
      $scope.tableParamsMateriales.reload();
    };
    //
    $scope.tableParamsMateriales = new ngTableParams({
      page: 1,            // show first page
      count: 10,          // count per page
      sorting: {
        nombre: 'asc'     // initial sorting
      }
    }, {
      total: materiales.length, // length of data
      getData: function($defer, params) {
        // use build-in angular filter
        var orderedData = params.sorting() ?
          $filter('orderBy')(materiales, params.orderBy()) :
          materiales;

        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
      }
    });

    refreshMateriales();

    $scope.crearEditarMaterial = function (materiales) {

      var modalInstance = $modal.open({
        templateUrl: 'views/templates/crear-editar-material.html',
        controller: 'ModalCrearEditarMaterialInstance',
        resolve: {
          materiales: function () {
            return materiales;
          }
        }
      });

      modalInstance.result.then(function () {
        refreshMateriales();
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
    //
    //$scope.eliminarRol = function (objeto) {
    //
    //  var modalInstance = $modal.open({
    //    templateUrl: 'views/templates/eliminar-administracion.html',
    //    controller: 'ModalEliminarGestionInstanceCtrl',
    //    resolve: {
    //      opciones: function () {
    //        return {
    //          objeto:objeto,
    //          texto: "Estas seguro que desea eliminar a",
    //          nombre: objeto.nombre,
    //          tipoObjeto: "Rol"
    //        };
    //      }
    //    }
    //  });
    //
    //  modalInstance.result.then(function () {
    //  }, function () {
    //    $log.info('Modal dismissed at: ' + new Date());
    //  });
    //};

  });
