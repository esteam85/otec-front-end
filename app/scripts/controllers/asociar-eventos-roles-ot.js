'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:AsociarEventosRolesOtCtrl
 * @description
 * # AsociarEventosRolesOtCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('AsociarEventosRolesOtCtrl', function ($scope,opciones,objeto,leyenda,$modalInstance,tipoService,Session,ngToast,$log) {
    var data = opciones.data;
    $scope.titulo = leyenda;
    $scope.objeto = objeto;
    $scope.formulario = [];
    var objetoFinal = {};
    var eventosFinal = [];
    var eventosRoles = [];
    var contador = 0;
    tipoService.listarPorIdChris('listarEventosPorContratoAction','contratoId',data.contratoId).then(function(info){
    //tipoService.listarPorIdChris('listarEventosPorContratoAction','contratoId',3).then(function(info){

       eventosRoles = JSON.parse(info.listadoRolesPorPerfil)[0].workflowEventos;

       eventosRoles = eventosRoles.reverse();
      _.each(eventosRoles,function(value,index){
        //if(value.rol.id == Session.userRole.id){
        if(true){
          var usuarios = [];
          tipoService.listarPorIdChris('listarUsuariosPorRoleAction','rolId',value.rol.id).then(function(infoRole){

            usuarios = JSON.parse(infoRole.listadoUsuariosPorRol);
            var comboConEvento = {
              key: contador.toString(),
              type: 'horizontalUiSelect',
              templateOptions: {
                label: value.evento.nombre,
                placeholder: 'Seleccionar Usuario Telefónica',
                options:usuarios,
                "valueProp": "id",
                "labelProp": "nombres",
                required: true
              }
            };
            eventosFinal.push(value.evento);
            $scope.formulario.push(comboConEvento);
            contador++;
          });
        }
      });
    });




    $scope.asociar = function(){

      objetoFinal.idOt = data.id;
      objetoFinal.eventos=[];
      _.each($scope.objeto,function(item,index){
        objetoFinal.eventos.push(eventosFinal[index]);
        objetoFinal.eventos[index].duracion = $scope.objeto[index.toString()].id;
      });
      $log.info(objetoFinal);
      $scope.submitting = true;

      tipoService.crearEditarChris('crear','asignarResponsableWorkflowEventoEjecucion',"parametros",objetoFinal).then(function(data){
        ngToast.create("Eventos Asociados con éxito.");
        $modalInstance.close(data);
      },function(error){
        ngToast.danger("Ocurrio un erro, intente mas tarde.");
        $modalInstance.dismiss(error);
      });

    };

    $scope.cancel = function () {
      $modalInstance.dismiss();
    };

  });
