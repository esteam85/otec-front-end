'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ProveedoresCtrl
 * @description
 * # ProveedoresCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('AdministracionItemCtrl',function ($scope,$state,tabData) {


    $scope.tabData   = tabData;

    var def = _.find($scope.tabData, function(tab){ return tab.default == true});

    $state.go(def.route);


  });
