'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:MultipleFileUploadCtrl
 * @description
 * # MultipleFileUploadCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('MultipleFileUploadCtrl', function ($scope,$log,$filter,$q,$timeout,Upload,fileUpload,Session,ngToast) {
    $scope.model[$scope.options.key] = false;
    $scope.obj = {};
    $scope.filesValidation = true;
    $scope.files= [];
    $scope.uploadFiles = function (files) {
      $scope.files = _.union($scope.files,files);
    };

    $scope.valorFormField = [{
      key: 'observaciones',
      type: 'horizontalTextarea',
      templateOptions: {
        label: 'Observaciones*',
        placeholder: 'Observaciones...'
      }
    },
      {
      "key": "usuariosRoles",
      "type": "horizontalUiMultiSelectGET",
      "templateOptions": {
        "defaultFunction":function(model,options,scope){

          var defaultVal = _.filter(options, function(val){
            if(val.id==2 || val.id==3 || val.id==4 || val.id==5 || val.id==7){
              val.inactive = true;
            return true;
            }
          });
          $scope.obj.usuariosRolesDefault = defaultVal;
          scope.to.options = [];

          return defaultVal;
        },
        "placeholder": "Roles",
        "label": "Roles*",
        "valueProp": "id",
        "labelProp": "nombre",
        "metodo":"Rol"
      }
    }];

    if($scope.to.noObservaciones){
      $scope.obj.observaciones = true;
      $scope.valorFormField = _.reject( $scope.valorFormField, function(item){ return item.key == "observaciones" });
    }

    $scope.remove = function(file){

      $scope.files = _.reject( $scope.files, function(item){ return item == file });

    };

    $scope.$watch('files',function(newv,oldv){
      if($scope.to.filesRequeried && newv.length==0){
       $scope.filesValidation = false;
      }else{
        $scope.filesValidation = true;
      }
    });

    $scope.subirInformeDeAvance = function () {
      var files = $scope.files;
      $scope.promesa = fileUpload.uploadMultipleFileToUrl(files, $scope.to.action || "informarAvanceOt",{
          observaciones:$scope.obj.observaciones,
          usuarioId:Session.userId,
          roles:$scope.obj.usuariosRoles,
          objCoreOT: $scope.to.objCoreOt ||"LibroObra",
          otId:$scope.model.otId || $scope.model.id
        }).then(function(){
          $scope.files= [];
          $scope.obj.observaciones = undefined;
          $scope.obj.usuariosRoles = $scope.obj.usuariosRolesDefault;
          ngToast.success( $scope.to.mensajeExito || "Informe de Avance Exitoso");
        if($scope.to.required){
          $scope.model[$scope.options.key] = true;
        }
        },function(){
          ngToast.danger($scope.to.mensajeError ||  "Informe de Avance Fallido");
        });
    };

  });
