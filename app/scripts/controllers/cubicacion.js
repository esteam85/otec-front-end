'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:CubicacionCtrl
 * @description
 * # CubicacionCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('CubicacionCtrl',function ($scope,$modal,$log,$filter,$stateParams,ngTableParams,tipoService,leyenda,tituloBoton,metodos,opcionesCrearEditar,tableHeader,Session) {


    var desdeBoton = $stateParams.desdeBoton;


    $scope.columns = tableHeader;
    $scope.leyenda = leyenda;
    $scope.tituloBoton = tituloBoton;
    $scope.form = opcionesCrearEditar.form;
    $scope.metodos = metodos;
    $scope.opcionesCrearEditar = opcionesCrearEditar;
    var objetos = [];

    function refresh(){
        $scope.tableParams.reload();
    };

    $scope.tableParams = new ngTableParams({
      page: 1,            // show first page
      count: 10,          // count per page
      sorting: {
        fechaCreacion: 'desc'     // initial sorting
      }
    }, {
      total: objetos.length, // length of data
      getData: function($defer, params) {

        console.log(params.orderBy()[0]);
        console.log( params.sorting());

        $scope.myPromise = tipoService.listar(metodos.listar,metodos.action,{estado:metodos.parametros.estado,pos:(params.page() - 1) * params.count(),cantidad:params.count(),orderBy:params.orderBy()[0]} || metodos.parametros).then(function(data){

          objetos = data.listado;
          var orderedData = params.sorting() ?
            $filter('orderBy')(objetos, params.orderBy()) :
            objetos;
          params.total(data.total);
          $defer.resolve(orderedData);

        });




      }
    });

    $scope.crearEditar = function (objeto) {

      if(!!opcionesCrearEditar.templateUrl && !!opcionesCrearEditar.controller){
        var modalInstance = $modal.open({
          size:"xl",
          templateUrl: opcionesCrearEditar.templateUrl,
          controller: opcionesCrearEditar.controller
        });
      }else{
        var modalInstance = $modal.open({
          templateUrl: 'views/templates/crear-editar.html',
          controller: 'ModalCrearEditarInstance',
          size: opcionesCrearEditar.modalSize || '',
          backdrop:opcionesCrearEditar.backdrop || true,
          resolve: {
            objeto: function () {
              return objeto;
            },
            opciones: function(){
              return opcionesCrearEditar;
            },
            leyenda:function(){
              return leyenda;
            }

          }
        });
      }

      modalInstance.result.then(function (data) {

        if(opcionesCrearEditar.proximoModal == "CargarImagen"){
          var modalInstance = $modal.open({
            templateUrl: 'views/templates/adjuntar-imagen.html',
            controller: 'ModalEnviarImagenInstance',
            //size: opcionesCrearEditar.modalSize || '',
            //windowClass: 'xx-dialog',
            resolve: {
              objeto: function () {
                return objeto;
              },
              opciones: function(){
                return{
                  tipo: "avatar-",
                  rut: JSON.parse(data.parametros).rut
                };
              },
              leyenda:function(){
                return leyenda;
              }

            }
          });

        }

        if(opcionesCrearEditar.proximoModal == "OTeventosRoles"){
          var modalInstance = $modal.open({
            templateUrl: 'views/templates/asociar-eventos-roles-ot.html',
            controller: 'AsociarEventosRolesOtCtrl',
            size:'lg',
            resolve: {
              objeto: function () {
                return objeto;
              },
              opciones: function(){
                return{
                  data: data
                };
              },
              leyenda:function(){
                return "Asociar Eventos con Usuario Telefónica";
              }

            }
          });

        }

        if(opcionesCrearEditar.proximoModal == "detalle-cubicacion"){
          var modalInstance = $modal.open({
            templateUrl: 'views/templates/detalle-cubicacion.html',
            controller: 'DetalleCubicacionCtrl',
            size:'lg',
            resolve: {
              objeto: function () {
                return objeto;
              },
              opciones: function(){
                return{
                  data: data
                };
              },
              leyenda:function(){
                return "Detalle Cubicación";
              }

            }
          });

        }


        refresh();

      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    $scope.listarDetalle = function (objeto,column) {

      var totalObj= objeto[column.lista.field];
      if(totalObj == undefined){
        objeto[column.lista.field] = [];
      }

      if(column.lista) {
        var modalInstance = $modal.open({
          templateUrl: 'views/templates/lista-detalle.html',
          controller: 'ModalListaDetalleInstanceCtrl',
          size: function () {
            //Por mientras solo se mostrara el nombre en los listar detalle de cada configuración, por eso debe ser mediano el modal, en ""
            if (objeto[column.lista.field].length > 0)return "";
            //if (objeto[column.field].length > 0)return column.lista.size;

            return "sm";
          },
          resolve: {
            objeto: function () {
              return objeto;
            },
            column: function () {
              return column;
            },
            opciones: function () {
              return opcionesCrearEditar;
            },
            refreshParent: function () {
              return refresh;
            }
          }
        });

        modalInstance.result.then(function (data) {

        }, function () {
        });
      }
    };

    $scope.eliminar = function (objeto) {

      var modalInstance = $modal.open({
        templateUrl: 'views/templates/quitar-eliminar.html',
        controller: 'ModalQuitarEliminarInstanceCtrl',
        resolve: {
          opciones: function () {
            return {
              objeto:objeto,
              texto: "Estas seguro que desea eliminar ",
              nombre: objeto.nombre,
              metodo:opcionesCrearEditar.metodos.editar
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        refresh();
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };


    if(desdeBoton == "1"){
      $scope.crearEditar();
    }

    $scope.verDetalle = function(obj){

      tipoService.listar(undefined,"obtenerDetalleCubicacion",{cubicacionId:obj.id}).then(function(data){

        var modalInstance = $modal.open({
          templateUrl: 'views/templates/general-formly.html',
          controller: 'VerDetalleCubicacionCtrl',
          size: opcionesCrearEditar.modalSize || '',
          resolve: {
            objeto: function () {
              return data;
            }
          }
        });

      });
    };

    $scope.$on("crear-cubicacion",function(event,args){
      $scope.crearEditar();
    });

    $scope.$on('$stateChangeSuccess',
      function(event, toState, toParams, fromState, fromParams){

        if(toState.name === "gestion-cubicacion-general.cubicacion" && fromState.name !== "gestion-cubicacion-general.cubicacion" && fromState.name !== "" && Session.userRole.id == 2 && toParams.desdeBoton == "true"){
          $scope.crearEditar();
        }

      });

  });
