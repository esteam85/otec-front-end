'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ModalGeneralFormlyCtrl
 * @description
 * # ModalGeneralFormlyCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('ModalGeneralFormlyCtrl', function ($scope,$log,$q,$filter,$modalInstance,ID_EVENTOS,objeto,opciones){

    $scope.objeto = objeto;
    $scope.modal = opciones.modal;
    $scope.formulario = opciones.formulario;

    $scope.cancel = function () {
      $modalInstance.dismiss();
    };

  });
