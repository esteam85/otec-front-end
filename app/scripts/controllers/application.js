'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ApplicationCtrl
 * @description
 * # ApplicationCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('ApplicationCtrl', function ($scope,$state,$log,$modal,CRUD,AuthService,USER_ROLES,AUTH_EVENTS,Session,$rootScope,$window,$timeout,$interval) {

    $scope.logged = false;
    //var opciones = {ID_EVENTO:5};
    //var form=[
    //{
    //    key: 'uploader',
    //    type: 'multipleFileUpload'
    //  }
    //];
    //_.extend(opciones,{
    //  codigo:"AC-AVANCE",
    //  AccionesPorEvento:undefined,
    //  proximaAccion:undefined,
    //  formulario:form,
    //  modal:{
    //    "tituloModal": "Informar Avance",
    //    "tituloBoton": "Cerrar Inf de Avance"
    //  }});
    //
    //
    //var modalInstance = $modal.open({
    //  templateUrl: 'views/templates/modal-crear-editar-ot.html',
    //  controller: 'ModalCrearEditarOtInstance',
    //  resolve: {
    //    objeto: function () {
    //      return {"AC-AVANCE":{otId:80}};
    //    },
    //    opciones: function(){
    //      return opciones;
    //    }
    //  }
    //});
    //
    //modalInstance.result.then(function (data) {
    //  //modalDinamico(data.objeto,0,data.AccionesPorEvento,ID_EVENTOS.CREAR_OT);
    //},function (data) {
    //
    //});
    $scope.notificactionsCount = 0;
    var userInfoNoParsed = $window.sessionStorage["userInfo"];
    var roleInfoNoparsed = $window.sessionStorage["roleInfo"];
    var sessionToken = $window.sessionStorage["TK"];
    if(userInfoNoParsed != undefined && userInfoNoParsed != "undefined" && roleInfoNoparsed != "undefined" && roleInfoNoparsed != undefined) {
      var userInfo = JSON.parse(userInfoNoParsed);
      var roleInfo = JSON.parse(roleInfoNoparsed);
      $rootScope.currentUser = userInfo;
      $rootScope.currentUserRole = roleInfo;
      $rootScope.currentSessionToken = sessionToken;
      $scope.logged = true;
    }else{
      $rootScope.currentUser = null;
      $rootScope.currentUserRole = null;
      $rootScope.currentSessionToken = null;
    }

    $scope.userRoles = USER_ROLES;
    $scope.isAuthorized = AuthService.isAuthorized;

    $rootScope.$on(AUTH_EVENTS.loginSuccess,function(){
      $scope.logged = true;
    });

    $scope.logout = function(){
      AuthService.logOut().then(function(data){

      Session.destroy();
      $window.sessionStorage["userInfo"] = undefined;
      $window.sessionStorage["roleInfo"] = undefined;
      $window.sessionStorage["TK"] = undefined;
      $rootScope.currentUser = undefined;
      $rootScope.currentUserRole = undefined;
      $scope.logged = false;
      $state.go('login');
      });
    };

    $scope.setCurrentUser = function (user) {
      $window.sessionStorage["userInfo"] = JSON.stringify(user);
      $rootScope.currentUser = user;
    };
    $scope.setCurrentUserRole = function (role) {
      $rootScope.currentUserRole = role;
      $window.sessionStorage["roleInfo"] = JSON.stringify(role);
    };

    $scope.setCurrentSessionToken = function (token) {
      $rootScope.currentSessionToken = token;
      $window.sessionStorage["TK"] = token;
    };

    $scope.isLoginPage=true;

   $scope.traerNotificaciones = function(){
      if(Session.userId){
        CRUD.listarCustom("obtenerNotificaciones",{idUsuario:Session.userId},{ignoreLoadingBar: true}).then(function(data){
          $scope.listadoNotificacion = JSON.parse(data.retorno);
          $scope.listadoNotificacionNoread = _.filter($scope.listadoNotificacion, function(item){ return item.leido === false; });
        });
      }
    };

    $rootScope.$on(AUTH_EVENTS.loginSuccess,function(){
      $scope.traerNotificaciones();
    });

    $timeout($scope.traerNotificaciones,1000);
    $interval($scope.traerNotificaciones,60000);

    $scope.crearOt = function(){
      if($state.current.name !== "gestion-ot-general.ot"){
      $state.go("gestion-ot-general.ot",{desdeBoton:true});
      }else{
        $rootScope.$broadcast("crear-ot");
      }
    };

    $scope.crearCubicacion = function(){
      if($state.current.name !== "gestion-cubicacion-general.cubicacion"){
      $state.go("gestion-cubicacion-general.cubicacion",{desdeBoton:true});
      }else{
        $rootScope.$broadcast("crear-cubicacion");
      }
    };

  });
