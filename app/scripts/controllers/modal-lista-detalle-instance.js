'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ModalListaDetalleInstanceCtrl
 * @description
 * # ModalListaDetalleInstanceCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('ModalListaDetalleInstanceCtrl', function ($q,$scope,$log,$modal,$filter,$modalInstance,ngTableParams,ngToast,objeto,column,opciones,tipoService, refreshParent,nombreHeaderTable) {

  //$scope.columns = column.lista.tableHeader;
  //Por mientras solo se mostrara el nombre en los listar detalle de cada configuración, en el futuro que vuelva a ser con tableHeader

  if(!!column.lista.usarTableHeader){
    $scope.columns = column.lista.tableHeader;
    if(typeof objeto[column.lista.field][0] == 'string'){
      _.each(objeto[column.lista.field],function(item,index){

        objeto[column.lista.field][index] = JSON.parse(item);
      });
    }

  }else{$scope.columns = nombreHeaderTable;}

  $scope.titulo = column.title;

  //Esto es para refrescar o no la vista padre al cerrar.
  var refresh = false;


    $scope.tableParams = new ngTableParams({
      page: 1,            // show first page
      count: 10,          // count per page
      sorting: {
        nombre: 'asc'     // initial sorting
      }
    }, {
      total:  objeto[column.lista.field].length, // length of data
      getData: function($defer, params) {
        // use build-in angular filter

        var orderedData = params.sorting() ?
          $filter('orderBy')( objeto[column.lista.field], params.orderBy()) :
          objeto[column.lista.field];
        params.total(orderedData.length);
        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

      }
    });

    $scope.tableParams.reload();

    $scope.cancel = function () {
      $modalInstance.dismiss(refresh);
    };

    $scope.quitar = function (obj) {

      var modalInstance = $modal.open({
        templateUrl: 'views/templates/quitar-eliminar.html',
        controller: 'ModalQuitarEliminarInstanceCtrl',
        resolve: {
          opciones: function () {
            return {
              objeto:obj,
              texto: "Estas seguro que desea quitar ",
              nombre: obj.nombre,
              titulo: "Quitar",
              tituloBoton: "Quitar",
              accion:"quitar"
            };
          }
        }
      });

      modalInstance.result.then(function (data) {

        if(data == "quitar") {
          objeto[column.lista.field] = _.without(objeto[column.lista.field], _.findWhere(objeto[column.lista.field], obj));
          editar(objeto);
          //refresh = true;

        }
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

     function editar(objeto) {
       _.each(opciones.formulario, function(item) {

         if(item.templateOptions.fecha === true ){
           objeto[item.key] = $filter('date')(objeto[item.key],'dd-MM-yyyy');
         }
       });

       objeto.accion = "editar";
       tipoService.crearEditar(objeto, opciones.metodos.crear, opciones.metodos.editar).then(function (data) {
         $scope.tableParams.reload();
         refreshParent();
         ngToast.create(data.success);
       }, function (error) {
         ngToast.danger(error.danger);
       });
     };
});
