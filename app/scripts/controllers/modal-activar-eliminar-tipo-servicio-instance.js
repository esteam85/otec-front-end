'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ModalcreareditartipoproveedorinstanceCtrl
 * @description
 * # ModalcreareditartipoproveedorinstanceCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('ModalActivarEliminarTipoServicioInstance', function ($scope,$log,$modalInstance,usuariosService,tipoServicio,estado,ngToast) {


    $scope.tipoServicio = tipoServicio ;


    $scope.titulo = 'Necesitamos que confirme la operación';


    $scope.crearEditarTipoServicio = function () {
      tipoServicio.estado = estado;

      usuariosService.crearEditarTipoServicio(tipoServicio).then(function(data){
        ngToast.success(data.mensaje);
        $log.info("Tipo Servicio creado o editado");
        $modalInstance.close();
      },function(data){
        ngToast.danger(data.mensaje);
        $log.info("Error al crear o Editar Proveedor");
        $modalInstance.dismiss();
      });
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };



  });
