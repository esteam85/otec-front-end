'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:AnadireditarCtrl
 * @description
 * # AnadireditarCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('ModalEnviarImagenInstance', function ($q,$scope,$log,$filter,$modalInstance,Restangular,FileUploader,fileUpload,$modal,ngToast,opciones) {

    var uploader = $scope.uploader = new FileUploader({
      url: '/enviarImagen'
    });

    uploader.filters.push({
      name: 'imageFilter',
      fn: function(item /*{File|FileLikeObject}*/, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
    });

    $scope.uploadFile = function(){

      var file = $scope.myFile;
      var nombreImagen = opciones.tipo + opciones.rut;
      console.log('file is ' );
      console.dir(file);
      var uploadUrl = "enviarImagen";
      fileUpload.uploadFileToUrl(file, uploadUrl,nombreImagen).then(function(){
      ngToast.create("Imagen almacenda correctamente");
      });
      $modalInstance.close();
    };

    $scope.cancel = function () {
      $modalInstance.dismiss();
    };



  });
