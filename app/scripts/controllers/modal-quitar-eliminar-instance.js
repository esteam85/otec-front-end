'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ModalEliinarUsuarioInstanceCtrl
 * @description
 * # ModalEliinarUsuarioInstanceCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('ModalQuitarEliminarInstanceCtrl', function ($scope,$q,$log,$modalInstance,tipoService,ngToast,opciones) {

    $scope.objeto = opciones.objeto;

    $scope.texto = opciones.texto || "Estas seguro de eliminar a";

    $scope.titulo = opciones.titulo || "Eliminar ";

    $scope.nombre = opciones.nombre;

    $scope.tituloBoton = opciones.tituloBoton || "Eliminar";

    $scope.quitarEliminarUsuario = function(){
      if(opciones.accion == "quitar"){

        $modalInstance.close("quitar");

      }else{
        tipoService.eliminar($scope.objeto,opciones.metodo).then(function(data){
          ngToast.create(data.success);
          $modalInstance.close(data);
        },function(error){
          ngToast.danger(error.danger);
          $modalInstance.dismiss(error);
        });
      }
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

  });
