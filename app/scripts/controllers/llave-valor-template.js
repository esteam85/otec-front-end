'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:LlaveValorTemplateCtrl
 * @description
 * # LlaveValorTemplateCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('LlaveValorTemplateCtrl', function ($scope,$log,$filter,tipoService,ngTableParams) {


    $scope.requerido= $scope.to.requerid;


    $scope.model[$scope.options.key] =  $scope.model[$scope.options.key] || [];


    if(!!$scope.options.templateOptions.defaults.length && $scope.options.templateOptions.defaults.length>0 && $scope.model[$scope.options.key].length === 0 ){
      $scope.model[$scope.options.key] =  angular.copy($scope.options.templateOptions.defaults);

    }else{
      //Hacer obligatorios los que son por default
      _.each($scope.options.templateOptions.defaults,function(value,index){

        $scope.model[$scope.options.key][index].obligatorio = value.obligatorio;

      });
    }

    $scope.columns= $scope.options.templateOptions.columns;

    function refresh(){
      $scope.tableParams.reload();
    };

    $scope.tableParams = new ngTableParams({
      page: 1,            // show first page
      count: 10,          // count per page
      sorting: {
        nombre: 'asc'     // initial sorting
      }
    }, {
      total: $scope.model[$scope.options.key].length, // length of data
      getData: function($defer, params) {
        // use build-in angular filter

        var orderedData = params.sorting() ?
          $filter('orderBy')($scope.model[$scope.options.key], params.orderBy()) :
          $scope.model[$scope.options.key];
        params.total(orderedData.length);
        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

      }
    });

    $scope.quitar = function(obj){

      $scope.model[$scope.options.key]= _.reject($scope.model[$scope.options.key], function(item){ return item == obj });
      refresh();
    };

    $scope.addOption = function(){

      $scope.model[$scope.options.key].push({llave:'',valor:'',"isTemplateOption":true});
      refresh();
    };
  });
