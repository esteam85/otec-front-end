'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:AnadireditarCtrl
 * @description
 * # AnadireditarCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('ModalCrearEditarInstance', function ($q,$scope,$log,$filter,$modalInstance,$modal,ngToast,objeto,opciones,leyenda,tipoService,Session) {

    $scope.formulario = opciones.formulario;
    if(!!objeto){
      $scope.tituloBoton = "Actualizar";
      $scope.titulo= opciones.tituloEditar || 'Editar '+leyenda;
      $scope.objeto = _.clone(objeto);
      $scope.objeto.disabled = false;
    }else{
      $scope.objeto = {estado:"A",accion:'crear'};
      $scope.tituloBoton = "Crear";
      $scope.titulo= opciones.tituloCrear || 'Nuevo '+leyenda;
    }

    if(opciones.verDetalle){
      $scope.titulo= leyenda;
      $scope.tituloBoton = undefined;
      $scope.objeto = _.clone(objeto);
      $scope.objeto.disabled = true;
      opciones.verDetalle = undefined;
    }

    _.each(opciones.formulario, function(item) {


      if(item.type === "horizontalUiSelect" || item.type === "horizontalUiMultiSelect" || item.type === "horizontalUiMultiSelectInput" ){
        if(!item.templateOptions.options){
        item.templateOptions.options = [];

        tipoService.listar(item.templateOptions.metodo,item.templateOptions.action).then(function(data){
          if(item.type === "horizontalUiMultiSelectInput"){
            var dataEnvolved = [];
            _.each(data ,function(it){
              var newItem = {};
              newItem[item.templateOptions.subkey] = it;
              dataEnvolved.push(newItem);
            });
            item.templateOptions.options = dataEnvolved;
          }else{
          item.templateOptions.options = data;
          }
        });
        }
      }

      if(item.type === "horizontalInputRut" && !!objeto){
        //Se agrega el digito verificador al editar
        $scope.objeto.rut = objeto.rut + objeto.dv;
      }

    });

    $scope.crearEditar = function () {

      $scope.objeto.usuario={id:Session.userId};
      $scope.objeto.gestor={id:Session.userId};

      $scope.submitting = true;
      if(!opciones.metodos){
        opciones.metodos = {};
        opciones.metodos.crear = undefined;
        opciones.metodos.editar = undefined;
      //significa que no estan porque "action"
      }
      $scope.promesa = tipoService.crearEditar($scope.objeto,opciones.metodos.crear,opciones.metodos.editar,leyenda,opciones.action).then(function(data){
        ngToast.create(data.success);

        if($scope.objeto.contrato){
        data.contratoId = $scope.objeto.contrato.id
        }
        data.objeto = $scope.objeto;
        $modalInstance.close(data);
      },function(error){
        ngToast.danger(error.danger);
        $modalInstance.dismiss(error);
      });

    };

    $scope.cancel = function () {
        $modalInstance.dismiss();
    };

  });
