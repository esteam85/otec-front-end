'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('LoginCtrl', function ($scope,$log,$state,$rootScope,AUTH_EVENTS,AuthService,Session,ngToast,VERSION,Restangular) {

    $scope.showLogButton = false;
    $scope.selectedRole={};
    $scope.version = VERSION;
    $scope.credentials = {
      usuario: '',
      clave: ''
    };

    $scope.$watch('credentials.usuario', function() {
      $scope.showRoleSelect = false;
      $scope.showLogButton = false;
    });

    $scope.$watch('credentials.clave', function() {
      $scope.showRoleSelect = false;
      $scope.showLogButton = false;
    });

    $scope.buscar = function (credentials) {
      $scope.selectedRole.selected=undefined;
      AuthService.login(credentials).then(function (user) {
        $scope.setCurrentUser(user.datosUsuario);
        $scope.showLogButton = true;
        $scope.showRoleSelect = true;
        if(user.datosUsuario.usuariosRoles.length == 1){
          $scope.selectedRole.selected = user.datosUsuario.usuariosRoles[0];
        }
      }, function () {
        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        ngToast.danger("Usuario o Contraseña erroneos");
      });
    };


    $scope.login = function () {
      $scope.credentials.rol = $scope.selectedRole.selected;
      AuthService.login($scope.credentials).then(function (data) {
      Session.create($scope.currentUser.id, $scope.currentUser.id,$scope.selectedRole.selected,$scope.sessionToken);
      $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
      $scope.setCurrentUserRole(Session.userRole);
      $scope.setCurrentSessionToken(data.tk);
      $state.go('dashboard');
      }, function () {
        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        ngToast.danger("Error en login, intente mas tarde");
      });
    };

  });
