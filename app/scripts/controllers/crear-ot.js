'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:CrearOtCtrl
 * @description
 * # CrearOtCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('CrearOtCtrl', function ($scope,$state,$rootScope,$interval,$modal,$log,$filter,$stateParams,ID_EVENTOS,ngTableParams,tipoService,OtService,leyenda,tituloBoton,metodos,opcionesCrearEditar,tableHeader,Session,AccionesService) {

    $scope.session = Session;



    var desdeBoton = $stateParams.desdeBoton;

    $scope.columns = tableHeader;
    $scope.leyenda = leyenda;
    $scope.tituloBoton = tituloBoton;
    $scope.form = opcionesCrearEditar.form;
    $scope.metodos = metodos;
    var objetos = [];
    $scope.estadoListadoOt = metodos.parametros.estado;


    $scope.tableParams = new ngTableParams({
      page: 1,            // show first page
      count: 10,          // count per page
      sorting: {
        fechaCreacion: 'desc'     // initial sorting
      }
    },{
      total: 0, // length of data
      getData: function($defer, params) {

        $scope.myPromise = tipoService.listar(metodos.listar,metodos.action,{estado:metodos.parametros.estado,pos:(params.page() - 1) * params.count(),cantidad:params.count(),orderBy:params.orderBy()[0]} || metodos.parametros).then(function(data){
        //$scope.myPromise = tipoService.listar(metodos.listar,metodos.action,metodos.parametros).then(function(data){

          objetos = data.listado;
          //objetos = data;
          var orderedData = params.sorting() ?
            $filter('orderBy')(objetos, params.orderBy()) :
            objetos;
          params.total(data.total);
          $defer.resolve(orderedData);

        });



      }
    });

    function refresh(){
      $scope.tableParams.reload();
    }

    function modalDinamico(objeto,incremento,AccionesPorEvento,ID_EVENTO,paso) {

      if(incremento >= AccionesPorEvento.length){
        refresh();
        return false;
      }


      var objFinal = AccionesService.prepareEjecutarEvento(objeto,incremento,AccionesPorEvento,ID_EVENTO,paso);


      var modalInstance = $modal.open({
        templateUrl: 'views/templates/modal-crear-editar-ot.html',
        controller: 'ModalCrearEditarOtInstance',
        "backdrop":'static',
        size: opcionesCrearEditar.modalSize || objFinal.opciones.modal.modalSize ||'',
        resolve: {
          objeto: function () {
            return objFinal.objeto;
          },
          opciones: function(){
            return objFinal.opciones;
          }
        }
      });

      modalInstance.result.then(function (data) {

        modalDinamico(data.objeto,data.opciones.incremento,data.opciones.AccionesPorEvento,data.opciones.ID_EVENTO,data.opciones.paso);
      }, function () {
        //refresh();
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    $scope.eliminar = function (objeto) {

      var modalInstance = $modal.open({
        templateUrl: 'views/templates/quitar-eliminar.html',
        controller: 'ModalQuitarEliminarInstanceCtrl',
        resolve: {
          opciones: function () {
            return {
              objeto:objeto,
              texto: "Estas seguro que desea eliminar ",
              nombre: objeto.nombre,
              metodo:opcionesCrearEditar.metodos.editar
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        refresh();
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    $scope.crearEditar = function(){

      var modalInstance = $modal.open({
        templateUrl: 'views/templates/modal-crear-editar-ot.html',
        controller: 'ModalCrearEditarOtInstance',
        "backdrop":opcionesCrearEditar.backdrop,
        resolve: {
          objeto: function () {
            return {};
          },
          opciones: function(){
            return {
              "codigo": "AC",
              "proximaAccion": undefined,
              "AccionesPorEvento": undefined,
              "formulario": opcionesCrearEditar.formulario,
              "modal": {
                "tituloModal": "Crear Órden de Trabajo",
                "tituloBoton": "Siguiente"
              }
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        modalDinamico(data.objeto,0,data.AccionesPorEvento,ID_EVENTOS.CREAR_OT,1);
      },function (data) {

      });
    };

    if(desdeBoton == "1"){
      $scope.crearEditar();

    }

    $scope.ejecutarEvento = function(objeto){
      OtService.obtenerAccionesPorOtId(objeto.id).then(function(data){
        modalDinamico(objeto,0,data.acciones,data.eventoId,0);
      });
    };

    $scope.editarEjecutarEvento = function(objeto){

      tipoService.listar(undefined,'listarTrabajadorOt',{otId:objeto.id,eventoId:ID_EVENTOS.ASIGNAR_TRABAJADOR}).then(function(data){
        modalDinamico(objeto,0,[data],ID_EVENTOS.ASIGNAR_TRABAJADOR,0);
      });


    };

    $scope.listarDetalle = function(objeto,column){

      if(column.lista.request){
        tipoService.listar(undefined,column.lista.request.action,column.lista.request.parametros(objeto,column,$scope)).then(function(data){
          objeto[column.lista.field] = data;
          desplegarDetalle(objeto,column);
        },function(error){

        });

      }else{
        desplegarDetalle(objeto,column);
      }
    };


    function desplegarDetalle(objeto,column) {

      var totalObj= objeto[column.lista.field];
      if(totalObj == undefined){
        objeto[column.lista.field] = [];
      }

      if(column.lista) {
        var modalInstance = $modal.open({
          templateUrl: 'views/templates/lista-detalle-ot.html',
          controller: 'ModalListaDetalleInstanceCtrl',
          size: function () {
            //Por mientras solo se mostrara el nombre en los listar detalle de cada configuración, por eso debe ser mediano el modal, en ""
            if (objeto[column.lista.field].length > 0)return "";
            //if (objeto[column.field].length > 0)return column.lista.size;

            return "sm";
          },
          resolve: {
            objeto: function () {
              return objeto;
            },
            column: function () {
              return column;
            },
            opciones: function () {
              return opcionesCrearEditar;
            },
            refreshParent: function () {
              return '';
            }
          }
        });

        modalInstance.result.then(function (data) {

        }, function () {
        });
      }
    };


    $scope.descargarExcel = function(){

      var modalInstance = $modal.open({
        templateUrl: 'views/templates/descargar-excel-ot.html',
        controller: 'ModalDescargarExcelOtCtrl',
        resolve: {
          opciones: function(){
            return {
              "modal": {
                "tituloModal": "Exportar Listado OT"
              }
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
      },function (data) {

      });

    };

    $scope.adjuntarArchivos = function (obj){

      var formly =  [{
        key:"archivos",
        type:"multipleFileUpload",
        templateOptions:{
          action:"adjuntarArchivoOt",
          objCoreOt:"Adjunto",
          uploadButtonTitle:"Adjuntar Archivos",
          mensajeExito:"Archivos Adjuntados con Éxito",
          mensajeError:"Error al Adjuntar Archivos",
          filesRequeried:true
        }
      }];

      var modalInstance = $modal.open({
        templateUrl: 'views/templates/general-formly.html',
        controller: 'ModalGeneralFormlyCtrl',
        "backdrop":opcionesCrearEditar.backdrop,
        resolve: {
          objeto: function () {
            return obj;
          },
          opciones: function(){
            return {
              "formulario": formly,
              "modal": {
                "tituloModal": "Adjuntar Archivos",
                "tituloBoton": undefined
              }
            };
          }
        }
      });

      modalInstance.result.then(function (data) {
        modalDinamico(data.objeto,0,data.AccionesPorEvento,ID_EVENTOS.CREAR_OT,1);
      },function (data) {

      });


    }


    $scope.$on("crear-ot",function(event,args){
      $scope.crearEditar();
    });

    $scope.$on('$stateChangeSuccess',
      function(event, toState, toParams, fromState, fromParams){

        if(toState.name === "gestion-ot-general.ot" && fromState.name !== "gestion-ot-general.ot" && fromState.name !== "" && Session.userRole.id == 2 && toParams.desdeBoton == "true"){
          $scope.crearEditar();
        }

      });
  });
