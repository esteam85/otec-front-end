'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ApplicationCtrl
 * @description
 * # ApplicationCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('CambioClaveCtrl', function ($scope,$location,$state,$window,AuthService,ngToast) {

    var searchObject = $location.search();
    $scope.token = searchObject.token;


  $scope.buscar = function (credentials) {
    credentials.token =  $scope.token;
    AuthService.cambiarPass(credentials).then(function (user) {
      if(user == true){
        ngToast.success("Contraseña cambiada exitosamente");
        $state.go("dashboard");
        //$location.path("/login");
        //$window.location.href = '#/login';
      }else{
        ngToast.danger("Ocurrio un error al cambiar la contraseña. Intente mas tarde");
      }
    }, function () {
      ngToast.danger("Ocurrio un error al cambiar la contraseña. Intente mas tarde");
    });

    //alert("$scope.token: " + $scope.token + credentials.pass);

  };

  });
