'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ModalDescargarExcelOtCtrl
 * @description
 * # ModalDescargarExcelOtCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('ModalDescargarExcelOtCtrl', function ($scope,$log,$q,$filter,$modalInstance,ID_EVENTOS,opciones,Session){

    $scope.objeto = {};
    $scope.modal = opciones.modal;
    $scope.formulario = [{
      "key": "fechaInicio",
      "type": "horizontalDatepicker",
      "templateOptions": {
        "fecha":true,
        "minDate":"",
        "label": "Fecha Inicio",
        "type": "text",
        "datepickerPopup": "dd-MM-yyyy",
        required: true
      }
    },
      {
        "key": "fechaTermino",
        "type": "horizontalDatepicker",
        "templateOptions": {
          "targetMinDateKey":"fechaInicio",
          "fecha":true,
          "minDate":"",
          "label": "Fecha Término",
          "type": "text",
          "datepickerPopup": "dd-MM-yyyy",
          required: true
        }
      }];

    $scope.cancel = function () {
      $modalInstance.dismiss();
    };

    $scope.descargarExcel = function(){

      window.location="descargarListadoOt?esDescargaExcel=1&usuarioId="+Session.userId+"&fechaInicio="+$scope.objeto.fechaInicio+"&fechaTermino="+$scope.objeto.fechaTermino;

    };



  });
