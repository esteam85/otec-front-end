'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:TableTemplateCtrl
 * @description
 * # TableTemplateCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('TableTemplateCtrl', function ($scope,$log,$filter,tipoService,ngTableParams){


    //if($scope.to.parse){
    //  $scope.model[$scope.options.key] = JSON.parse($scope.model[$scope.options.key]);
    //}

    $scope.model[$scope.options.key] =  $scope.model[$scope.options.key] || [];

    if($scope.to.minData && $scope.model[$scope.options.key].length == 0){
      $scope.model[$scope.options.key].push($scope.to.minData);
    }

    $scope.columns =$scope.options.templateOptions.columns;

    $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 25,          // count per page
        sorting: $scope.to.sorting || {
          descripcion: 'asc'     // initial sorting
        }
      },
      {
        counts: [],
        total: $scope.model[$scope.options.key].length, // length of data
        getData: function($defer, params) {
          // use build-in angular filter
          var orderedData = params.sorting() ?
            $filter('orderBy')($scope.model[$scope.options.key], params.orderBy()) :
            $scope.model[$scope.options.key];
          params.total(orderedData.length);
          $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

        }
      });

    $scope.$watchCollection("model."+$scope.options.key,function(newVal,oldVal){
      if(newVal){
      refresh();
      }
    });

    $scope.validarForm = {
      key:"validar",
      type:"horizontalCheckboxNoLabel",
      "templateOptions": {
        required: true
      }
    };

    function refresh(){
      $scope.tableParams.reload();
    };

    $scope.quitar = function(obj){

      $scope.model[$scope.options.key]= _.reject($scope.model[$scope.options.key], function(item){ return item == obj });
      refresh();
    };

    $scope.addOption = function(){
      $scope.model[$scope.options.key]= _.union($scope.model[$scope.options.key],[{}]);
      refresh();
    };

  });
