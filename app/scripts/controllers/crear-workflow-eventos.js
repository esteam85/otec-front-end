'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:CrearWorkflowEventosCtrl
 * @description
 * # CrearWorkflowEventosCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('CrearWorkflowEventosCtrl', function ($scope,$filter,$modalInstance,tipoService,ngTableParams,ngToast) {


      $scope.muestroDetalleDesicion = false;
      $scope.muestroBotonAgregar = false;
      $scope.muestroTabla = false;
      $scope.errorEventoPadre = false;
      $scope.errorDesicionEvento = false;
      $scope.muestroTablaDesicionEvento = false;
      var objetos = [];
      var desicion = "";
      resetarCombos();

      $scope.eventosSeleccionados = [
        {nombre:"Evento 1", desicion:"No", proximo: "Evento2", prioridad: 0},
        {nombre:"Evento 2", desicion:"Si", opcion:"Aprueba", proximo: "Evento3", prioridad: 1},
        {nombre:"Evento 2", desicion:"Si", opcion:"Rechaza", proximo: "Evento4", prioridad: 2}
      ];

      //
      //$scope.listaEventos = [
      //  {id:"1", nombre:"Ingresar Carta"},
      //  {id:"2", nombre:"Asigar trabajo"},
      //  {id:"3", nombre:"Realizar trabajo"},
      //  {id:"4", nombre:"Validacion Trabajo"},
      //  {id:"5", nombre:"Pagos"}
      //];
      $scope.listaEventos = [];

      tipoService.listar("Evento").then(function(data){
        $scope.listaEventos = data;
      },function(error){
        //mostrar toast
      });

      $scope.listaRoles = [];

      tipoService.listar("Rol").then(function(data){
        $scope.listaRoles = data;
      },function(error){
        //mostrar toast
      });
      //$scope.listadetalleDesicion = [
      //  {id:"1", nombre:"Aprobado"},
      //  {id:"2", nombre:"Rechazar"},
      //  {id:"3", nombre:"Deribar"}
      //];

      $scope.listadetalleDesicion = [];

      tipoService.listar("Decision").then(function(data){
        $scope.listadetalleDesicion = data;
      },function(error){
        //mostrar toast
      });



      var workflowEventos = [];

      $scope.tableParams = new ngTableParams({
        page: 1,            // show first page
        count: 10          // count per page
        //sorting: {
        //  nombre: 'asc'     // initial sorting
        //}
      }, {
        total: objetos.length, // length of data
        getData: function($defer, params) {
          // use build-in angular filter

          var orderedData = params.sorting() ?
            $filter('orderBy')(objetos, params.orderBy()) :
            objetos;
          params.total(orderedData.length);
          $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

        }
      });

      $scope.tableWorkflowEventosParam = new ngTableParams({
        page: 1,            // show first page
        count: 10          // count per page
        //sorting: {
        //  nombre: 'asc'     // initial sorting
        //}
      }, {
        total: workflowEventos.length, // length of data
        getData: function($defer, params) {
          // use build-in angular filter

          var orderedData = params.sorting() ?
            $filter('orderBy')(workflowEventos, params.orderBy()) :
            objetos;
          params.total(orderedData.length);
          $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

        }
      });

      $scope.agregarDesicionEvento = function () {

        if($scope.eventoDetalleSeleccionado.selected && $scope.desicionSeleccionada.selected){
          $scope.muestroTablaDesicionEvento = true
          var idEventoSelected = $scope.eventoDetalleSeleccionado.selected;
          var idDetalleSelected = $scope.desicionSeleccionada.selected;
          var desicionEvento = {evento: idEventoSelected,desicion: idDetalleSelected}
          if(validaLista(objetos,desicionEvento)){
            objetos.push(desicionEvento);
            $scope.tableParams.reload();
            $scope.errorDesicionEvento = false;
          }else{
            $scope.errorDesicionEvento = true;
            $scope.errorDesicionEventoMensaje = "Ya agrego esta opción";
          }

        }else{
          $scope.errorDesicionEvento = true;
          $scope.errorDesicionEventoMensaje = "Tiene que seleccionar una Desicion y un Evento";

        }

      };


      $scope.desplegarDesiciones = function () {
        if(!$scope.eventoSeleccionado.selected){
          $scope.errorEventoPadre = true;
        }else{
          $scope.errorEventoPadre = false;
          $scope.muestroDetalleDesicion = true;
          $scope.muestroBotonAgregar = false;
          $scope.muestroEventoProximo = false;
          desicion = "Si";
        }

      };

      $scope.esconderDesiciones = function () {

        if(!$scope.eventoSeleccionado.selected){
          $scope.errorEventoPadreMensaje = "Tiene que seleccionar un Evento";
          $scope.errorEventoPadre = true;

        }else{
          $scope.errorEventoPadre = false;
          $scope.muestroBotonAgregar = true;
          $scope.muestroDetalleDesicion = false;
          $scope.muestroTablaDesicionEvento = false;
          desicion = "No";
          $scope.muestroEventoProximo = true;
        }

      };

      $scope.agregarEvento = function () {

        $scope.muestroBotonAgregar = false;
        $scope.muestroDetalleDesicion = false;
        $scope.muestroTablaDesicionEvento = false;
        if(validaListaWf2(workflowEventos,$scope.eventoSeleccionado.selected)){
          angular.forEach(objetos, function(value, key) {
            var obj = {
              id: $scope.eventoSeleccionado.selected.id,
              nombre: $scope.eventoSeleccionado.selected.nombre,
              rol: $scope.rolSeleccionado.selected,
              desicion: desicion,
              opcion: value.desicion,
              proximo: value.evento,
              prioridad: value.prioridad
            }

            workflowEventos.push(obj)
          });
          $scope.muestroTabla = true;
          $scope.tableWorkflowEventosParam.reload();

        }else{
          $scope.errorEventoPadreMensaje = "Ya agrego este evento";
          $scope.errorEventoPadre = true;
        }
        resetarCombos();

      };

      $scope.agregarEventoSolo = function () {

        $scope.muestroBotonAgregar = false;
        $scope.muestroDetalleDesicion = false;
        $scope.muestroTablaDesicionEvento = false;
        if($scope.eventoProximoSeleccionado.selected == $scope.eventoSeleccionado.selected){
          $scope.errorEventoPadreMensaje = "No puede ser el mismo evento el proximo";
          $scope.errorEventoPadre = true;
        } else {
          if(validaListaWf(workflowEventos,$scope.eventoSeleccionado.selected,$scope.eventoProximoSeleccionado.selected)){

            var obj = {
              id: $scope.eventoSeleccionado.selected.id,
              nombre: $scope.eventoSeleccionado.selected.nombre,
              rol: $scope.rolSeleccionado.selected,
              desicion: desicion,
              opcion: "--",
              proximo: $scope.eventoProximoSeleccionado.selected,
              prioridad: 0
            }
            workflowEventos.push(obj)

            $scope.muestroTabla = true;
            $scope.tableWorkflowEventosParam.reload();

          }else{
            $scope.errorEventoPadreMensaje = "Ya agrego este evento";
            $scope.errorEventoPadre = true;
          }
        }
        $scope.muestroEventoProximo = false;
        resetarCombos();

      };
      $scope.crearWorkflowEvento = function () {

        var parametros = {};
          parametros.accion = "crear",
          parametros.nombre = $scope.nombre,
          parametros.descripcion = $scope.descripcion,
          parametros.fechaCreacion = $scope.dt,
          parametros.estado = $scope.estado,
          parametros.workflowEventos = [],

          angular.forEach(workflowEventos, function(value, key) {

            var workflowEventos = {};
            workflowEventos.evento = { id: value.id };
            workflowEventos.rol = { id: value.rol.id };
            workflowEventos.decision = { id: value.opcion.id };
            workflowEventos.proximo = value.proximo.id,
            workflowEventos.prioridad = value.prioridad
            parametros.workflowEventos.push(workflowEventos);

          });

        tipoService.crearEditar(parametros,"Workflow","Workflow","Workflow").then(function(data){
          ngToast.create(data.success);
          $modalInstance.close(data);
        },function(error){
          ngToast.danger(error.danger);
          $modalInstance.dismiss(error);
        });

      };


      function validaListaWf2(lista, seleccion1){
        var respuesta = true;
        angular.forEach(lista, function(value, key) {
          if(value.id == seleccion1.id){
            respuesta = false;
          }
        });
        return respuesta;
      };

      function validaListaWf(lista, seleccion1,seleccion2){
        var respuesta = true;
        angular.forEach(lista, function(value, key) {
          if(value.id == seleccion1.id && value.proximo.id == seleccion2.id){
            respuesta = false;
          }
        });
        return respuesta;
      };

      function validaLista(lista, seleccion){
        var respuesta = true;
        angular.forEach(lista, function(value, key) {
          if(value.evento.id == seleccion.evento.id || value.desicion.id == seleccion.desicion.id){
            respuesta = false;
          }
        });
        return respuesta;
      };

      function resetarCombos(lista, seleccion){
        $scope.eventoDetalleSeleccionado = {};
        $scope.eventoSeleccionado = {};
        $scope.rolSeleccionado = {};
        $scope.desicionSeleccionada = {};
        $scope.eventoProximoSeleccionado = {};
        objetos = [];
        desicion = "";
        return;
      };


      $scope.format = 'dd-MMMM-yyyy';
      $scope.isOpen = false;

      $scope.openCalendar = function(e) {
        e.preventDefault();
        e.stopPropagation();

        $scope.isOpen = true;
      };

      $scope.today = function() {
        $scope.dt = new Date();
      };
      $scope.today();

      $scope.clear = function () {
        $scope.dt = null;
      };

      // Disable weekend selection
      $scope.disabled = function(date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
      };

      $scope.toggleMin = function() {
        //$scope.minDate = $scope.minDate ? null : new Date();
        $scope.minDate = new Date();
      };
      $scope.toggleMin();
      $scope.maxDate = new Date(2020, 5, 22);

      $scope.open2 = function($event) {
        $scope.status.opened = true;
      };

      $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
      };

      $scope.status = {
        opened: false
      };

      var tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);
      var afterTomorrow = new Date();
      afterTomorrow.setDate(tomorrow.getDate() + 2);
      $scope.events =
        [
          {
            date: tomorrow,
            status: 'full'
          },
          {
            date: afterTomorrow,
            status: 'partially'
          }
        ];

      $scope.getDayClass = function(date, mode) {
        if (mode === 'day') {
          var dayToCheck = new Date(date).setHours(0,0,0,0);

          for (var i=0;i<$scope.events.length;i++){
            var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

            if (dayToCheck === currentDay) {
              return $scope.events[i].status;
            }
          }
        }

        return '';
      };

      $scope.minDate = new Date();

      $scope.test = function() {
        $scope.minDate = new Date();
      }
  });
