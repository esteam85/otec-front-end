'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('DashboardCtrl', function ($scope,highchartsNG,highchartExamples,Session,GraficosService,$timeout) {

    $scope.chartConfig = [];

    if(Session.userRole.id == 2){
      $scope.chartConfig.push({class:'col-lg-6'});
      $scope.chartConfig.push({class:'col-lg-6'});
      $scope.chartConfig.push({class:'col-lg-6'});
      $scope.chartConfig.push({class:'col-lg-6'});
      $timeout(function(){
        $scope.chartConfig[0].grafico = GraficosService.QotCerradasVSEnCurso();
        $scope.chartConfig[2].grafico = GraficosService.QotPorProveedor();
        $scope.chartConfig[1].grafico = GraficosService.QapCerradasVSEnCurso();
        $scope.chartConfig[3].grafico = GraficosService.MontoPagadosPorProveedor();

      },1000);
    }

    if(Session.userRole.id == 5){
      $scope.chartConfig.push({class:'col-lg-12'});
      $scope.chartConfig.push({class:'col-lg-12'});
      $scope.chartConfig.push({class:'col-lg-12'});
      $scope.chartConfig.push({class:'col-lg-12'});
      $timeout(function(){
        $scope.chartConfig[0].grafico = GraficosService.PagosDevengadosPorPrograma();
        $scope.chartConfig[1].grafico = GraficosService.QotPorContratoPorPeriodo();
        $scope.chartConfig[2].grafico = GraficosService.MontoDeOtPorContratoPorPeriodo();
        $scope.chartConfig[3].grafico = GraficosService.PagosEstimadosVsPagosReales();
      },500);
    }
    if(Session.userRole.id == 6 ||Session.userRole.id == 9 || Session.userRole.id == 8 ){
      $scope.chartConfig.push({class:'col-lg-6'});
      $scope.chartConfig.push({class:'col-lg-6'});
      $scope.chartConfig.push({class:'col-lg-6'});
      $scope.chartConfig.push({class:'col-lg-6'});
      $timeout(function(){
        $scope.chartConfig[0].grafico = GraficosService.PagosPorProveedorPorPeriodoEncurso();
        $scope.chartConfig[1].grafico = GraficosService.PagosPorProveedorPorPeriodoFinalizados();
        $scope.chartConfig[2].grafico = GraficosService.CargaTotalPorProveedorPorPeriodo();
        $scope.chartConfig[3].grafico = GraficosService.CargaTotalPorProveedorPorPeriodoTrimestreAnterior();
      },500);
    }
    if(Session.userRole.id == 3 || Session.userRole.id == 7 ){
      $scope.chartConfig.push({class:'col-lg-6'});
      $scope.chartConfig.push({class:'col-lg-6'});
      $scope.chartConfig.push({class:'col-lg-6'});
      $scope.chartConfig.push({class:'col-lg-6'});
      $timeout(function(){
        $scope.chartConfig[0].grafico = GraficosService.QotCerradasVSEnCurso();
        $scope.chartConfig[1].grafico = GraficosService.QapCerradasVSEnCurso();
        $scope.chartConfig[2].grafico = GraficosService.PagosPorPeriodoEncurso();
        $scope.chartConfig[3].grafico = GraficosService.PagosPorPeriodoFinalizados();
      },500);
    }

  });
