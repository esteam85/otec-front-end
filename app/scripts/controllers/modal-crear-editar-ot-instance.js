'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:ModalCrearEditarOtCtrl
 * @description
 * # ModalCrearEditarOtCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('ModalCrearEditarOtInstance', function ($scope,$log,$q,$filter,$modalInstance,ID_EVENTOS,objeto,opciones,OtService,AccionesService,ngToast) {
    $scope.objeto = {};
    $scope.keyPropertyLenght = 0;
    $scope.promesa = false;

    _.each(opciones.formulario,function(item){
      if(item.fieldGroup != undefined)$scope.keyPropertyLenght = $scope.keyPropertyLenght + item.fieldGroup.length;
    });

    if(objeto[opciones.codigo])$scope.objeto = objeto[opciones.codigo];

    if(objeto[objeto.codigoAnterior])_.extend($scope.objeto,objeto[objeto.codigoAnterior]);

    $scope.objeto.otId = objeto.id;
    $scope.objeto.eventoId = opciones.ID_EVENTO;
    $scope.codigo = opciones.codigo;
    $scope.paso = opciones.paso;
    $scope.modal = opciones.modal;
    var servicio = opciones.modal.servicio;
    $scope.formulario = opciones.formulario;
    $scope.tabs = opciones.tabs;

    function seleccionarCrearOt(contratoId){
      //SBE
      if(contratoId == 1){
        return ID_EVENTOS.CREAR_OT_SBE;}
      if(contratoId == 3){
        return ID_EVENTOS.CREAR_OT_RAN;}

      return ID_EVENTOS.CREAR_OT;
    };

    $scope.crearEditar = function(botonData){
      $scope.submitting = true;
      objeto[opciones.codigo] = $scope.objeto;
      _.extend(objeto,$scope.objeto);
      objeto.codigoAnterior = opciones.codigo;
      if(!opciones.AccionesPorEvento){

        var parametros = {eventoId:seleccionarCrearOt($scope.objeto.contrato.id),contratoId:$scope.objeto.contrato.id,proveedorId:$scope.objeto.proveedor.id};
        OtService.obtenerAccionesPorEvento(parametros).then(function(item){

          $modalInstance.close({
            opciones:opciones,objeto: objeto,
            AccionesPorEvento:item});
        });

      }else{

        if(servicio == "obtenerAccionesFrontendPorEvento"){
          var parametros = {eventoId:botonData.eventoId,otIdAux:objeto.id};
          $scope.promesa = OtService.obtenerAccionesPorEvento(parametros).then(function(item){
            $modalInstance.close({
              opciones:{ AccionesPorEvento:item,ID_EVENTO:botonData.eventoId,incremento:0},objeto: objeto});
          },function(error){
            ngToast.danger("Error");
            $scope.submitting = false;
          });
        }

        if(!!servicio && !$scope.promesa){
        //if(!!servicio){
          $scope.promesa = OtService.ejecutarServicioAccion(servicio,AccionesService.prepararObjeto(objeto,opciones.ID_EVENTO,botonData),
            opciones.ID_EVENTO).then(function(data){
              ngToast.success("Ejecutado Correctamente");
              opciones.incremento++;
                $modalInstance.close({
                  opciones:opciones,objeto: objeto,retorno:data});
              },function(error){
              $scope.submitting = false;
              ngToast.danger("Error");
            });

        }

        if(!$scope.promesa){
            opciones.incremento++;
          $modalInstance.close({
            opciones:opciones,objeto: objeto});
        }
      }

    };

    $scope.cancel = function () {
      $modalInstance.dismiss();
    };

    $scope.volver = function(){

      $log.info("volver");
    };

    $scope.$on("cambiar:modal",function(event,data){
            $log.info($scope.modal);
            $scope.modal.tituloModal = data.tituloModal;
    });
  });
