'use strict';

/**
 * @ngdoc function
 * @name otecApp.controller:GestionUsuarioCuentaCtrl
 * @description
 * # GestionUsuarioCuentaCtrl
 * Controller of the otecApp
 */
angular.module('otecApp')
  .controller('GestionUsuarioCuentaCtrl', function ($scope,$log) {
    $scope.formulario = [{
      key: 'nombreUsuario',
      type: 'horizontalInput',
      templateOptions: {
        label: 'Nombre Usuario',
        placeholder: 'Nombre Usuario',
        required: true
      }
    },{
      key: 'nombres',
      type: 'horizontalInput',
      templateOptions: {
        label: 'Nombres',
        placeholder: 'Nombres',
        required: true
      }
    },{
      "key": "apellidos",
      type: 'horizontalInput',
      "templateOptions": {
        "placeholder": "Apellidos",
        "label": "Apellidos",
        required: true

      }
    },{
      "key": "rut",
      type: 'horizontalInputRut',
      "templateOptions": {
        "placeholder": "Rut",
        "label": "Rut",
        required: true

      }
    },{
      "key": "email",
      type: 'horizontalInput',
      "templateOptions": {
        "placeholder": "Email",
        "label": "Email",
        "type": "email",
        required: true

      }
    },{
      "key": "celular",
      type: 'horizontalInputMask',
      templateOptions: {
        "label": 'Celular',
        "placeholder": "Celular",
        mask: '(999) 99999999',
        required: true
      }
    }
    ]
  });
