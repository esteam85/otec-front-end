'use strict';

/**
 * @ngdoc overview
 * @name otecApp
 * @description
 * # otecApp
 *
 * Main module of the application.
 */
angular
  .module('otecApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.validate',
    'ngTable',
    'ui.bootstrap',
    'angular-loading-bar',
    'ui.select',
    'ui.router',
    'ui.router.tabs',
    'platanus.rut',
    'restangular',
    'formly',
    'formlyBootstrap',
    'ngToast',
    'ui.mask',
    'angularFileUpload',
    'highcharts-ng',
    'angularMoment',
    'directives.inputMatch',
    'ngFileUpload',
    'angular-notification-icons',
    'cgBusy',
    'ng-mfb',
    'rzModule'

  ]).config(['$tooltipProvider', function ($tooltipProvider) {
    $tooltipProvider.options({
      appendToBody: 'true',
      popupDelay: 800
    });
  }])
  .run(function ($rootScope, AUTH_EVENTS, AuthService,Session) {

    $rootScope.$on('$stateChangeSuccess', function(event, current, previous){
      $rootScope.pageTitle = current.title;
    });

    $rootScope.$on('$stateChangeStart', function (event, next) {
      var authorizedRoles = next.data.authorizedRoles;
      if(AuthService.logRequired(authorizedRoles)){
        if (!AuthService.isAuthorized(authorizedRoles)) {
          event.preventDefault();
          if (AuthService.isAuthenticated()) {
            // user is not allowed
            $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
          } else {
            // user is not logged in
            $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
          }
        }
      }else{
        if(!!Session.userId){
          event.preventDefault();
        }
      }

    });
  })
  .run(function($templateCache){
    $templateCache.put("template/datepicker/popup.html",
      "<div>\n" +
      "<ul class=\"dropdown-menu\" ng-if=\"isOpen\" style=\"display: block\" ng-style=\"{top: position.top+'px', left: position.left+'px'}\" ng-keydown=\"keydown($event)\" ng-click=\"$event.stopPropagation()\">\n" +
      "	<li ng-transclude></li>\n" +
      "	<li ng-if=\"showButtonBar\" style=\"padding:10px 9px 2px\">\n" +
      "		<span class=\"btn-group\">\n" +
        //"			<button type=\"button\" class=\"btn btn-sm btn-info pull-left\" ng-click=\"select('today')\">{{ getText('current') }}</button>\n" +
        //"			<button type=\"button\" class=\"btn btn-sm btn-danger pull-right\" ng-click=\"select(null)\">{{ getText('clear') }}</button>\n" +
      "		</span>\n" +
      "		<button type=\"button\" class=\"btn btn-sm btn-success pull-right\" ng-click=\"close()\">{{ getText('close') }}</button>\n" +
      "	</li>\n" +
      "</ul>\n" +
      "</div>");
  })
  .run(function(){

    Highcharts.setOptions({colors :['#033C73', '#3FA746', '#80699B', '#3D96AE',
      '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92']});
  });
