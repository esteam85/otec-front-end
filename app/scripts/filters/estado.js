'use strict';

/**
 * @ngdoc filter
 * @name otecApp.filter:estado
 * @function
 * @description
 * # estado
 * Filter in the otecApp.
 */
angular.module('otecApp')
  .filter('estado', function () {
    return function (input) {
      if(input == "A")return 'Activo';
      if(input == "E")return 'Eliminado';
      if(input == "I")return 'Inactivo';
      return input;
    };
  });
