'use strict';

/**
 * @ngdoc filter
 * @name otecApp.filter:picker
 * @function
 * @description
 * # picker
 * Filter in the otecApp.
 */
angular.module('otecApp')
  .filter('picker', function($filter) {
    return function(value, filterName) {
      try{
      return $filter(filterName)(value);
      }catch(e){
        return value;
      }
    };
  });
