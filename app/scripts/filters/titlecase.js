'use strict';

/**
 * @ngdoc filter
 * @name otecApp.filter:titlecase
 * @function
 * @description
 * # titlecase
 * Filter in the otecApp.
 */
angular.module('otecApp')
  .filter('titlecase', function() {
    return function(s) {
      s = ( s === undefined || s === null ) ? '' : s;
      return s.toString().toLowerCase().replace( /\b([a-z])/g, function(ch) {
        return ch.toUpperCase();
      });
    };
  });
