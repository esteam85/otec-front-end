'use strict';

/**
 * @ngdoc directive
 * @name otecApp.directive:notificationItem
 * @description
 * # notificationItem
 */
angular.module('otecApp')
  .directive('notificationItem', function () {
    return {
      template: "<li ui-sref=\"detalleOt({id:data.ot.id})\" ng-click=\"checkNotification(data)\" class=\"notification row\" ng-class=\"{\'notification-unread\':data.leido == false, \'notification-read\':data.leido == true}\">\n  <div class=\'row\'>\n  <i ng-class=\"{\'notification-icon-unread\':data.leido == false, \'notification-icon-read\':data.leido == true}\" class=\"{{icon}} icon col-lg-1\"></i>\n  <span class=\'col-lg-11 pull-right\'> {{data.mensaje}}</span>\n  </div>\n \n  <div class=\'row\'>\n    <span ng-show=\'data.leido\' class=\"col-lg-6 visto\"> visto </span>\n    <span class=\"fecha col-lg-6 pull-right\">fecha: {{data.fechaCreacion | date:\'dd-MM-yyyy\'}}</span>\n  </div>\n</li>\n<hr ng-hide=\"$parent.$last\" />",
      restrict: 'E',
      scope: {
        data: "=",
        refresh: '='
      },
      controller:["$scope","CRUD","$q",function($scope,CRUD,$q){

        switch($scope.data.eventoId) {
          case 1:
            $scope.icon = "fa fa-file-text-o fa-2x";
            break;
          case 2:
            $scope.icon = "fa fa-street-view fa-2x";
            break;
          case 3:
            $scope.icon = "fa fa-bookmark-o fa-2x";
            break;
          default:
            $scope.icon = "fa fa-exclamation-triangle fa-2x";
        }

        $scope.checkNotification = function(obj){
          if(!obj.leido){
          CRUD.listarCustom("leerNotificaciones",{idNotificacion:obj.id},{ignoreLoadingBar: true}).then(function(){
            $scope.data.leido = true;
            $scope.refresh();
          });
          }
        };
      }]
    };
  });
