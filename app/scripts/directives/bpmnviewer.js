'use strict';

/**
 * @ngdoc directive
 * @name otecApp.directive:bpmnViewer
 * @description
 * # bpmnViewer
 */
angular.module('otecApp')
  .directive('bpmnViewer', function () {
    return {
      template: '<div id="canvas"></div>',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        var BpmnViewer = window.BpmnJS;
        var viewer = new BpmnViewer({ container: '#canvas' });
      }
    };
  });
