'use strict';

/**
 * @ngdoc directive
 * @name otecApp.directive:timestampFormat
 * @description
 * # timestampFormat
 */
angular.module('otecApp')
  .directive('datepickerPopup', function(dateFilter,$parse){
    return{
      restrict:'EAC',
      require:'?ngModel',
      link:function(scope,element,attrs,ngModel,ctrl){
        ngModel.$parsers.push(function(viewValue){
          return dateFilter(viewValue,'dd-MM-yyyy');
        });
      }
    }
  });
