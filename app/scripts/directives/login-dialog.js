'use strict';

/**
 * @ngdoc directive
 * @name otecApp.directive:loginDialog
 * @description
 * # loginDialog
 */
angular.module('otecApp')
  .directive('loginDialog', function (AUTH_EVENTS) {
    return {
      restrict: 'A',
      template: '<div ng-if="visible" ng-include="\'views/login.html\'">',
      link: function (scope) {
        var showDialog = function () {
          scope.visible = true;
        };

        scope.visible = false;
        scope.$on(AUTH_EVENTS.notAuthenticated, showDialog);
        scope.$on(AUTH_EVENTS.sessionTimeout, showDialog);
      }
    };
  });
