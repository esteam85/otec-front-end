'use strict';

/**
 * @ngdoc directive
 * @name otecApp.directive:navBarOt
 * @description
 * # navBarOt
 */
angular.module('otecApp')
  .directive('navBarOt', function () {
    return {
      templateUrl: 'views/templates/nav-bar-ot-template.html',
      restrict: 'E',
      replace: true,
      link: function postLink(scope, element, attrs) {

      }
    };
  });
