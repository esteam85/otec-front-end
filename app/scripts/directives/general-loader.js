'use strict';

/**
 * @ngdoc directive
 * @name otecApp.directive:generalLoader
 * @description
 * # generalLoader
 */
angular.module('otecApp')
  .directive('generalLoader', function () {
    return {
      template: "<div class=\"spinner-general-loader\">\n" +
      "  <div class=\"rect1\"></div>\n" +
      "  <div class=\"rect2\"></div>\n" +
      "  <div class=\"rect3\"></div>\n" +
      "  <div class=\"rect4\"></div>\n" +
      "  <div class=\"rect5\"></div>\n" +
      "</div>",
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
      }
    };
  }).directive('generalLoaderMulti', function () {
    return {
      template: "<div class=\"spinner-general-loader-multi\">\n" +
      "  <div class=\"rect1\"></div>\n" +
      "  <div class=\"rect2\"></div>\n" +
      "  <div class=\"rect3\"></div>\n" +
      "  <div class=\"rect4\"></div>\n" +
      "  <div class=\"rect5\"></div>\n" +
      "</div>",
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
      }
    };
  });
