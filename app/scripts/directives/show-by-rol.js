'use strict';

/**
 * @ngdoc directive
 * @name otecApp.directive:showByRol
 * @description
 * # showByRol
 */
angular.module('otecApp')
  .directive('showByRol', function ($rootScope,$log) {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        element.hide();
        if(!!$rootScope.currentUserRole){
          try{
            var roles = JSON.parse(attrs.showByRol);
          }catch(e){
            $log.error("Json Mal Formado, revise el valor de show-by-rol,",e);
          }
          var test  = _.find( roles, function(rol){ return rol.toLowerCase() == $rootScope.currentUserRole.nombre.toLowerCase(); });
          if(!!test){
            element.show();
          }
        }
      }
    };
  });
