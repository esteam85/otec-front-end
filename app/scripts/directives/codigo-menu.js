'use strict';

/**
 * @ngdoc directive
 * @name otecApp.directive:codigoMenu
 * @description
 * # codigoMenu
 */
angular.module('otecApp')
  .directive('codigoMenu', function ($rootScope) {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        element.hide();
        if(!!$rootScope.currentUserRole){
          var test  = _.find( $rootScope.currentUserRole.rolesOpciones, function(opcion){ return opcion.codigoMenu == attrs.codigoMenu; });
          if(!!test){
            element.show();
          }
        }
      }
    };
  });
