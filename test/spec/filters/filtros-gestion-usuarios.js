'use strict';

describe('Filter: filtrosGestionUsuarios', function () {

  // load the filter's module
  beforeEach(module('otecApp'));

  // initialize a new instance of the filter before each test
  var filtrosGestionUsuarios;
  beforeEach(inject(function ($filter) {
    filtrosGestionUsuarios = $filter('filtrosGestionUsuarios');
  }));

  it('should return the input prefixed with "filtrosGestionUsuarios filter:"', function () {
    var text = 'angularjs';
    expect(filtrosGestionUsuarios(text)).toBe('filtrosGestionUsuarios filter: ' + text);
  });

});
