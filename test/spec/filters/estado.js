'use strict';

describe('Filter: estado', function () {

  // load the filter's module
  beforeEach(module('otecApp'));

  // initialize a new instance of the filter before each test
  var estado;
  beforeEach(inject(function ($filter) {
    estado = $filter('estado');
  }));

  it('should return the input prefixed with "estado filter:"', function () {
    var text = 'angularjs';
    expect(estado(text)).toBe('estado filter: ' + text);
  });

});
