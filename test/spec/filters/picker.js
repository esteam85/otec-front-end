'use strict';

describe('Filter: picker', function () {

  // load the filter's module
  beforeEach(module('otecApp'));

  // initialize a new instance of the filter before each test
  var picker;
  beforeEach(inject(function ($filter) {
    picker = $filter('picker');
  }));

  it('should return the input prefixed with "picker filter:"', function () {
    var text = 'angularjs';
    expect(picker(text)).toBe('picker filter: ' + text);
  });

});
