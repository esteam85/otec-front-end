'use strict';

describe('Controller: ModalListaDetalleInstanceCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var ModalListaDetalleInstanceCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    ModalListaDetalleInstanceCtrl = $controller('ModalListaDetalleInstanceCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ModalListaDetalleInstanceCtrl.awesomeThings.length).toBe(3);
  });
});
