'use strict';

describe('Controller: WorkflowsCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var WorkflowsCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    WorkflowsCtrl = $controller('WorkflowsCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(WorkflowsCtrl.awesomeThings.length).toBe(3);
  });
});
