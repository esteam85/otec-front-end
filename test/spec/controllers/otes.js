'use strict';

describe('Controller: OtesCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var OtesCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    OtesCtrl = $controller('OtesCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(OtesCtrl.awesomeThings.length).toBe(3);
  });
});
