'use strict';

describe('Controller: PerfilesCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var PerfilesCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    PerfilesCtrl = $controller('PerfilesCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PerfilesCtrl.awesomeThings.length).toBe(3);
  });
});
