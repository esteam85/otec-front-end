'use strict';

describe('Controller: RolesCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var RolesCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    RolesCtrl = $controller('RolesCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RolesCtrl.awesomeThings.length).toBe(3);
  });
});
