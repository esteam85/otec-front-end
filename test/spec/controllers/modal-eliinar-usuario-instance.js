'use strict';

describe('Controller: ModalEliinarUsuarioInstanceCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var ModalEliinarUsuarioInstanceCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    ModalEliinarUsuarioInstanceCtrl = $controller('ModalEliinarUsuarioInstanceCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ModalEliinarUsuarioInstanceCtrl.awesomeThings.length).toBe(3);
  });
});
