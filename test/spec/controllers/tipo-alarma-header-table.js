'use strict';

describe('Controller: TipoAlarmaHeaderTableCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var TipoAlarmaHeaderTableCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    TipoAlarmaHeaderTableCtrl = $controller('TipoAlarmaHeaderTableCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TipoAlarmaHeaderTableCtrl.awesomeThings.length).toBe(3);
  });
});
