'use strict';

describe('Controller: GestionAlarmasNotificacionesCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var GestionAlarmasNotificacionesCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    GestionAlarmasNotificacionesCtrl = $controller('GestionAlarmasNotificacionesCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(GestionAlarmasNotificacionesCtrl.awesomeThings.length).toBe(3);
  });
});
