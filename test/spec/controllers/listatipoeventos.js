'use strict';

describe('Controller: ListatipoeventosCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var ListatipoeventosCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    ListatipoeventosCtrl = $controller('ListatipoeventosCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ListatipoeventosCtrl.awesomeThings.length).toBe(3);
  });
});
