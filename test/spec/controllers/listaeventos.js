'use strict';

describe('Controller: ListaeventosCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var ListaeventosCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    ListaeventosCtrl = $controller('ListaeventosCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ListaeventosCtrl.awesomeThings.length).toBe(3);
  });
});
