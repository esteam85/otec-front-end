'use strict';

describe('Controller: TipoproveedorCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var TipoproveedorCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    TipoproveedorCtrl = $controller('TipoproveedorCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(TipoproveedorCtrl.awesomeThings.length).toBe(3);
  });
});
