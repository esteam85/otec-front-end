'use strict';

describe('Controller: CrearEventoCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var CrearEventoCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    CrearEventoCtrl = $controller('CrearEventoCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CrearEventoCtrl.awesomeThings.length).toBe(3);
  });
});
