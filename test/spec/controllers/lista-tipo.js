'use strict';

describe('Controller: ListaTipoCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var ListaTipoCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    ListaTipoCtrl = $controller('ListaTipoCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ListaTipoCtrl.awesomeThings.length).toBe(3);
  });
});
