'use strict';

describe('Controller: ProyectosCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var ProyectosCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    ProyectosCtrl = $controller('ProyectosCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProyectosCtrl.awesomeThings.length).toBe(3);
  });
});
