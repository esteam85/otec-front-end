'use strict';

describe('Controller: GestionUsuariosCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var GestionUsuariosCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    GestionUsuariosCtrl = $controller('GestionUsuariosCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(GestionUsuariosCtrl.awesomeThings.length).toBe(3);
  });
});
