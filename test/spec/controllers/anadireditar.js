'use strict';

describe('Controller: AnadireditarCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var AnadireditarCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    AnadireditarCtrl = $controller('AnadireditarCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AnadireditarCtrl.awesomeThings.length).toBe(3);
  });
});
