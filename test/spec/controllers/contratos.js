'use strict';

describe('Controller: ContratosCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var ContratosCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    ContratosCtrl = $controller('ContratosCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ContratosCtrl.awesomeThings.length).toBe(3);
  });
});
