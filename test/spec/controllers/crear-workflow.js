'use strict';

describe('Controller: CrearWorkflowCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var CrearWorkflowCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    CrearWorkflowCtrl = $controller('CrearWorkflowCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(CrearWorkflowCtrl.awesomeThings.length).toBe(3);
  });
});
