'use strict';

describe('Controller: ProveedoresCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var ProveedoresCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    ProveedoresCtrl = $controller('ProveedoresCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ProveedoresCtrl.awesomeThings.length).toBe(3);
  });
});
