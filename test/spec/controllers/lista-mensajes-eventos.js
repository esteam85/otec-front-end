'use strict';

describe('Controller: ListaMensajesEventosCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var ListaMensajesEventosCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    ListaMensajesEventosCtrl = $controller('ListaMensajesEventosCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ListaMensajesEventosCtrl.awesomeThings.length).toBe(3);
  });
});
