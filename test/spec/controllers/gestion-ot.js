'use strict';

describe('Controller: GestionOtCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var GestionOtCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    GestionOtCtrl = $controller('GestionOtCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(GestionOtCtrl.awesomeThings.length).toBe(3);
  });
});
