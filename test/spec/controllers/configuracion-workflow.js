'use strict';

describe('Controller: ConfiguracionWorkflowCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var ConfiguracionWorkflowCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    ConfiguracionWorkflowCtrl = $controller('ConfiguracionWorkflowCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ConfiguracionWorkflowCtrl.awesomeThings.length).toBe(3);
  });
});
