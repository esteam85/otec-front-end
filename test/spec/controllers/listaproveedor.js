'use strict';

describe('Controller: ListaproveedorCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var ListaproveedorCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    ListaproveedorCtrl = $controller('ListaproveedorCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ListaproveedorCtrl.awesomeThings.length).toBe(3);
  });
});
