'use strict';

describe('Controller: AlarmasCtrl', function () {

  // load the controller's module
  beforeEach(module('otecApp'));

  var AlarmasCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    AlarmasCtrl = $controller('AlarmasCtrl', {
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AlarmasCtrl.awesomeThings.length).toBe(3);
  });
});
