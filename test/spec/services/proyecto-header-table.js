'use strict';

describe('Service: proyectoHeaderTable', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var proyectoHeaderTable;
  beforeEach(inject(function (_proyectoHeaderTable_) {
    proyectoHeaderTable = _proyectoHeaderTable_;
  }));

  it('should do something', function () {
    expect(!!proyectoHeaderTable).toBe(true);
  });

});
