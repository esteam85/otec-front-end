'use strict';

describe('Service: authResolver', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var authResolver;
  beforeEach(inject(function (_authResolver_) {
    authResolver = _authResolver_;
  }));

  it('should do something', function () {
    expect(!!authResolver).toBe(true);
  });

});
