'use strict';

describe('Service: tipoDefaultTable', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var tipoDefaultTable;
  beforeEach(inject(function (_tipoDefaultTable_) {
    tipoDefaultTable = _tipoDefaultTable_;
  }));

  it('should do something', function () {
    expect(!!tipoDefaultTable).toBe(true);
  });

});
