'use strict';

describe('Service: proveedorHeaderTable', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var proveedorHeaderTable;
  beforeEach(inject(function (_proveedorHeaderTable_) {
    proveedorHeaderTable = _proveedorHeaderTable_;
  }));

  it('should do something', function () {
    expect(!!proveedorHeaderTable).toBe(true);
  });

});
