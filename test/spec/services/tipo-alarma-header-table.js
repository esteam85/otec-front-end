'use strict';

describe('Service: tipoAlarmaHeaderTable', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var tipoAlarmaHeaderTable;
  beforeEach(inject(function (_tipoAlarmaHeaderTable_) {
    tipoAlarmaHeaderTable = _tipoAlarmaHeaderTable_;
  }));

  it('should do something', function () {
    expect(!!tipoAlarmaHeaderTable).toBe(true);
  });

});
