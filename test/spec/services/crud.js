'use strict';

describe('Service: CRUD', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var CRUD;
  beforeEach(inject(function (_CRUD_) {
    CRUD = _CRUD_;
  }));

  it('should do something', function () {
    expect(!!CRUD).toBe(true);
  });

});
