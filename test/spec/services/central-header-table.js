'use strict';

describe('Service: centralHeaderTable', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var centralHeaderTable;
  beforeEach(inject(function (_centralHeaderTable_) {
    centralHeaderTable = _centralHeaderTable_;
  }));

  it('should do something', function () {
    expect(!!centralHeaderTable).toBe(true);
  });

});
