'use strict';

describe('Service: parentTipoHeaderDefaultTable', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var parentTipoHeaderDefaultTable;
  beforeEach(inject(function (_parentTipoHeaderDefaultTable_) {
    parentTipoHeaderDefaultTable = _parentTipoHeaderDefaultTable_;
  }));

  it('should do something', function () {
    expect(!!parentTipoHeaderDefaultTable).toBe(true);
  });

});
