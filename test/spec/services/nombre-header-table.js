'use strict';

describe('Service: nombreHeaderTable', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var nombreHeaderTable;
  beforeEach(inject(function (_nombreHeaderTable_) {
    nombreHeaderTable = _nombreHeaderTable_;
  }));

  it('should do something', function () {
    expect(!!nombreHeaderTable).toBe(true);
  });

});
