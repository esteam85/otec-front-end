'use strict';

describe('Service: agenciaHeaderTable', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var agenciaHeaderTable;
  beforeEach(inject(function (_agenciaHeaderTable_) {
    agenciaHeaderTable = _agenciaHeaderTable_;
  }));

  it('should do something', function () {
    expect(!!agenciaHeaderTable).toBe(true);
  });

});
