'use strict';

describe('Service: contratoHeaderTable', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var contratoHeaderTable;
  beforeEach(inject(function (_contratoHeaderTable_) {
    contratoHeaderTable = _contratoHeaderTable_;
  }));

  it('should do something', function () {
    expect(!!contratoHeaderTable).toBe(true);
  });

});
