'use strict';

describe('Service: tipoDefaultForm', function () {

  // load the service's module
  beforeEach(module('otecApp'));

  // instantiate service
  var tipoDefaultForm;
  beforeEach(inject(function (_tipoDefaultForm_) {
    tipoDefaultForm = _tipoDefaultForm_;
  }));

  it('should do something', function () {
    expect(!!tipoDefaultForm).toBe(true);
  });

});
