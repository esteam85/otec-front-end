'use strict';

describe('Directive: bpmnViewer', function () {

  // load the directive's module
  beforeEach(module('otecApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<bpmn-viewer></bpmn-viewer>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the bpmnViewer directive');
  }));
});
