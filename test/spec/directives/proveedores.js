'use strict';

describe('Directive: proveedores', function () {

  // load the directive's module
  beforeEach(module('otecApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<proveedores></proveedores>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the proveedores directive');
  }));
});
